﻿using System;
using System.Collections;
using System.Collections.Generic;
using Shailan.System.Ads;
using Shailan.System.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Shailan.AgeNeutral
{
    public class AgeNeutralScreen : MonoBehaviour
    {
        [SerializeField]
        private Canvas m_canRef = null;

        [SerializeField]
        private GraphicRaycaster m_grRef = null;

        [SerializeField]
        private TMP_Dropdown m_ddDay = null, m_ddMonth = null, m_ddYear = null;

        private List<string> m_lstDays = null, m_lstMonths = null, m_lstYears = null;

        [SerializeField]
        private Button m_btnProceed = null;

        [SerializeField]
        private GameObject m_goErrorMessage = null;

        private int Day { get; set; }
        private int Month { get; set; }
        private int Year { get; set; }

        public bool IsPlayerUnderaged { get; private set; } = true;

        public event Action OnAgeConfirmed = null;

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        public void ShowUI()
        {
            m_canRef.enabled = m_grRef.enabled = true;
        }

        public void HideUI()
        {
            m_canRef.enabled = m_grRef.enabled = false;
        }

        private void Awake()
        {
            AssignDays();
            AssignMonths();
            AssignYears();
        }

        private void AssignDays()
        {
            m_lstDays = new List<string>()
            {
                "DAY",
                "1","2","3","4","5","6","7","8","9","10",
                "11","12","13","14","15","16","17","18","19","20",
                "21","22","23","24","25","26","27","28","29","30","31"
            };

            m_ddDay.ClearOptions();
            m_ddDay.AddOptions(m_lstDays);
        }

        private void AssignMonths()
        {
            m_lstMonths = new List<string>()
            {
                "MONTH",
                "JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE",
                "JULY","AUGUST","SEPETEMBER","OCTOBER","NOVEMBER","DECEMBER"
            };

            m_ddMonth.ClearOptions();
            m_ddMonth.AddOptions(m_lstMonths);
        }

        private void AssignYears()
        {
            m_lstYears = new List<string>()
            {
                // "YEAR",
                "1900","1901","1902","1903","1904","1905","1906","1907","1908","1909",
                "1910","1911","1912","1913","1914","1915","1916","1917","1918","1919",
                "1920","1921","1922","1923","1924","1925","1926","1927","1928","1929",
                "1930","1931","1932","1933","1934","1935","1936","1937","1938","1939",
                "1940","1941","1942","1943","1944","1945","1946","1947","1948","1949",
                "1950","1951","1952","1953","1954","1955","1956","1957","1958","1959",
                "1960","1961","1962","1963","1964","1965","1966","1967","1968","1969",
                "1970","1971","1972","1973","1974","1975","1976","1977","1978","1979",
                "1980","1981","1982","1983","1984","1985","1986","1987","1988","1989",
                "1990","1991","1992","1993","1994","1995","1996","1997","1998","1999",
                "2000","2001","2002","2003","2004","2005","2006","2007","2008","2009",
                "2010","2011","2012","2013","2014","2015","2016","2017","2018","2019"
            };

            int l_iStartYear = 2020;
            int l_iLatestYear = DateTime.Now.Year;

            for (int i = l_iStartYear; i <= l_iLatestYear; i++)
            {
                m_lstYears.Add(i.ToString());
            }

            m_lstYears.Add("YEAR");
            m_lstYears.Reverse();

            m_ddYear.ClearOptions();
            m_ddYear.AddOptions(m_lstYears);
        }

        public void onValueChangeDays()
        {
            if (m_ddDay.value == 0 || m_ddMonth.value == 0 || m_ddYear.value == 0)
            {
                m_btnProceed.interactable = false;
            }
            else
            {
                m_btnProceed.interactable = true;
            }

            m_goErrorMessage.SetActive(false);

            // int l_iDay = 1;
            // if (!int.TryParse(m_ddDay.options[m_ddDay.value].text, out l_iDay))
            // {
            //     Debug.LogError("AgeNeutralScren:: try parse failed, " + m_ddDay.options[m_ddDay.value].text);
            // }
            // Day = l_iDay;

            Day = m_ddDay.value;

        }

        public void onValueChangeMonths()
        {
            // if (m_ddDay.value.Equals("DAY") || m_ddMonth.value.Equals("MONTH") || m_ddYear.value.Equals("YEAR"))
            if (m_ddDay.value == 0 || m_ddMonth.value == 0 || m_ddYear.value == 0)
            {
                m_btnProceed.interactable = false;
            }
            else
            {
                m_btnProceed.interactable = true;
            }

            m_goErrorMessage.SetActive(false);

            // int l_iMonth = 1;
            // if (!int.TryParse(m_ddMonth.options[m_ddMonth.value].text, out l_iMonth))
            // {
            //     Debug.LogError("AgeNeutralScren:: try parse failed, " + m_ddMonth.options[m_ddMonth.value].text);
            // }
            // Month = l_iMonth;

            Month = m_ddMonth.value;
        }

        public void onValueChangeYears()
        {
            if (m_ddDay.value == 0 || m_ddMonth.value == 0 || m_ddYear.value == 0)
            {
                m_btnProceed.interactable = false;
            }
            else
            {
                m_btnProceed.interactable = true;
            }

            m_goErrorMessage.SetActive(false);

            int l_iYear = 1;
            if (!int.TryParse(m_ddYear.options[m_ddYear.value].text, out l_iYear))
            {
                Debug.LogError("AgeNeutralScren:: try parse failed, " + m_ddYear.options[m_ddYear.value].text);
            }
            Year = l_iYear;
        }

        public void onClickProceedBtn()
        {
            // Debug.LogError($"{Day}/{Month}/{Year}");
            DateTime l_playerDob;
            try
            {
                l_playerDob = new DateTime(Year, Month, Day);

            }
            catch (ArgumentOutOfRangeException ex)
            {
                m_goErrorMessage.SetActive(true);
                return;
            }

            DateTime l_currentDateTime = DateTime.Now;

            // TimeSpan l_ts = l_currentDateTime.TimeOfDay - l_playerDob.TimeOfDay;

            // Debug.LogError("l_ts:" + l_ts.TotalDays);

            // DateTime l_difference = new DateTime(l_currentDateTime.Year - l_playerDob.Year, l_currentDateTime.Month - l_playerDob.Month, l_currentDateTime.Day - l_playerDob.Day);

            int playerAge = 0;

            if (l_currentDateTime.Month > l_playerDob.Month)
            {
                playerAge = l_currentDateTime.Year - l_playerDob.Year;

            }
            else if (l_currentDateTime.Month == l_playerDob.Month)
            {
                if (l_currentDateTime.Day >= l_playerDob.Day)
                {
                    playerAge = l_currentDateTime.Year - l_playerDob.Year;

                }
                else
                {
                    playerAge = l_currentDateTime.Year - l_playerDob.Year - 1;
                }
            }
            else
            {
                playerAge = l_currentDateTime.Year - l_playerDob.Year - 1;
            }


            // Debug.LogError("Year" + playerAge);

            if (playerAge <= 16)
            {
                IsPlayerUnderaged = true;
                // Debug.LogError("Underaged");
            }
            else
            {
                IsPlayerUnderaged = false;
                // Debug.LogError("Not Underaged");
            }

            IAdmobManagerRef.IsPlayerUnderAged = IsPlayerUnderaged;
            OnAgeConfirmed.Invoke();
            HideUI();
        }

    }
}

//  if (l_currentDateTime.Month >= l_playerDob.Month)
//             {
//                 if (l_currentDateTime.Day >= l_playerDob.Day)
//                 {
//                     playerAge = l_currentDateTime.Year - l_playerDob.Year;

//                 }
//                 else
//                 {
//                     playerAge = l_currentDateTime.Year - l_playerDob.Year - 1;

//                 }
//             }
//             else
//             {
//                 playerAge = l_currentDateTime.Year - l_playerDob.Year - 1;
//             }
