﻿using NestedXn0.Manager.Hud;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.WinMarkUI;
using Shailan.System.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.UI
{
    internal class UINestedBlock : MonoBehaviour
    {
        #region PROPERTIES

        /// <summary>
        /// Block Id
        /// </summary>
        [SerializeField]
        private int m_id = -1;
        internal int Id { get { return m_id; } }

        /// <summary>
        /// X or 0?
        /// </summary>
        private bool? m_binaryCode = null;
        internal bool? BinaryCode
        {
            get
            {
                return m_binaryCode;
            }
        }

        /// <summary>
        /// For testing(In Debug) where -1= not marked, 0=true, 1=false
        /// </summary>
        private int DebugBinaryCode = -1;

        /// <summary>
        /// FOR EDITOR TESTING
        /// </summary>
        [SerializeField]
        private bool m_isMarked = false;

        /// <summary>
        /// Is Block Marked?
        /// </summary>
        /// <value></value>
        internal bool IsMarked
        {
            get
            {
                if (m_binaryCode == null)
                {
                    // m_isMarked = false;    // FOR EDITOR TESTING
                    return false;
                }
                // m_isMarked = true;      // FOR EDITOR TESTING
                return true;
            }
        }

        /// <summary>
        /// Binary code of the player who initially owns the block
        /// </summary>
        private bool? m_isBlockOwner = null;

        /// <summary>
        /// winning patterns possible
        /// </summary>
        [SerializeField]
        private Pattern[] m_arrPatterns = null;
        internal Pattern[] arrPatterns { get { return m_arrPatterns; } }

        #endregion

        /// <summary>
        /// Respective UI Xn0 Ref
        /// </summary>
        [SerializeField]
        private UIXn0 m_UIXn0Ref = null;

        /// <summary>
        /// Main UI Nested xn0 ref
        /// </summary>
        [SerializeField]
        private UINestedXn0 m_UINestedXn0Ref = null;

        /// <summary>
        /// Icon holding image ref
        /// </summary>
        [SerializeField]
        private Image m_img = null;

        /// <summary>
        /// Block btn ref
        /// </summary>
        [SerializeField]
        private Button m_btnImg = null;

        /// <summary>
        /// Win mark Script ref
        /// </summary>
        [SerializeField]
        private WinMark m_WinMarkref = null;

        /// <summary>
        /// Player manager ref
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Hud Manager ref
        /// </summary>
        private IHudManager m_IHudManagerRef = null;
        private IHudManager IHudManagerRef
        {
            get
            {
                if (m_IHudManagerRef == null)
                {
                    m_IHudManagerRef = ManagersHandler.GetManager<IHudManager>();
                }
                return m_IHudManagerRef;
            }
        }

        private void Start()
        {
            m_UIXn0Ref.OnWinnerAchieved -= OnBlockWinner;
            m_UIXn0Ref.OnWinnerAchieved += OnBlockWinner;
            m_UIXn0Ref.OnDrawAchieved -= OnBlockDraw;
            m_UIXn0Ref.OnDrawAchieved += OnBlockDraw;
        }

        /// <summary>
        /// Set X or 0 sprite
        /// </summary>
        /// <param name="a_spr"></param>
        /// <param name="a_col"></param>
        internal void SetSprite(Sprite a_spr, Color a_col)
        {
            m_img.sprite = a_spr;
            m_img.color = a_col;

            //CHANGE ALPHA
            Color l_col = m_img.color;
            l_col.a = 1;
            m_img.color = l_col;

            m_btnImg.enabled = false;
        }

        /// <summary>
        /// Set binary code of the block
        /// </summary>
        /// <param name="a_value"></param>
        internal void SetBinaryCode(bool a_value)
        {
            m_binaryCode = a_value;
            if (a_value)
            {
                DebugBinaryCode = 0;
            }
            else
            {
                DebugBinaryCode = 1;
            }
        }

        /// <summary>
        /// On winning pattern or draw achieved in the respective UIXn0
        /// </summary>
        /// <param name="a_blockWinnerBinaryCode"></param>
        /// <returns></returns>
        private bool OnBlockCompleted(bool a_blockWinnerBinaryCode)
        {
            m_isMarked = true;  // for editor testing
            bool l_isGameComplted = m_UINestedXn0Ref.onNestedBlockComplete(m_id, a_blockWinnerBinaryCode);
            m_isBlockActive = false;

            m_img.enabled = true;
            m_UIXn0Ref.gameObject.SetActive(false);
            IHudManagerRef.ZoomOutToBlock(m_id);  // zoom out to block

            return l_isGameComplted;
        }

        /// <summary>
        /// Is block's recpective UIXn0 active?
        /// </summary>
        private bool m_isBlockActive = false;
        internal bool IsBlockActive { get { return m_isBlockActive; } }

        // private bool m_isNestedBlockActivation = false;

        /// <summary>
        /// Activate the recpective UIXn0
        /// </summary>
        public void ActivateBlock()
        {
            // if (!m_isNestedBlockActivation)
            // {
            //     return;
            // }

            m_isBlockActive = true;
            m_img.enabled = false;
            m_UIXn0Ref.gameObject.SetActive(true);
            m_UINestedXn0Ref.DisableAllBlocks();
            m_isBlockOwner = IPlayerManagerRef.CurrentPlayer.BinaryCode;
            IHudManagerRef.ZoomInToBlock(m_id);  // zoom in to block

            // REGISTER BOT EVENT
            m_UIXn0Ref.RegisterBotEvent();
        }

        /// <summary>
        /// Enable block for interaction
        /// </summary>
        internal void EnableBlock()
        {
            if (IsMarked)
            {
                return;
            }
            m_btnImg.interactable = true;

            //CHANGE COLOR (USE COLOR.LERP HEAR MAYBE)
            // m_img.color = Color.gray;

            // Color l_col = m_img.color;
            // l_col.a = 1;
            // m_img.color = l_col;
        }

        /// <summary>
        /// Disable block for interaction
        /// </summary>
        internal void DisableBlock()
        {
            if (IsMarked)
            {
                return;
            }
            // m_isNestedBlockActivation = false;
            m_btnImg.interactable = false;

            //CHANGE COLOR (USE COLOR.LERP HEAR MAYBE)
            // m_img.color = Color.white;

            // Color l_col = m_img.color;
            // l_col.a = 0.25f;
            // m_img.color = l_col;
        }

        /// <summary>
        /// On block winner achived
        /// </summary>
        /// <param name="a_playerBinaryCode"></param>
        /// <param name="a_iwinningPattern"></param>
        private void OnBlockWinner(bool a_playerBinaryCode, int a_iwinningPattern)
        {
            // MARK WINNING PATTERN AND EXECUTE WINNER CONDTIONS
            m_WinMarkref.OnWinMarkCompleted -= () => { ExecuteWinnerCondition(a_playerBinaryCode, a_iwinningPattern); };
            m_WinMarkref.OnWinMarkCompleted += () => { ExecuteWinnerCondition(a_playerBinaryCode, a_iwinningPattern); };
            m_WinMarkref.WinMarkThePattern(a_iwinningPattern, false);
        }

        /// <summary>
        /// Executes when block winner is achieved
        /// </summary>
        /// <param name="a_playerBinaryCode"></param>
        /// <param name="a_iwinningPattern"></param>
        private void ExecuteWinnerCondition(bool a_playerBinaryCode, int a_iwinningPattern)
        {
            bool l_isGameComplted = OnBlockCompleted(a_playerBinaryCode);

            if (l_isGameComplted)
            {
                return;
            }

            //WINNER GETS TO CHOOSE THE NEXT BLOCK
            Player l_objOwner = IPlayerManagerRef.getPlayerViaBinaryCode(a_playerBinaryCode);

            if (l_objOwner.EPlayerId.Equals(EPlayer.Player2) && IPlayerManagerRef.IsPlayer2Bot)
            {
                m_UINestedXn0Ref.AssignTurn(a_playerBinaryCode);

                // MARK WINNER
                m_UINestedXn0Ref.BotsTurn();
            }
            else
            {
                m_UINestedXn0Ref.AssignTurn(a_playerBinaryCode);
                m_UINestedXn0Ref.EnableAllBlocks();
            }
        }

        /// <summary>
        /// On block draw achieved
        /// </summary>
        private void OnBlockDraw()
        {
            if (m_isBlockOwner == null)
            {
                Debug.LogError("UINestedBlock:: Block owner not assigned");
                return;
            }
            bool l_blockOwner = (bool)m_isBlockOwner;
            bool l_isGameComplted = OnBlockCompleted(l_blockOwner);

            if (l_isGameComplted)
            {
                return;
            }

            // BLOCK OWN CHANGES HERE
            Player l_objOwner = IPlayerManagerRef.getPlayerViaBinaryCode(l_blockOwner);
            if (l_objOwner.EPlayerId.Equals(EPlayer.Player1) && IPlayerManagerRef.IsPlayer2Bot)
            {
                m_UINestedXn0Ref.AssignTurn(!l_blockOwner);
                m_UINestedXn0Ref.BotsTurn();
            }
            else
            {
                m_UINestedXn0Ref.AssignTurn(!l_blockOwner);
                m_UINestedXn0Ref.EnableAllBlocks();
            }
        }

        /// <summary>
        /// Bots turn for respective UI Xn0 blocks
        /// </summary>
        internal void traditionalXn0BotTurn()
        {
            m_UIXn0Ref.BotsTurn();
        }

        /// <summary>
        /// Resets block data
        /// </summary>
        internal void ResetBlock()
        {
            m_img.sprite = null;
            m_binaryCode = null;
            DebugBinaryCode = -1;

            Color l_col = m_img.color;
            l_col.a = 0.0f;
            m_img.color = l_col;

            m_isMarked = false;

            m_isBlockActive = false; ;
            m_img.enabled = true;
            m_UIXn0Ref.gameObject.SetActive(false);
            DisableBlock();
            m_isBlockOwner = null;
            m_btnImg.enabled = true;

            m_UIXn0Ref.ResetAllBlocks();

            m_WinMarkref.ResetMark(); // RESET WIN PATTERN MARK
        }
    }
}