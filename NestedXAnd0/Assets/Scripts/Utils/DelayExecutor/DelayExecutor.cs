﻿using System;
using System.Threading.Tasks;

namespace Shailan.Utils.DelayExecutor
{
    public class DelayExecutor
    {

        /// <summary>
        /// Default Contructor 
        /// </summary>
        /// <param name="a_fltTime"></param>
        /// <param name="a_callBack"></param>
        public DelayExecutor(float a_fltTime, Action a_callBack)
        {
            ExecuteDelegateInDelay(a_fltTime, a_callBack);
        }

        /// <summary>
        /// Execute the provided delegate with given delay
        /// </summary>
        /// <param name="a_fltTime"></param>
        /// <param name="a_callBack"></param>
        /// <returns></returns>
        private async void ExecuteDelegateInDelay(float a_fltTime, Action a_callBack)
        {
            await Task.Delay(TimeSpan.FromSeconds(a_fltTime));
            a_callBack.Invoke();
        }
    }
}