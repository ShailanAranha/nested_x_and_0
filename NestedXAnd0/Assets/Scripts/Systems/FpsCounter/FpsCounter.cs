﻿using System.Collections.Generic;
using UnityEngine;

namespace Shailan.System.Fps
{
    internal class FpsCounter : MonoBehaviour, IFpsCounter
    {
        /// <summary>
        /// how many frames to wait until you re-calculate the FPS
        /// </summary>
        private int m_iGranularity = 5;

        /// <summary>
        /// Stores fps value during counter
        /// </summary>
        private List<double> m_lstTimes;

        /// <summary>
        /// Maintains current counter value
        /// </summary>
        private int m_iCounter = 5;

        /// <summary>
        /// Fps value
        /// </summary>
        private double m_doFps = 0.0f;
        public double Fps
        {
            get
            {
                return m_doFps;
            }
        }

        public void Start()
        {
            m_lstTimes = new List<double>();
        }

        public void Update()
        {
            if (m_iCounter <= 0)
            {
                CalculateFPS();
                m_iCounter = m_iGranularity;
            }

            m_lstTimes.Add(Time.deltaTime);
            m_iCounter--;
        }

        /// <summary>
        ///  Calculate average fps value based on Granularity
        /// </summary>
        private void CalculateFPS()
        {
            double sum = 0;
            foreach (double F in m_lstTimes)
            {
                sum += F;
            }

            double average = sum / m_lstTimes.Count;
            m_doFps = 1 / average;
            m_lstTimes.Clear();
        }
    }
}