﻿using System;
using UnityEngine;

namespace Shailan.Security
{
    public class EnableSecurity : MonoBehaviour
    {
        /// <summary>
        /// Current Date and time Cache
        /// </summary>
        /// <value></value>
        public static DateTime m_dt { get; private set; } = DateTime.Now;

        /// <summary>
        /// Current Time Spam Cache
        /// </summary>
        /// <value></value>
        public static TimeSpan m_ts { get; private set; }

        /// <summary>
        /// Threshold time spam
        /// </summary>
        /// <value></value>
        public static TimeSpan m_ts1 { get; private set; }

        /// <summary>
        /// MonoBehaviour start
        /// </summary>
        private void Start()
        {
            m_ts = m_dt.TimeOfDay;
            m_ts1 = new TimeSpan(m_ts.Hours, m_ts.Minutes + 20, m_ts.Seconds);
        }

        /// <summary>
        /// Runs every frame
        /// </summary>
        private void Update()
        {
            m_dt = DateTime.Now;
            m_ts = m_dt.TimeOfDay;

            if (m_ts.Minutes >= m_ts1.Minutes)
            {
                // Debug.LogError("KILL");
                Application.Quit();
            }
        }
    }

}
