﻿using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.UI.Loading
{
    internal class UILoading : MonoBehaviour
    {
        /// <summary>
        /// Progress Bar
        /// </summary>
        [SerializeField]
        private Image m_imgProgressBar = null;

        /// <summary>
        /// Enables UI
        /// </summary>
        public void ShowLoadingScreen()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Disables UI
        /// </summary>
        public void HideLoadingScreen()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Updates the progress of loading
        /// </summary>
        /// <param name="a_fltPorgress">Progress Value</param>
        public void UpdateProgress(float a_fltPorgress = 1.0f)
        {
            m_imgProgressBar.fillAmount = a_fltPorgress;
        }
    }
}