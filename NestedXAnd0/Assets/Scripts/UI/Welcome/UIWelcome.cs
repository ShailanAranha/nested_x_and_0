﻿using System;
using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;

namespace NestedXn0.UI.Welcome
{
    internal class UIWelcome : MonoBehaviour
    {

        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_anim = null;

        /// <summary>
        /// Speed of the animation
        /// </summary>
        [SerializeField, Range(0.0f, 1.0f)]
        private float m_fltSpeed = 1.0f;

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// Rect reference
        /// </summary>
        [SerializeField]
        private RectTransform m_rectCanvas = null, m_rectTopPanel = null, m_rectBotPanel = null;

        /// <summary>
        /// On complete event
        /// </summary>
        internal event Action OnWelcomeScreenCompleted = null;

        /// <summary>
        /// Initiate welcome animation
        /// </summary>
        public void ShowWelcomeScreen()
        {
            m_rectTopPanel.sizeDelta = new Vector2(m_rectCanvas.rect.width, m_rectCanvas.rect.height / 2);
            m_rectBotPanel.sizeDelta = new Vector2(m_rectCanvas.rect.width, m_rectCanvas.rect.height / 2);

            // m_rectTopPanel.sizeDelta = new Vector2(m_rectCanvas.rect.width, m_rectCanvas.rect.height);
            // m_rectBotPanel.sizeDelta = new Vector2(m_rectCanvas.rect.width, m_rectCanvas.rect.height);

            // Debug.LogError("m_rectCanvas.rect.height:" + m_rectCanvas.rect.height);
            // Debug.LogError("m_rectCanvas.rect.width:" + m_rectCanvas.rect.width);

            m_anim.Play("Welcome");
            m_anim["Welcome"].speed = m_fltSpeed;
            gameObject.SetActive(true);
            // ISoundManagerRef.playSfx("a_welcome");
            ISoundManagerRef.playAmbientMusic("a_welcome");
        }

        /// <summary>
        /// Hide UI After the animation complete event is called
        /// </summary>
        public void HideUI()
        {
            gameObject.SetActive(false);
            m_anim.Stop();
            ISoundManagerRef.playAmbientMusic("a_amb_bensound_buddy");
            OnWelcomeScreenCompleted?.Invoke();
        }
    }

}