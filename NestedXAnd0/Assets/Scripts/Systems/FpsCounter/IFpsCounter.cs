﻿namespace Shailan.System.Fps
{
    internal interface IFpsCounter
    {
        /// <summary>
        /// Fps value
        /// </summary>
        double Fps { get; }
    }
}