﻿using UnityEngine;

namespace Shailan.System.Manager
{
    public abstract class ManagerBase : MonoBehaviour, IManagerBase
    {
        /// <summary>
        /// Managers Awake call(Executes necessary manager operation before implementing dervied class's awake call)
        /// </summary>
        protected virtual void ManagerAwake() { }

        /// <summary>
        /// Managers Start call(Executes necessary manager operation before implementing dervied class's start call)
        /// </summary>
        protected virtual void ManagerStart() { }

        /// <summary>
        /// Managers Destroy call(Executes necessary manager operation before implementing dervied class's destroy call)
        /// </summary>
        protected virtual void ManagerDestroy() { }

        /// <summary>
        /// Monobehaviour call awake
        /// </summary>
        protected void Awake()
        {
            ManagersHandler.RegisterManager(this);
            ManagerAwake();
        }

        /// <summary>
        /// Monobehaviour call start
        /// </summary>
        protected void Start()
        {
            ManagerStart();
        }

        /// <summary>
        /// Monobehaviour call destroy
        /// </summary>
        protected void OnDestroy()
        {
            ManagerDestroy();
            ManagersHandler.UnregisterManager(this);
        }

    }
}