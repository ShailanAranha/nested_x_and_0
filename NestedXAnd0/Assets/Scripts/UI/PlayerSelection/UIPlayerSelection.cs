﻿using Firebase.Analytics;
using NestedXn0.Manager.MainGameUIManager;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Manager.Xn0Manager;
using NestedXn0.Menu;
using Shailan.System.Ads;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using Shailan.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.UI.PlayerSelection
{
    internal class UIPlayerSelection : MonoBehaviour
    {
        private IMainGameUIManager m_IMainGameUIManagerRef = null;
        private IMainGameUIManager IMainGameUIManagerRef
        {
            get
            {
                if (m_IMainGameUIManagerRef == null)
                {
                    m_IMainGameUIManagerRef = ManagersHandler.GetManager<IMainGameUIManager>();
                }
                return m_IMainGameUIManagerRef;
            }
        }

        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        [SerializeField]
        private GameObject m_goBackGround = null;

        internal void ShowUI()
        {
            m_goBackGround.SetActive(true);
            SetInitialValues();
            gameObject.SetActive(true);
        }

        internal void HideUI()
        {
            gameObject.SetActive(false);
            m_goBackGround.SetActive(false);
        }

        private void SetInitialValues()
        {
            // ResetAnalysisData();
            IPlayerManagerRef.SetPlayer2Status(false);
            m_goPlayerSliderImg.anchoredPosition = m_vecPlayer2Pos;
            m_goDifficulty.SetActive(false);
            DefaultNickname_2P();

            IPlayerManagerRef.SetCurrentBotType(EBotType.Beginner);
            m_goDifficultySliderImg.anchoredPosition = m_vecBeginnerPos;

            AssignXn0Colors();
        }

        #region ANALYTICS

        // private const string TWO_PLAYER_DATA_VALUE = "2 Players";
        // private const string BOT_PLAYER_DATA_VALUE = "Comp";

        // private const string NO_BOT_VALUE = "N0 Bots";
        // private const string BOT_BEGINNER_VALUE = "Beginer";
        // private const string BOT_REGULAR_VALUE = "Regular";
        // private const string BOT_DIFFICULT_VALUE = "Difficult";


        // public string PlayerSelectionData { get; private set; } = TWO_PLAYER_DATA_VALUE;
        // public string BotData { get; private set; } = NO_BOT_VALUE;

        // private void ResetAnalysisData()
        // {
        //     PlayerSelectionData = TWO_PLAYER_DATA_VALUE;
        //     BotData = NO_BOT_VALUE;
        // }

        #endregion

        #region PLAYER SELECTION

        [Header("Player Selection"), SerializeField, Space]
        private RectTransform m_goPlayerSliderImg = null;

        [SerializeField]
        private Vector2 m_vecPlayer1Pos = Vector2.zero, m_vecPlayer2Pos = Vector2.zero;

        public void OnClick_Player1Btn()
        {
            // PlayerSelectionData = BOT_PLAYER_DATA_VALUE;
            IPlayerManagerRef.SetPlayer2Status(true);
            UITweening.MoveLerp(m_goPlayerSliderImg, m_goPlayerSliderImg.anchoredPosition, m_vecPlayer1Pos, 0.25f, () =>
            {
                m_goDifficulty.SetActive(true);
                DefaultNickname_1P();
            });
        }

        public void OnClick_Player2Btn()
        {
            // PlayerSelectionData = TWO_PLAYER_DATA_VALUE;
            IPlayerManagerRef.SetPlayer2Status(false);
            UITweening.MoveLerp(m_goPlayerSliderImg, m_goPlayerSliderImg.anchoredPosition, m_vecPlayer2Pos, 0.25f, () =>
            {
                m_goDifficulty.SetActive(false);
                DefaultNickname_2P();
            });
        }
        #endregion

        #region DIFFICULTY SELECTION

        [Header("Difficulty Selection"), SerializeField, Space]
        private RectTransform m_goDifficultySliderImg = null;

        [SerializeField]
        private Vector2 m_vecBeginnerPos = Vector2.zero, m_vecRegularPos = Vector2.zero, m_vecProPos = Vector2.zero;

        [SerializeField]
        private GameObject m_goDifficulty = null;
        public void OnClick_BeginnerBtn()
        {
            // BotData = BOT_BEGINNER_VALUE;
            IPlayerManagerRef.SetCurrentBotType(EBotType.Beginner);
            UITweening.MoveLerp(m_goDifficultySliderImg, m_goDifficultySliderImg.anchoredPosition, m_vecBeginnerPos, 0.25f);
        }

        public void OnClick_RegularBtn()
        {
            // BotData = BOT_REGULAR_VALUE;
            IPlayerManagerRef.SetCurrentBotType(EBotType.Regular);
            UITweening.MoveLerp(m_goDifficultySliderImg, m_goDifficultySliderImg.anchoredPosition, m_vecRegularPos, 0.25f);
        }

        public void OnClick_ProBtn()
        {
            // BotData = BOT_DIFFICULT_VALUE;
            IPlayerManagerRef.SetCurrentBotType(EBotType.Pro);
            UITweening.MoveLerp(m_goDifficultySliderImg, m_goDifficultySliderImg.anchoredPosition, m_vecProPos, 0.25f);
        }
        #endregion

        #region BEGIN

        public void OnClick_BeginBtn()
        {
            //REMOVE BANNER AD
            IAdmobManagerRef.HideAdBanner();
            IAdmobManagerRef.ShowBannerAdIfLoaded = false;

            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                HideUI();
                IMainGameUIManagerRef.InitiateTraditinalXn0Game();
            }
            else if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Nested))
            {
                HideUI();
                IMainGameUIManagerRef.InitiateNestedXnoGame();
            }
            else
            {
                Debug.LogError("UIEndGameMenu::EXn0Type not asigned!!!");
            }
        }

        public void onClick_BackBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(MenuType.NewGame);
        }
        #endregion

        #region PLAYER NICKNAME

        [Header("Player Nickname"), SerializeField, Space]
        private Button m_btnSwitch = null;

        [SerializeField]
        private TMP_InputField m_ifPlayer1 = null, m_ifPlayer2 = null;

        internal string getPlayer1NickName { get { return m_ifPlayer1.text; } }
        internal string getPlayer2NickName { get { return m_ifPlayer2.text; } }

        private void DefaultNickname_1P()
        {
            m_ifPlayer1.text = PlayerManager.DEFAULT_PLAYER1_NAME;
            m_ifPlayer2.text = PlayerManager.DEFAULT_COMP_NAME;
            m_ifPlayer2.interactable = false;
        }


        private void DefaultNickname_2P()
        {
            m_ifPlayer1.text = PlayerManager.DEFAULT_PLAYER1_NAME;
            m_ifPlayer2.text = PlayerManager.DEFAULT_PLAYER2_NAME;
            m_ifPlayer2.interactable = true;
        }

        /// <summary>
        /// Assigns default nickname of left it empty
        /// </summary>
        public void onDeselect_ifPlayer1()
        {
            if (string.IsNullOrEmpty(m_ifPlayer1.text))
                m_ifPlayer1.text = PlayerManager.DEFAULT_PLAYER1_NAME; ;
        }

        /// <summary>
        /// Assigns default nickname of left it empty
        /// </summary>
        public void onDeselect_ifPlayer2()
        {
            if (string.IsNullOrEmpty(m_ifPlayer2.text))
                m_ifPlayer2.text = PlayerManager.DEFAULT_PLAYER2_NAME;
        }

        #endregion

        #region BINARY CODE SWITCH

        [SerializeField]
        private Image m_imgXRef = null, m_img0Ref = null;

        internal bool getPlayer1BinaryCode { get; private set; } = true;
        internal bool getPlayer2BinaryCode { get; private set; } = false;

        [SerializeField]
        private RectTransform m_rectBinarySymbols = null;

        /// <summary>
        /// Switches the binary code of the players(X 0 value)
        /// </summary>
        public void onClick_Switch()
        {
            m_btnSwitch.interactable = false;
            Vector2 l_vec2Switch = new Vector2(0, m_rectBinarySymbols.eulerAngles.y + 180);
            UITweening.RotateLerp(m_rectBinarySymbols, m_rectBinarySymbols.eulerAngles, l_vec2Switch, 0.5f, () =>
            {
                getPlayer1BinaryCode = !getPlayer1BinaryCode;
                getPlayer2BinaryCode = !getPlayer2BinaryCode;
                m_btnSwitch.interactable = true;
            });

            
            // Analytics
            FirebaseAnalytics.LogEvent("BinaryCodeSwitch");
        }

        /// <summary>
        /// Assigns the color of X n 0 sprites from the settings menu
        /// </summary>
        private void AssignXn0Colors()
        {
            m_imgXRef.color = IMenuManagerRef.XColor;
            m_img0Ref.color = IMenuManagerRef.YColor;
        }

        #endregion

    }
}