﻿using System;
using UnityEngine;

namespace NestedXn0.UI
{
    internal class CamZoomToUI : MonoBehaviour
    {
        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_animRef = null;

        /// <summary>
        /// Exectues when zoom in is completed
        /// </summary>
        internal event Action OnZoomInFinished = null;

        /// <summary>
        /// Exectues when zoom out is completed
        /// </summary>
        internal event Action OnZoomOutFinished = null;

        /// <summary>
        /// Is camera zoom in?
        /// </summary>
        private bool m_isZoomIn = false;

        /// <summary>
        /// Invokes when zoom in completed
        /// </summary>
        public void OnZoomInCompleted()
        {
            if (m_isZoomIn)
            {
                return;
            }
            m_isZoomIn = true;
            OnZoomInFinished?.Invoke();
        }

        /// <summary>
        ///  Invokes when zoom out completed
        /// </summary>
        public void OnZoomOutCompleted()
        {
            if (!m_isZoomIn)
            {
                return;
            }
            m_isZoomIn = false;
            OnZoomOutFinished?.Invoke();
        }

        /// <summary>
        /// Zoom in to block via id
        /// </summary>
        /// <param name="a_iBlockId"></param>
        internal void ZoomInBlock(int a_iBlockId)
        {
            string l_strAnimName = "ZoomToBlock" + a_iBlockId;
            m_animRef[l_strAnimName].speed = 1;
            m_animRef.Play(l_strAnimName);
        }

        /// <summary>
        /// Zoom out from block via id
        /// </summary>
        /// <param name="a_iBlockId"></param>
        internal void ZoomOutBlock(int a_iBlockId)
        {
            string l_strAnimName = "ZoomToBlock" + a_iBlockId;
            m_animRef[l_strAnimName].speed = -1;
            m_animRef[l_strAnimName].time = m_animRef[l_strAnimName].length;
            m_animRef.Play(l_strAnimName);
        }
    }
}