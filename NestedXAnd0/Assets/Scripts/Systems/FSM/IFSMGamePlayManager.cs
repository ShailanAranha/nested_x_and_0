﻿using Shailan.System.Manager;

namespace Shailan.System.FSM
{
    internal interface IFSMGamePlayManager : IManagerBase
    {
        /// <summary>
        /// Index of the current state
        /// </summary>
        /// <value></value>
        int StateIndex { get; }

        /// <summary>
        /// Current running state
        /// </summary>
        /// <value></value>
        string CurrentState { get; }

        /// <summary>
        /// Initializing finate state machine
        /// </summary>
        void InitiateFSM();

        /// <summary>
        /// Transition to state via state name
        /// </summary>
        /// <param name="a_strState">State Name</param>
        void TransitionToState(string a_strState);

        /// <summary>
        /// Transitions to next state from the flow list
        /// </summary>
        void TransitionToNextState();
    }
}