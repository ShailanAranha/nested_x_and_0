﻿using NestedXn0.UI.Credits;
using NestedXn0.UI.HowToPlay;
using NestedXn0.UI.Setting;
using Shailan.System.Ads;
using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal class MenuManager : ManagerBase, IMenuManager
    {
        /// <summary>
        /// List of type of menu
        /// </summary>
        [SerializeField]
        private UIMenuBase[] m_arrUIMenuBaseRef = null;

        /// <summary>
        /// Last menu shown in the screen
        /// </summary>
        /// <value></value>
        public MenuType LastShownMenu { get; set; } = MenuType.None;

        /// <summary>
        /// Previous menu of the Last menu shown in the screen
        /// </summary>
        /// <value></value>
        public MenuType PreviouslyShownMenu { get; set; } = MenuType.None;

        /// <summary>
        /// Sound manager instance ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        /// <summary>
        /// Custom Manager ref
        /// </summary>
        protected override void ManagerStart()
        {
            base.ManagerStart();
            // LOAD BANNER AD
            // IAdmobManagerRef.LoadBannerAd();
            // IAdmobManagerRef.LoadInterstitialAd();

            // SHOW MAIN MENU UI
            ShowMenu(MenuType.MainMenu);

            // GET SAVED SETTINGS DATA 
            m_UISettingsRef.getSavedData();

            //PLAY AMBIENT MUSIC
            ISoundManagerRef.playAmbientMusic("a_amb_bensound_little_idea");

        }

        /// <summary>
        /// Show Menu Via type
        /// </summary>
        /// <param name="a_Etype"></param>
        public void ShowMenu(MenuType a_Etype)
        {
            int l_iCount = m_arrUIMenuBaseRef.Length;
            bool l_isFound = false;

            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrUIMenuBaseRef[i].EType.Equals(a_Etype))
                {
                    l_isFound = true;
                    if (!a_Etype.Equals(LastShownMenu))
                    {
                        PreviouslyShownMenu = LastShownMenu;
                        LastShownMenu = a_Etype;
                    }
                    m_arrUIMenuBaseRef[i].ShowUI();
                    break;
                }
            }

            if (!l_isFound)
            {
                Debug.LogError("MenuManager:: Menu not found:" + a_Etype);
            }
        }

        /// <summary>
        /// Hide Menu via type
        /// </summary>
        /// <param name="a_Etype"></param>
        public void HideMenu(MenuType a_Etype)
        {
            int l_iCount = m_arrUIMenuBaseRef.Length;
            bool l_isFound = false;

            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrUIMenuBaseRef[i].EType.Equals(a_Etype))
                {
                    l_isFound = true;
                    m_arrUIMenuBaseRef[i].HideUI();
                    break;
                }
            }

            if (!l_isFound)
            {
                Debug.LogError("MenuManager:: Menu not found:" + a_Etype);
            }
        }

        #region HOW TO PLAY

        /// <summary>
        /// How to play UI ref
        /// </summary>
        [SerializeField]
        private UIHowToPlay m_UIHowToPlayRef = null;

        /// <summary>
        /// Show UI
        /// </summary>
        public void showHowToPlayUI()
        {
            m_UIHowToPlayRef.ShowUI();
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        public void hideHowToPlayUI()
        {
            m_UIHowToPlayRef.HideUI();
        }
        #endregion

        #region CREDITS

        /// <summary>
        /// Credits UI Ref
        /// </summary>
        [SerializeField]
        private UICredits m_UICreditsRef = null;

        /// <summary>
        /// Show UI
        /// </summary>
        public void showUICreditsUI()
        {
            m_UICreditsRef.ShowUI();
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        public void hideUICreditsUI()
        {
            m_UICreditsRef.HideUI();
        }
        #endregion

        #region END GAME

        /// <summary>
        /// Initiates End Screen menu button animations
        /// </summary>
        public void PlayEndScreenBtnAnim()
        {
            int l_iCount = m_arrUIMenuBaseRef.Length;

            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrUIMenuBaseRef[i].EType.Equals(MenuType.EndGame))
                {
                    UIEndGameMenu l_obj = (UIEndGameMenu)m_arrUIMenuBaseRef[i];
                    l_obj.ShowEndScreenBtns();
                    break;
                }
            }
        }
        #endregion

        #region SETTINGS MENU

        /// <summary>
        /// Setting UI ref
        /// </summary>
        [SerializeField]
        private UISettings m_UISettingsRef = null;

        /// <summary>
        /// Current customized color of x
        /// </summary>
        /// <value></value>
        public Color XColor { get { return m_UISettingsRef.XColor; } }

        /// <summary>
        /// Current customized color of 0
        /// </summary>
        /// <value></value>
        public Color YColor { get { return m_UISettingsRef.YColor; } }

        /// <summary>
        /// Show UI
        /// </summary>
        public void showSettingsUI()
        {
            m_UISettingsRef.ShowUI();
        }

        #endregion
    }
}