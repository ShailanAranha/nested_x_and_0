﻿using NestedXn0.Manager.Hud;
using NestedXn0.Manager.MainGameUIManager;
using NestedXn0.Manager.Xn0Manager;
using NestedXn0.UI.Confirmation;
using Shailan.System.Ads;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal class UIInGameMenu : UIMenuBase
    {

        /// <summary>
        /// Game Manager ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Main Game UI Manager ref
        /// </summary>
        private IMainGameUIManager m_IMainGameUIManagerRef = null;
        private IMainGameUIManager IMainGameUIManagerRef
        {
            get
            {
                if (m_IMainGameUIManagerRef == null)
                {
                    m_IMainGameUIManagerRef = ManagersHandler.GetManager<IMainGameUIManager>();
                }
                return m_IMainGameUIManagerRef;
            }
        }

        /// <summary>
        /// Confirmation UI Ref
        /// </summary>
        [SerializeField]
        private UIConfirmation m_UIQuitConfirmationRef = null, m_UIMainMenuConfirmationRef = null;

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// Hud Manager instance ref
        /// </summary>
        private IHudManager m_IHudManagerRef = null;
        private IHudManager IHudManagerRef
        {
            get
            {
                if (m_IHudManagerRef == null)
                {
                    m_IHudManagerRef = ManagersHandler.GetManager<IHudManager>();
                }
                return m_IHudManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        /// <summary>
        /// Background image obj ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBg = null;

        /// <summary>
        /// Show UI
        /// </summary>
        internal override void ShowUI()
        {
            m_goBg.SetActive(true);
            base.ShowUI();
            Time.timeScale = 0;
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal override void HideUI()
        {
            base.HideUI();
            m_goBg.SetActive(false);
            Time.timeScale = 1;
        }

        /// <summary>
        /// Takes back to the game
        /// </summary>
        public void OnClick_ResumeGameBtn()
        {
            // Time.timeScale = 1;
            HideUI();
        }

        /// <summary>
        /// Opens confirmation
        /// </summary>
        public void OnClick_MainMenuBtn()
        {
            m_UIMainMenuConfirmationRef.ShowUI();
        }

        /// <summary>
        /// Redirects to Main menu
        /// </summary>
        public void OpenMainMenu()
        {
            HideUI();

            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                IMainGameUIManagerRef.ResetTraditinalXn0GameData();
            }
            else if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Nested))
            {
                IHudManagerRef.ZoomOutOfCurrentBlock();
                IMainGameUIManagerRef.ResetNestedXn0GameData();
            }
            else
            {
                Debug.LogError("UIEndGameMenu::EXn0Type not asigned!!!");
            }

            IMenuManagerRef.ShowMenu(MenuType.MainMenu);

            //PLAY AMBIENT MUSIC
            ISoundManagerRef.playAmbientMusic("a_amb_bensound_little_idea");

            //SHOW BANNER AD
            // IAdmobManagerRef.ShowAdBanner();
        }

        /// <summary>
        /// Opens saved games menu UI
        /// </summary>
        public void OnClick_SaveGameBtn()
        {

        }

        /// <summary>
        /// Opens how to play UI
        /// </summary>
        public void OnClick_HowToPlayBtn()
        {
            HideUI();
            IMenuManagerRef.showHowToPlayUI();
        }

        /// <summary>
        /// Opens setting menu UI
        /// </summary>
        public void OnClick_SettingsBtn()
        {
            HideUI();
            IMenuManagerRef.showSettingsUI();
        }

        /// <summary>
        /// shows confirmation box
        /// </summary>
        public void OnClick_ExitBtn()
        {
            m_UIQuitConfirmationRef.ShowUI();
        }

        /// <summary>
        /// Quits the application
        /// </summary>
        public void QuitGame()
        {
            Application.Quit();
        }

    }
}