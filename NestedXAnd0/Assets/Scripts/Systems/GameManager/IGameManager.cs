﻿using NestedXn0.Manager.Xn0Manager;
using Shailan.System.Manager;
using UnityEngine;

namespace Shailan.System.GameManager
{
    internal interface IGameManager : IManagerBase
    {
        /// <summary>
        /// Currently running or last ran Xn0 type game 
        /// </summary>
        /// <value></value>
        EXn0Type ECurrentXn0Type { get; set; }

        /// <summary>
        /// Main camera ref
        /// </summary>
        Camera MainCamRef { get; }

        /// <summary>
        /// Executes very first thing of the game (On application load)
        /// </summary>
        void OnApplicaionStart();
    }
}