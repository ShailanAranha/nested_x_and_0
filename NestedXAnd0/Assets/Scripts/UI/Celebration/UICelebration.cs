﻿using System;
using System.Threading.Tasks;
using NestedXn0.Manager.Hud;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Manager.Xn0Manager;
using NestedXn0.Menu;
using Shailan.System.Ads;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using Shailan.Tweening;
using UnityEngine;

namespace NestedXn0.UI.Celebration
{
    internal class UICelebration : MonoBehaviour
    {
        /// <summary>
        /// Player board UI rect ref
        /// </summary>
        [SerializeField]
        private RectTransform m_rectPlayerBoard = null;

        /// <summary>
        /// Draw TMP GO
        /// </summary>
        [SerializeField]
        private GameObject m_goDrawMessage = null;

        /// <summary>
        /// Crow image GO
        /// </summary>
        [SerializeField]
        private GameObject m_goCrown = null;

        /// <summary>
        /// Crow img gameobject reference
        /// </summary>
        [SerializeField]
        private RectTransform m_rectCrown = null;

        /// <summary>
        /// Player Board End Screen Position
        /// </summary>
        [SerializeField]
        private Vector2 m_vec2PlayerBoardPosition = Vector2.zero;

        /// <summary>
        /// Crown positions of respective player
        /// </summary>
        [SerializeField]
        private Vector2 m_vec2Player1CrownPosition = Vector2.zero, m_vec2Player2CrownPosition = Vector2.zero;

        /// <summary>
        /// Player Board Default position
        /// </summary>
        [SerializeField]
        private Vector2 m_vec2ResetPlayerBoardPos = Vector2.zero;

        /// <summary>
        /// Game over TMP canvas group ref
        /// </summary>
        [SerializeField]
        private CanvasGroup m_cavgrpGameOver = null;

        /// <summary>
        /// Winner's binary code
        /// </summary>
        private bool? m_isWinner = null;

        /// <summary>
        /// Player Mamager reference
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Hud Manager ref
        /// </summary>
        private IHudManager m_IHudManagerRef = null;
        private IHudManager IHudManagerRef
        {
            get
            {
                if (m_IHudManagerRef == null)
                {
                    m_IHudManagerRef = ManagersHandler.GetManager<IHudManager>();
                }
                return m_IHudManagerRef;
            }
        }

        /// <summary>
        /// Game Manager ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Menu Manager ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        private IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        /// <summary>
        /// background image reference
        /// </summary>
        [SerializeField]
        private GameObject m_goBackgroundImg = null;

        /// <summary>
        /// Initiates celebration UI
        /// </summary>
        /// <param name="a_winnerBinaryCode"></param>
        internal void startCelebration(bool? a_winnerBinaryCode)
        {
            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                IHudManagerRef.HideXn0Btns();
            }
            else
            {
                IHudManagerRef.HideNestedXn0Btns();
            }
            m_isWinner = a_winnerBinaryCode;
            m_goBackgroundImg.SetActive(true);
            gameObject.SetActive(true);
            MovePlayerBoard();
        }

        /// <summary>
        /// Move Player board to end screen postions
        /// </summary>
        private void MovePlayerBoard()
        {
            UITweening.MoveLerp(m_rectPlayerBoard, m_rectPlayerBoard.anchoredPosition, m_vec2PlayerBoardPosition, 1.2f, FadeInGameOverMsg);
        }

        /// <summary>
        /// Fade in Game Over TMP 
        /// </summary>
        private void FadeInGameOverMsg()
        {
            // UITweening.Fade(m_cavgrpGameOver.alpha, 0, 1, 2);
            Fade(0, 1, 1, ShowResults);
        }

        /// <summary>
        /// Fade canvas group alpha
        /// </summary>
        /// <param name="a_fltMinValue"></param>
        /// <param name="a_fltMaxValue"></param>
        /// <param name="a_fltFadeTime"></param>
        /// <param name="a_callBack"></param>
        /// <returns></returns>
        private async void Fade(float a_fltMinValue, float a_fltMaxValue, float a_fltFadeTime = 1.0f, Action a_callBack = null)
        {
            float l_fltCurrentTime = 0.0f;
            float l_fltNormalizedValue = 0.0f;
            float l_fltTimeOfTravel = a_fltFadeTime;
            while (l_fltCurrentTime <= l_fltTimeOfTravel)
            {
                l_fltCurrentTime += Time.deltaTime;
                l_fltNormalizedValue = l_fltCurrentTime / l_fltTimeOfTravel; // we normalize our time 
                m_cavgrpGameOver.alpha = Mathf.Lerp(a_fltMinValue, a_fltMaxValue, l_fltNormalizedValue);
                await Task.Yield();
            }
            a_callBack?.Invoke();
        }

        /// <summary>
        /// Display crown to the winner 
        /// </summary>
        private void ShowResults()
        {
            if (m_isWinner == null)
            {
                m_goDrawMessage.SetActive(true);
            }
            else
            {
                Player m_player = IPlayerManagerRef.getPlayerViaBinaryCode((bool)m_isWinner);

                if (m_player.EPlayerId.Equals(EPlayer.Player1))
                {
                    m_rectCrown.anchoredPosition = m_vec2Player1CrownPosition;
                    m_goCrown.SetActive(true);
                }
                else
                {
                    m_rectCrown.anchoredPosition = m_vec2Player2CrownPosition;
                    m_goCrown.SetActive(true);
                }
            }

            //  Delay(1.0f, IMenuManagerRef.PlayEndScreenBtnAnim);

            Delay(1.0f, () =>
            {
                // show interstitial ad
                if (IAdmobManagerRef.IsInterstitialAdLoaded)    // CHECK IF LOADED
                    IAdmobManagerRef.ShowAdInterstitial();      // SHOW IF LOADED 
                else
                    IAdmobManagerRef.LoadInterstitialAd();     // LOAD IF NOT LOADED

                 IMenuManagerRef.PlayEndScreenBtnAnim();
            });

            // IMenuManagerRef.PlayEndScreenBtnAnim();
        }

        /// <summary>
        /// Delay Executor
        /// </summary>
        /// <param name="a_fltDelayTime"></param>
        /// <param name="a_callback"></param>
        /// <returns></returns>
        private async void Delay(float a_fltDelayTime, Action a_callback)
        {
            await Task.Delay(TimeSpan.FromSeconds(a_fltDelayTime));
            a_callback.Invoke();
        }

        /// <summary>
        /// Reset all Data
        /// </summary>
        private void ResetData()
        {
            m_cavgrpGameOver.alpha = 0;
            m_goCrown.SetActive(false);
            m_goDrawMessage.SetActive(false);
            m_rectPlayerBoard.anchoredPosition = m_vec2ResetPlayerBoardPos;

            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                IHudManagerRef.ShowXn0Btns();
            }
            else
            {
                IHudManagerRef.ShowNestedXn0Btns();
            }

        }

        /// <summary>
        /// Reset and Hide UI
        /// </summary>
        internal void HideUI()
        {
            m_goBackgroundImg.SetActive(false);
            gameObject.SetActive(false);
            ResetData();
        }
    }

}