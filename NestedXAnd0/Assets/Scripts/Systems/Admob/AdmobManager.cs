﻿using System.Collections;
using System.Collections.Generic;
using Shailan.System.Manager;
using GoogleMobileAds.Api;
using GoogleMobileAds.Placement;
using UnityEngine;
using Firebase.Analytics;
using Shailan.System.Analytics;

namespace Shailan.System.Ads
{
    public class AdmobManager : ManagerBase, IAdmobManager
    {
        private BannerAdGameObject m_bannerAdObj = null;
        private InterstitialAdGameObject m_interstitialAdObj = null;
        public bool IsBannerAdLoaded { get; private set; } = false;
        public bool IsInterstitialAdLoaded { get; private set; } = false;
        public bool ShowBannerAdIfLoaded { get; set; } = true;

        private IFirebaseManager m_IFirebaseManagerRef = null;
        private IFirebaseManager IFirebaseManagerRef
        {
            get
            {
                if (m_IFirebaseManagerRef == null)
                {
                    m_IFirebaseManagerRef = ManagersHandler.GetManager<IFirebaseManager>();
                }
                return m_IFirebaseManagerRef;
            }
        }

        public bool IsPlayerUnderAged { get; set; } = true;
        private const string AGE_RESTRICTION_KEY = "IsPlayerUnderAged?";

        protected override void ManagerStart()
        {
            base.ManagerStart();
            // InitializeAdmob();
        }

        protected override void ManagerDestroy()
        {
            PlayerPrefs.SetString(AGE_RESTRICTION_KEY, IsPlayerUnderAged.ToString());
            PlayerPrefs.Save();

            DestroyBannerAds();
            DestroyInterstitialAd();
            base.ManagerDestroy();
        }

        public void InitializeAdmob()
        {
            if (PlayerPrefs.HasKey(AGE_RESTRICTION_KEY))
            {
                bool m_isPlayerUnderAged = false;
                if (!bool.TryParse(PlayerPrefs.GetString(AGE_RESTRICTION_KEY), out m_isPlayerUnderAged))
                {
                    Debug.LogError("UIPrivacyPolicy:: try parse failed:" + PlayerPrefs.GetString(AGE_RESTRICTION_KEY));
                }
                IsPlayerUnderAged = m_isPlayerUnderAged;
            }

            if (IsPlayerUnderAged)
            {
                RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
                   .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
                   .SetMaxAdContentRating(MaxAdContentRating.G)
                   .build();
                MobileAds.SetRequestConfiguration(requestConfiguration);

                Debug.LogError("AdmobManager: Admob initialized for under age player");
            }

            // AdRequest request = new AdRequest.Builder()
            //      .AddExtra("max_ad_content_rating", "G").TagForChildDirectedTreatment(true)
            //      .Build();

            MobileAds.Initialize((initStatus) =>
            {
                // SDK initialization is complete

            });

            m_bannerAdObj = MobileAds.Instance.GetAd<BannerAdGameObject>("Banner Ad");
            m_interstitialAdObj = MobileAds.Instance.GetAd<InterstitialAdGameObject>("Interstitial Ad");
        }

        #region BANNER AD
        public void LoadBannerAd()
        {
            if (!IFirebaseManagerRef.EnableBannerAd)
                return;

            // IsBannerAdLoaded = true;
            m_bannerAdObj.LoadAd();
        }

        public void ShowIfAdBanner()
        {
            if (!IFirebaseManagerRef.EnableBannerAd)
                return;

            if (!IsBannerAdLoaded)
                return;

            m_bannerAdObj.Show();
        }

        public void LoadIfNotNShowAdBanner()
        {
            if (!IFirebaseManagerRef.EnableBannerAd)
                return;

            if (IsBannerAdLoaded)
            {
                m_bannerAdObj.Show();
            }
            else
            {
                m_bannerAdObj.LoadAd();
            }
        }

        public void HideAdBanner()
        {
            if (!IFirebaseManagerRef.EnableBannerAd)
                return;

            if (!IsBannerAdLoaded)
                return;

            m_bannerAdObj.Hide();
        }

        public void SetLoadStatusOfBannerAd(bool a_value)
        {
            IsBannerAdLoaded = a_value;
            Debug.Log("IsBannerAdLoaded:" + IsBannerAdLoaded);
        }

        public void SetShowConditionOnBannerAdLoad()
        {
            if (!IFirebaseManagerRef.EnableBannerAd)
                return;

            if (!ShowBannerAdIfLoaded)
            {
                m_bannerAdObj.Hide();
            }
        }

        public void DestroyBannerAds()
        {
            m_bannerAdObj.DestroyAd();
        }

        public void OnClickBannerAd()
        {
            // Analytics
            FirebaseAnalytics.LogEvent("BannerAdClick");
        }

        #endregion

        #region INTERSTITIAL AD
        public void LoadInterstitialAd()
        {
            if (!IFirebaseManagerRef.EnableInterstitialAd)
                return;

            m_interstitialAdObj.LoadAd();
        }

        public void LoadInterstitialAdIfNotLoaded()
        {
            if (!IFirebaseManagerRef.EnableInterstitialAd)
                return;

            if (!IsInterstitialAdLoaded)
                m_interstitialAdObj.LoadAd();
        }

        public void ShowAdInterstitial()
        {
            if (!IFirebaseManagerRef.EnableInterstitialAd)
                return;

            m_interstitialAdObj.ShowIfLoaded();
        }

        public void SetLoadStatusOfInterstitialAd(bool a_value)
        {
            IsInterstitialAdLoaded = a_value;
            Debug.Log("IsInterstitialAdLoaded:" + IsInterstitialAdLoaded);
        }

        public void DestroyInterstitialAd()
        {
            m_interstitialAdObj.DestroyAd();
        }

        public void OnClickInterstitialAd()
        {
            // Analytics
            FirebaseAnalytics.LogEvent("InterstitialAdClick");
        }

        #endregion

        #region REWAREDED AD
        public void ShowAdRewaredVideo()
        {
            throw new global::System.NotImplementedException();
        }
        #endregion

    }
}
