﻿using System;
using NestedXn0.Manager.Hud;
using NestedXn0.Manager.Xn0Manager;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using Shailan.System.Sound;
using Shailan.Utils.DelayExecutor;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.WinMarkUI
{
    internal class WinMark : MonoBehaviour
    {
        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_animRef = null;

        /// <summary>
        /// Rect of the object
        /// </summary>
        [SerializeField]
        private RectTransform m_rectSelf = null;

        /// <summary>
        /// Mark Image 
        /// </summary>
        [SerializeField]
        private Image m_imgMark = null;

        /// <summary>
        /// On win mark animatin complete event
        /// </summary>
        internal event Action OnWinMarkCompleted = null;

        /// <summary>
        /// Hud Manager ref
        /// </summary>
        private IHudManager m_IHudManagerRef = null;
        private IHudManager IHudManagerRef
        {
            get
            {
                if (m_IHudManagerRef == null)
                {
                    m_IHudManagerRef = ManagersHandler.GetManager<IHudManager>();
                }
                return m_IHudManagerRef;
            }
        }

        /// <summary>
        /// Game Manager ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        private bool m_isDelay = false;

        /// <summary>
        /// On win mark animatin complete call
        /// </summary>
        private void WinMarkCompleted()
        {
            if (m_isDelay)
            {
                DelayExecutor l_obj = new DelayExecutor(1f, () =>
                {
                    OnWinMarkCompleted?.Invoke();
                    OnWinMarkCompleted = null;
                    gameObject.SetActive(false);
                    m_animRef.Stop();

                    //RESET
                    m_rectSelf.anchoredPosition = Vector2.zero;
                    m_rectSelf.eulerAngles = Vector3.zero;
                });
            }
            else
            {
                OnWinMarkCompleted?.Invoke();
                OnWinMarkCompleted = null;
                gameObject.SetActive(false);
                m_animRef.Stop();

                //RESET
                m_rectSelf.anchoredPosition = Vector2.zero;
                m_rectSelf.eulerAngles = Vector3.zero;
            }

        }

        /// <summary>
        /// play win animation via pattern id
        /// </summary>
        /// <param name="a_iPattern"></param>
        internal void WinMarkThePattern(int a_iPattern, bool a_isDelay)
        {
            // if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            // {
            //     IHudManagerRef.HideXn0Btns();
            // }
            // else
            // {
            //     IHudManagerRef.HideNestedXn0Btns();
            // }
            m_isDelay = a_isDelay;
            IHudManagerRef.HideXn0Btns();

            string l_strAnimName = "pattern" + a_iPattern;
            gameObject.SetActive(true);
            m_animRef.Play(l_strAnimName);
            // ISoundManagerRef.playSfx("a_sfx_victory");
            // Debug.LogError("called");
        }

        internal void WinMArkNestedPattern(int a_iPattern)
        {
            m_isDelay = true;
            IHudManagerRef.HideNestedXn0Btns();

            string l_strAnimName = "pattern" + a_iPattern;
            gameObject.SetActive(true);
            m_animRef.Play(l_strAnimName);
            // ISoundManagerRef.playSfx("a_sfx_victory");
            // Debug.LogError("called");
        }

        internal void ResetMark()
        {
            m_imgMark.fillAmount = 0.0f;
        }
    }
}