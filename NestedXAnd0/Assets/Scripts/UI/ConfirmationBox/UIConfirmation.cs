﻿using UnityEngine;

namespace NestedXn0.UI.Confirmation
{
    internal class UIConfirmation : MonoBehaviour
    {
        /// <summary>
        /// Show UI
        /// </summary>
        public void ShowUI()
        {
            gameObject.SetActive(true);
        }

         /// <summary>
        /// Hide UI
        /// </summary>
        public void HideUI()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// No btn click event
        /// </summary>
        public void onClick_NoBtn()
        {
            gameObject.SetActive(false);
        }
        
    }

}

