﻿using System;
using System.Threading.Tasks;
using NestedXn0.Menu;
using NestedXn0.UI.PlayerSelection;
using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Manager.PlayerManager
{
    internal class PlayerManager : ManagerBase, IPlayerManager
    {
        /// <summary>
        /// Player 1 Obj
        /// </summary>
        private Player m_Player1 = null;
        public Player Player1 { get { return m_Player1; } }

        /// <summary>
        /// Player 2 obj
        /// </summary>
        private Player m_Player2 = null;
        public Player Player2 { get { return m_Player2; } }

        /// <summary>
        ///  player who begins the game
        /// </summary>
        private EPlayer m_EStartPlayer = EPlayer.None;

        /// <summary>
        /// Current Bot type
        /// </summary>
        private EBotType m_ECurrentBotType = EBotType.None;
        public EBotType GetCurrentBotType { get { return m_ECurrentBotType; } }

        /// <summary>
        /// Is player 2 bot?
        /// </summary>
        private bool m_isPlayer2Bot = false;
        public bool IsPlayer2Bot { get { return m_isPlayer2Bot; } }

        /// <summary>
        /// Executes on bots turn (if player 2 is a bot)
        /// </summary>
        public event Action InitiateBotsTurn = null;

        /// <summary>
        /// Executes when the player turn changes
        /// </summary>
        public event Action<Player> OnCurrentPlayerChanged = null;

        /// <summary>
        /// Current players turn
        /// </summary>
        private Player m_CurrentPlayer = null;
        public Player CurrentPlayer
        {
            get
            {
                if (m_CurrentPlayer == null)
                {
                    Debug.LogError("PlayerManager:: player not assigned yet!! ");
                }
                return m_CurrentPlayer;
            }
            private set
            {
                //CHECK IF THE PLAYER 2 IS BOT AND THE TURN 
                // if (value.EPlayerId.Equals(EPlayer.Player2) && m_isPlayer2Bot)
                // {
                //     InitiateBotsTurn.Invoke();
                // }
                m_CurrentPlayer = value;
            }
        }

        /// <summary>
        /// Menu manager ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// X n 0 sprite
        /// </summary>
        [SerializeField]
        private Sprite m_sprX = null, m_spr0 = null;

        /// <summary>
        /// Player selection UI ref
        /// </summary>
        [SerializeField]
        private UIPlayerSelection m_UIPLayerSelectionRef = null;

        /// <summary>
        /// PLayer 1 Id
        /// </summary>
        internal const string DEFAULT_PLAYER1_NAME = "PLAYER 1";

        /// <summary>
        /// Player 2 Id
        /// </summary>
        internal const string DEFAULT_PLAYER2_NAME = "PLAYER 2";

        /// <summary>
        /// Bot Id
        /// </summary>
        internal const string DEFAULT_COMP_NAME = "COMP";

        /// <summary>
        /// Create player Objects
        /// </summary>
        public void CreatePlayers()
        {
            // m_Player1 = new Player(EPlayer.Player1, DEFAULT_PLAYER1_NAME, true, m_sprX);
            // m_Player2 = new Player(EPlayer.Player2, DEFAULT_PLAYER2_NAME, false, m_spr0);

            bool l_p1binaryCode = m_UIPLayerSelectionRef.getPlayer1BinaryCode; // caching
            bool l_p2binaryCode = m_UIPLayerSelectionRef.getPlayer2BinaryCode; // caching
            m_Player1 = new Player(EPlayer.Player1, m_UIPLayerSelectionRef.getPlayer1NickName, l_p1binaryCode, getRequiredSprite(l_p1binaryCode));
            m_Player2 = new Player(EPlayer.Player2, m_UIPLayerSelectionRef.getPlayer2NickName, l_p2binaryCode, getRequiredSprite(l_p2binaryCode));

            // ASSIGN SPRITE COLOR FOR PLAYER 1
            if (l_p1binaryCode)
                m_Player1.UpdateColor(IMenuManagerRef.XColor);
            else
                m_Player1.UpdateColor(IMenuManagerRef.YColor);

            // ASSIGN SPRITE COLOR FOR PLAYER 2
            if (l_p2binaryCode)
                m_Player2.UpdateColor(IMenuManagerRef.XColor);
            else
                m_Player2.UpdateColor(IMenuManagerRef.YColor);
        }

        /// <summary>
        /// Get required(x,0) sprite via binary code
        /// </summary>
        /// <param name="a_binaryCode"></param>
        /// <returns></returns>
        private Sprite getRequiredSprite(bool a_binaryCode)
        {
            if (a_binaryCode)
                return m_sprX;
            else
                return m_spr0;
        }

        /// <summary>
        /// Update player name
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        /// <param name="a_strName"></param>
        public void UpdatePlayerName(EPlayer a_EPlayerId, string a_strName)
        {
            if (m_Player1.EPlayerId.Equals(a_EPlayerId))
            {
                m_Player1.UpdateName(a_strName);
            }
            else
            {
                m_Player2.UpdateName(a_strName);
            }
        }

        /// <summary>
        /// Assign player turn via player id
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        public void AssignPlayerTurn(EPlayer a_EPlayerId)
        {
            if (m_Player1.EPlayerId.Equals(a_EPlayerId))
            {
                CurrentPlayer = m_Player1;
            }
            else
            {
                CurrentPlayer = m_Player2;
                if (m_isPlayer2Bot)
                {
                    InitiateBotsTurn?.Invoke();
                }
            }

            OnCurrentPlayerChanged?.Invoke(CurrentPlayer);
        }

        /// <summary>
        ///  Assign player turn via player id with delay
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        /// <param name="a_fltTIme"></param>
        /// <returns></returns>
        public async void AssignPlayerTurnWithDelay(EPlayer a_EPlayerId, float a_fltTIme)
        {
            await Task.Delay(TimeSpan.FromSeconds(a_fltTIme));
            AssignPlayerTurn(a_EPlayerId);
        }

        /// <summary>
        ///  Assign player turn via player id with delay
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        /// <param name="a_fltTIme"></param>
        /// <returns></returns>
        public async void AssignPlayerTurnWithDelay(EPlayer a_EPlayerId, float a_fltTIme, Action a_callBack)
        {
            await Task.Delay(TimeSpan.FromSeconds(a_fltTIme));
            AssignPlayerTurn(a_EPlayerId);
            a_callBack?.Invoke();
        }

        /// <summary>
        /// Swicth current player turn
        /// </summary>
        public void SwitchPlayerTurn()
        {
            if (CurrentPlayer.EPlayerId.Equals(EPlayer.Player1))
            {
                CurrentPlayer = m_Player2;
                if (m_isPlayer2Bot)
                {
                    InitiateBotsTurn?.Invoke();
                }
            }
            else
            {
                CurrentPlayer = m_Player1;
            }
            OnCurrentPlayerChanged?.Invoke(CurrentPlayer);
        }

        /// <summary>
        /// Get requied player via binary code 
        /// </summary>
        /// <param name="a_binaryCode"></param>
        /// <returns></returns>
        public Player getPlayerViaBinaryCode(bool a_binaryCode)
        {
            if (m_Player1.BinaryCode.Equals(a_binaryCode))
            {
                return m_Player1;
            }
            else
            {
                return m_Player2;
            }
        }

        /// <summary>
        /// set player 2 status as bot or player
        /// </summary>
        /// <param name="a_isPlayer2Bot"></param>
        public void SetPlayer2Status(bool a_isPlayer2Bot)
        {
            m_isPlayer2Bot = a_isPlayer2Bot;
        }

        /// <summary>
        /// Assign first turn to play
        /// </summary>
        /// <returns></returns>
        public EPlayer GetStartPlayer()
        {
            if (m_EStartPlayer.Equals(EPlayer.None))
            {
                m_EStartPlayer = EPlayer.Player1;
            }
            else if (m_EStartPlayer.Equals(EPlayer.Player1))
            {
                m_EStartPlayer = EPlayer.Player2;
            }
            else
            {
                m_EStartPlayer = EPlayer.Player1;
            }

            return m_EStartPlayer;
        }

        /// <summary>
        /// Set bot type
        /// </summary>
        /// <param name="a_Etype"></param>
        public void SetCurrentBotType(EBotType a_Etype)
        {
            m_ECurrentBotType = a_Etype;
        }

        /// <summary>
        /// Resets all  player data
        /// </summary>
        public void ResetPlayerData()
        {
            m_Player1 = null;
            m_Player2 = null;
            CurrentPlayer = null;
            m_isPlayer2Bot = false;
            m_EStartPlayer = EPlayer.None;
            m_ECurrentBotType = EBotType.None;
        }
    }

    internal class Player
    {
        /// <summary>
        /// Player Unique Id Using types
        /// </summary>
        private EPlayer m_EPlayerId;
        internal EPlayer EPlayerId { get { return m_EPlayerId; } }

        /// <summary>
        /// Player name
        /// </summary>
        private string m_strName = null;
        internal string PlayerName { get { return m_strName; } }

        /// <summary>
        /// Player Binary Code where X=true amd 0=false
        /// </summary>
        private bool m_binaryCode = false;
        internal bool BinaryCode { get { return m_binaryCode; } }

        /// <summary>
        /// Binary code Icon
        /// </summary>
        private Sprite m_sprBinaryCodeSprite = null;
        internal Sprite BinaryCodeSprite { get { return m_sprBinaryCodeSprite; } }

        /// <summary>
        /// Binary code icon color
        /// </summary>
        private Color m_colSpriteColor = Color.white;
        internal Color SpriteColor { get { return m_colSpriteColor; } }

        /// <summary>
        /// Param constructor to fill mandatory data during object creation 
        /// </summary>
        /// <param name="a_id"></param>
        /// <param name="a_strName"></param>
        /// <param name="a_binaryCode"></param>
        /// <param name="a_sprBinaryCodeSprite"></param>
        internal Player(EPlayer a_id, string a_strName, bool a_binaryCode, Sprite a_sprBinaryCodeSprite)
        {
            m_EPlayerId = a_id;
            m_strName = a_strName;
            m_binaryCode = a_binaryCode;
            m_sprBinaryCodeSprite = a_sprBinaryCodeSprite;
        }

        /// <summary>
        /// Update Player name
        /// </summary>
        /// <param name="a_strName"></param>
        internal void UpdateName(string a_strName)
        {
            m_strName = a_strName;
        }

        /// <summary>
        ///  Update Binary code icon color
        /// </summary>
        /// <param name="m_col"></param>
        internal void UpdateColor(Color m_col)
        {
            m_colSpriteColor = m_col;
        }
    }

    /// <summary>
    /// Player Types
    /// </summary>
    internal enum EPlayer : byte
    {
        None = 0, Player1 = 1, Player2 = 2
    }

    /// <summary>
    /// Bot type
    /// </summary>
    internal enum EBotType : byte
    {
        None = 0, Beginner = 1, Regular = 2, Pro = 3
    }
}