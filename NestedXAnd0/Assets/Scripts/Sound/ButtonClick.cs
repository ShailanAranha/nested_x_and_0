﻿using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;

namespace NestedXn0.Sound.ButtonClick
{
    internal class ButtonClick : MonoBehaviour
    {
        /// <summary>
        /// Sound manager instance ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        ///  Generic sfx for button click
        /// </summary>
        public void genericBtnClick()
        {
            ISoundManagerRef.playSfx("a_btn_click");
        }

        /// <summary>
        /// Play sfx via sfx Id
        /// </summary>
        /// <param name="a_sfxId"></param>
        public void PlaySfx(string a_sfxId)
        {
            ISoundManagerRef.playSfx(a_sfxId);
        }

    }
}