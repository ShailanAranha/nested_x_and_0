﻿using System.Collections.Generic;
using UnityEngine;

namespace Shailan.System.Manager
{
    public class ManagersHandler : MonoBehaviour
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static ManagersHandler s_instance = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        public ManagersHandler()
        {
            s_instance = this;
        }

        /// <summary>
        /// Finalizer (Destructor)
        /// </summary>
        ~ManagersHandler()
        {
            if (m_isDestroyed)
                s_instance = null;
        }

        /// <summary>
        /// List of Managers
        /// </summary>
        /// <typeparam name="IManagerBase"></typeparam>
        /// <returns></returns>
        private List<IManagerBase> m_lstRegisteredManagers = new List<IManagerBase>(10);

        /// <summary>
        /// Is OnDestroy Called?
        /// </summary>
        private bool m_isDestroyed = false;

        [SerializeField]
        private bool m_isDebug = false;

        private void OnDestroy()
        {
            m_isDestroyed = true;
        }

        /// <summary>
        /// For registering a Manager
        /// </summary>
        /// <param name="a_IManagerBase"></param>
        public static void RegisterManager(IManagerBase a_IManagerBase)
        {
            if (s_instance.m_lstRegisteredManagers.Contains(a_IManagerBase))
            {
                Debug.LogError("ManagersHandler:: Manager already registered:" + a_IManagerBase.ToString());
                return;
            }

            if (s_instance.m_isDebug)
                Debug.Log("ManagersHandler:: Manager registered:" + a_IManagerBase.ToString());

            s_instance.m_lstRegisteredManagers.Add(a_IManagerBase);
        }

        /// <summary>
        /// For unregistering a Manager
        /// </summary>
        /// <param name="a_IManagerBase"></param>
        public static void UnregisterManager(IManagerBase a_IManagerBase)
        {
            if (!s_instance.m_lstRegisteredManagers.Contains(a_IManagerBase))
            {
                Debug.LogError("ManagersHandler:: does not conatin manager:" + a_IManagerBase.ToString());
                return;
            }

            if (s_instance.m_isDebug)
                Debug.Log("ManagersHandler:: Manager Unregistered:" + a_IManagerBase.ToString());

            s_instance.m_lstRegisteredManagers.Remove(a_IManagerBase);
        }

        /// <summary>
        /// Get Required Manager
        /// </summary>
        /// <typeparam name="MANAGER_TYPE"></typeparam>
        /// <returns></returns>
        public static MANAGER_TYPE GetManager<MANAGER_TYPE>() where MANAGER_TYPE : IManagerBase
        {
            List<IManagerBase> l_lstManagers = s_instance.m_lstRegisteredManagers;
            int l_iCount = l_lstManagers.Count;
            for (int i = 0; i < l_iCount; i++)
            {
                if (l_lstManagers[i] is MANAGER_TYPE)
                {
                    //   l_lstManagers[i] implementes MANAGER_TYPE  

                    if (s_instance.m_isDebug)
                        Debug.Log("ManagersHandler:: Manager called:" + ((MANAGER_TYPE)l_lstManagers[i]).ToString());

                    return (MANAGER_TYPE)l_lstManagers[i];
                }
            }

            Debug.LogError("ManagersHandler:: Manager not found:" + typeof(MANAGER_TYPE));
            return default(MANAGER_TYPE);
        }
    }
}