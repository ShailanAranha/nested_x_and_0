﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Shailan.Tweening
{
    public class UITweening : MonoBehaviour
    {
        /// <summary>
        /// To move UI Object from point A to B, Smoothly
        /// </summary>
        /// <param name="a_rectSrc"></param>
        /// <param name="a_rectDest"></param>
        public async static void MoveLerp(RectTransform a_rectUIObj, Vector2 a_vecSrc, Vector2 a_vecDest, float a_fltTimeOfTravel = 1.0f, Action a_callBack = null)
        {
            float l_fltCurrentTime = 0.0f;
            float l_fltNormalizedValue = 0.0f;
            float l_fltTimeOfTravel = a_fltTimeOfTravel;
            while (l_fltCurrentTime <= l_fltTimeOfTravel)
            {
                l_fltCurrentTime += Time.deltaTime;
                l_fltNormalizedValue = l_fltCurrentTime / l_fltTimeOfTravel; // we normalize our time 
                a_rectUIObj.anchoredPosition = Vector2.Lerp(a_vecSrc, a_vecDest, l_fltNormalizedValue);
                await Task.Yield();
            }
            a_callBack?.Invoke();
        }

        /// <summary>
        /// To Rotate UI Object, Smoothly
        /// </summary>
        /// <param name="a_rectUIObj"></param>
        /// <param name="a_vecSrc"></param>
        /// <param name="a_vecDest"></param>
        /// <param name="a_fltTimeOfTravel"></param>
        /// <param name="a_callBack"></param>
        public async static void RotateLerp(RectTransform a_rectUIObj, Vector2 a_vecSrc, Vector2 a_vecDest, float a_fltTimeOfTravel = 1.0f, Action a_callBack = null)
        {
            float l_fltCurrentTime = 0.0f;
            float l_fltNormalizedValue = 0.0f;
            float l_fltTimeOfTravel = a_fltTimeOfTravel;
            while (l_fltCurrentTime <= l_fltTimeOfTravel)
            {
                l_fltCurrentTime += Time.deltaTime;
                l_fltNormalizedValue = l_fltCurrentTime / l_fltTimeOfTravel; // we normalize our time 
                a_rectUIObj.eulerAngles = Vector2.Lerp(a_vecSrc, a_vecDest, l_fltNormalizedValue);
                await Task.Yield();
            }
            a_callBack?.Invoke();
        }

        /// <summary>
        /// To Rotate UI Object, Smoothly
        /// </summary>
        /// <param name="a_rectUIObj"></param>
        /// <param name="a_vecSrc"></param>
        /// <param name="a_vecDest"></param>
        /// <param name="a_fltTimeOfTravel"></param>
        /// <param name="a_callBack"></param>
        //NOT TESTED
        public async static void ScaleLerp(RectTransform a_rectUIObj, Vector2 a_vecSrc, Vector2 a_vecDest, float a_fltTimeOfTravel = 1.0f, Action a_callBack = null)
        {
            float l_fltCurrentTime = 0.0f;
            float l_fltNormalizedValue = 0.0f;
            float l_fltTimeOfTravel = a_fltTimeOfTravel;
            while (l_fltCurrentTime <= l_fltTimeOfTravel)
            {
                l_fltCurrentTime += Time.deltaTime;
                l_fltNormalizedValue = l_fltCurrentTime / l_fltTimeOfTravel; // we normalize our time 
                a_rectUIObj.localScale = Vector2.Lerp(a_vecSrc, a_vecDest, l_fltNormalizedValue);
                await Task.Yield();
            }
            a_callBack?.Invoke();
        }

        // NOT CURRENT WORKING
        public async static void Fade(float a_fltObj, float a_fltMinValue, float a_fltMaxValue, float a_fltFadeTime = 1.0f, Action a_callBack = null)
        {
            float l_fltCurrentTime = 0.0f;
            float l_fltNormalizedValue = 0.0f;
            float l_fltTimeOfTravel = a_fltFadeTime;
            while (l_fltCurrentTime <= l_fltTimeOfTravel)
            {
                l_fltCurrentTime += Time.deltaTime;
                l_fltNormalizedValue = l_fltCurrentTime / l_fltTimeOfTravel; // we normalize our time 
                a_fltObj = Mathf.Lerp(a_fltMinValue, a_fltMaxValue, l_fltNormalizedValue);
                Debug.LogError(a_fltObj);
                await Task.Yield();
            }
            a_callBack?.Invoke();
        }
    }

}