﻿using System;
using System.Collections.Generic;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Manager.Xn0Manager;
using Shailan.System.Manager;
using NestedXn0.UI;
using UnityEngine;

namespace NestedXn0.Traditional
{
    internal class TraditionalXn0Handler : MonoBehaviour
    {
        /// <summary>
        /// List of all not possible patterns
        /// </summary>
        private List<int> m_lstDisposedPatterns = null;

        /// <summary>
        /// Xn0 Manager instance ref 
        /// </summary>
        private IXn0Manager m_IXn0ManagerRef = null;
        private IXn0Manager IXn0ManagerRef
        {
            get
            {
                if (m_IXn0ManagerRef == null)
                {
                    m_IXn0ManagerRef = ManagersHandler.GetManager<IXn0Manager>();
                }
                return m_IXn0ManagerRef;
            }
        }

        /// <summary>
        /// Player Manager Instance ref
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Is debug?
        /// </summary>
        [SerializeField]
        protected bool m_isDebug = false;

        private void Awake()
        {
            m_lstDisposedPatterns = new List<int>();
        }

        /// <summary>
        /// Check if the player input makes a wining pattern
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <param name="a_arrPattern"></param>
        /// <param name="a_result"></param>
        /// <returns></returns>
        public bool VerifyPattern(UIBlock[] a_arrBlocks, Pattern[] a_arrPattern, out Pattern a_result)
        {
            a_result = null;
            int l_iCount = a_arrPattern.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                // CHECK IF THE PATTERN IS ALREADY DIPOSED
                if (m_lstDisposedPatterns.Contains(a_arrPattern[i].PatternId))
                {
                    continue;
                }

                //CONVERT TO INT ARRAY
                int[] l_arrDesign = IXn0ManagerRef.getPatternDesignArray(a_arrPattern[i].Design);

                // GET BINARY CODES OF ALL DESIGN VALUES
                bool?[] l_arrDesignBinaryValues = IXn0ManagerRef.getBinaryCodeOfPattern(l_arrDesign, a_arrBlocks);

                //CURRENT PLAYER
                Player m_CurrentPlayer = IPlayerManagerRef.CurrentPlayer;

                // CHECK IF ALL INPUT BINARY CODE MATCHES WITH THE CURRENT PLAYER'S BINARY CODE
                if (IXn0ManagerRef.IsInputCodeMatchWithPlayerBInaryCodes(l_arrDesignBinaryValues, m_CurrentPlayer.BinaryCode))
                {
                    //WINNING PATTERN
                    a_result = a_arrPattern[i];

                    if (m_isDebug)
                    {
                        Debug.Log("Xn0Manager:: Winner achieved:" + a_result.PatternId + " Player:" + m_CurrentPlayer.PlayerName);
                    }
                    return true;
                }
                else
                {
                    // NO WINNER YET

                    //DISPOSE PATTERN CHECK
                    if (IXn0ManagerRef.IsDisposePattern(l_arrDesignBinaryValues, m_CurrentPlayer.BinaryCode))
                    {
                        m_lstDisposedPatterns.Add(a_arrPattern[i].PatternId);
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Reset data
        /// </summary>
        internal void ResetData()
        {
            m_lstDisposedPatterns?.Clear();
        }

        #region BOT

        /// <summary>
        /// Generate bots input
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <param name="a_arrPattern"></param>
        /// <param name="a_arrCurrentPlayersPreviousInputPatternCache"></param>
        /// <returns></returns>
        internal int GetBotsInputBlock(UIBlock[] a_arrBlocks, Pattern[] a_arrPattern, Pattern[] a_arrCurrentPlayersPreviousInputPatternCache)
        {
            switch (IPlayerManagerRef.GetCurrentBotType)
            {
                case EBotType.Beginner:
                    return GetBeginnerBotsInputBlock(a_arrBlocks);

                case EBotType.Regular:
                    return GetRegularBotsInputBlock(a_arrBlocks, a_arrPattern, a_arrCurrentPlayersPreviousInputPatternCache);

                case EBotType.Pro:
                    return GetProBotsInputBlock(a_arrBlocks, a_arrPattern, a_arrCurrentPlayersPreviousInputPatternCache);

                default:
                    Debug.LogError("TraditionalXn0Handler:: bot not assigned yet!!");
                    return -1;
            }
        }


        /// <summary>
        ///  Generate beginner bots input
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        private int GetBeginnerBotsInputBlock(UIBlock[] a_arrBlocks)
        {
            //GENERATE LIST OF PREFERABLE BOT INPUT
            List<int> l_lstBotInputBlockIds = new List<int>();

            // CHECK IF ALL PATTERNS ARE DISPOSED 
            int l_iBlockCount = a_arrBlocks.Length;
            for (int i = 0; i < l_iBlockCount; i++)
            {
                if (!a_arrBlocks[i].IsMarked)
                {
                    l_lstBotInputBlockIds.Add(a_arrBlocks[i].Id);
                }
            }

            //CHOOSE ONE RANDOMIZED BLOCK ID
            return IXn0ManagerRef.GetRandomizedNumber(l_lstBotInputBlockIds);
        }

        /// <summary>
        ///  Generate regular bots input
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <param name="a_arrPattern"></param>
        /// <param name="a_arrCurrentPlayersPreviousInputPatternCache"></param>
        /// <returns></returns>
        private int GetRegularBotsInputBlock(UIBlock[] a_arrBlocks, Pattern[] a_arrPattern, Pattern[] a_arrCurrentPlayersPreviousInputPatternCache)
        {
            //GENERATE LIST OF PREFERABLE BOT INPUT
            List<int> l_lstBotInputBlockIds = new List<int>();

            //CURRENT PLAYER
            Player m_CurrentPlayer = IPlayerManagerRef.CurrentPlayer;

            if (a_arrCurrentPlayersPreviousInputPatternCache != null)
            {
                int l_CurrentPlayersPreviousInputPatternCount = a_arrCurrentPlayersPreviousInputPatternCache.Length;
                for (int i = 0; i < l_CurrentPlayersPreviousInputPatternCount; i++)
                {
                    // Debug.LogError();
                    // CHECK IF THE PATTERN IS ALREADY DIPOSED
                    if (m_lstDisposedPatterns.Contains(a_arrCurrentPlayersPreviousInputPatternCache[i].PatternId))
                    {
                        continue;
                    }

                    //CONVERT TO INT ARRAY
                    int[] l_arrDesign = IXn0ManagerRef.getPatternDesignArray(a_arrCurrentPlayersPreviousInputPatternCache[i].Design);

                    // GET BINARY CODES OF ALL DESIGN VALUES
                    bool?[] l_arrDesignBinaryValues = IXn0ManagerRef.getBinaryCodeOfPattern(l_arrDesign, a_arrBlocks);

                    // CHECK IF THERE IS A POSSIBLE WIN PATTERN TO COMPLETE(FOR BOT)
                    int l_iWinningBlockId = -1;
                    if (IXn0ManagerRef.CheckForWinnablePattern(l_arrDesignBinaryValues, m_CurrentPlayer.BinaryCode, l_arrDesign, out l_iWinningBlockId))
                    {
                        if (m_isDebug)
                            Debug.Log("TraditionalXn0Handler:: bots winning pattern found:" + l_iWinningBlockId);

                        return l_iWinningBlockId;
                    }
                }
            }

            if (a_arrPattern != null)
            {
                int l_iCount = a_arrPattern.Length;
                for (int i = 0; i < l_iCount; i++)
                {
                    // CHECK IF THE PATTERN IS ALREADY DIPOSED
                    if (m_lstDisposedPatterns.Contains(a_arrPattern[i].PatternId))
                    {
                        continue;
                    }

                    //CONVERT TO INT ARRAY
                    int[] l_arrDesign = IXn0ManagerRef.getPatternDesignArray(a_arrPattern[i].Design);

                    // GET BINARY CODES OF ALL DESIGN VALUES
                    bool?[] l_arrDesignBinaryValues = IXn0ManagerRef.getBinaryCodeOfPattern(l_arrDesign, a_arrBlocks);

                    // CHECK IF THERE IS A POSSIBLE WIN PATTERN TO COMPLETE(FOR OPPOSITE PLAYER)
                    int l_iEnemyPlayersWinningBlockId = -1;
                    if (IXn0ManagerRef.CheckForWinnablePattern(l_arrDesignBinaryValues, !m_CurrentPlayer.BinaryCode, l_arrDesign, out l_iEnemyPlayersWinningBlockId))
                    {
                        if (m_isDebug)
                            Debug.Log("TraditionalXn0Handler:: enemy's winning pattern found:" + l_iEnemyPlayersWinningBlockId);

                        return l_iEnemyPlayersWinningBlockId;
                    }

                    // ADD ALL NULL VALUE(NOT ENTERED) TO THE LIST
                    for (int j = 0; j < l_arrDesignBinaryValues.Length; j++)
                    {
                        if (l_arrDesignBinaryValues[j] == null)
                        {
                            int l_iBlockId = l_arrDesign[j];
                            if (!l_lstBotInputBlockIds.Contains(l_iBlockId))
                            {
                                l_lstBotInputBlockIds.Add(l_iBlockId);
                            }
                        }
                    }
                }
            }

            // CHECK IF ALL PATTERNS ARE DISPOSED 
            int l_iBlockCount = a_arrBlocks.Length;
            for (int i = 0; i < l_iBlockCount; i++)
            {
                if (!a_arrBlocks[i].IsMarked)
                {
                    l_lstBotInputBlockIds.Add(a_arrBlocks[i].Id);
                }
            }

            //CHOOSE ONE RANDOMIZED BLOCK ID
            return IXn0ManagerRef.GetRandomizedNumber(l_lstBotInputBlockIds);
        }

        /// <summary>
        ///  Generate pro bots input
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <param name="a_arrPattern"></param>
        /// <param name="a_arrCurrentPlayersPreviousInputPatternCache"></param>
        /// <returns></returns>
        private int GetProBotsInputBlock(UIBlock[] a_arrBlocks, Pattern[] a_arrPattern, Pattern[] a_arrCurrentPlayersPreviousInputPatternCache)
        {
            //GENERATE LIST OF PREFERABLE BOT INPUT
            List<int> l_lstBotInputBlockIds = new List<int>();

            //CURRENT PLAYER
            Player m_CurrentPlayer = IPlayerManagerRef.CurrentPlayer;

            // GET FIRST INPUT
            int l_iFirstInput = GetBotsFirstInput(a_arrBlocks);
            if (l_iFirstInput != -1)
                return l_iFirstInput;

            // CHECK IF THERE IS A WINNABLE PATTERN
            if (a_arrCurrentPlayersPreviousInputPatternCache != null)
            {
                int l_CurrentPlayersPreviousInputPatternCount = a_arrCurrentPlayersPreviousInputPatternCache.Length;
                for (int i = 0; i < l_CurrentPlayersPreviousInputPatternCount; i++)
                {
                    // Debug.LogError();
                    // CHECK IF THE PATTERN IS ALREADY DIPOSED
                    if (m_lstDisposedPatterns.Contains(a_arrCurrentPlayersPreviousInputPatternCache[i].PatternId))
                    {
                        continue;
                    }

                    //CONVERT TO INT ARRAY
                    int[] l_arrDesign = IXn0ManagerRef.getPatternDesignArray(a_arrCurrentPlayersPreviousInputPatternCache[i].Design);

                    // GET BINARY CODES OF ALL DESIGN VALUES
                    bool?[] l_arrDesignBinaryValues = IXn0ManagerRef.getBinaryCodeOfPattern(l_arrDesign, a_arrBlocks);

                    // CHECK IF THERE IS A POSSIBLE WIN PATTERN TO COMPLETE(FOR BOT)
                    int l_iWinningBlockId = -1;
                    if (IXn0ManagerRef.CheckForWinnablePattern(l_arrDesignBinaryValues, m_CurrentPlayer.BinaryCode, l_arrDesign, out l_iWinningBlockId))
                    {
                        if (m_isDebug)
                            Debug.Log("TraditionalXn0Handler:: bots winning pattern found:" + l_iWinningBlockId);

                        return l_iWinningBlockId;
                    }
                }
            }

            // GENERATE INPUT FROM PLAYER CURRENT INPUT
            if (a_arrPattern != null)
            {
                int l_iCount = a_arrPattern.Length;
                for (int i = 0; i < l_iCount; i++)
                {
                    // CHECK IF THE PATTERN IS ALREADY DIPOSED
                    if (m_lstDisposedPatterns.Contains(a_arrPattern[i].PatternId))
                    {
                        continue;
                    }

                    //CONVERT TO INT ARRAY
                    int[] l_arrDesign = IXn0ManagerRef.getPatternDesignArray(a_arrPattern[i].Design);

                    // GET BINARY CODES OF ALL DESIGN VALUES
                    bool?[] l_arrDesignBinaryValues = IXn0ManagerRef.getBinaryCodeOfPattern(l_arrDesign, a_arrBlocks);

                    // CHECK IF THERE IS A POSSIBLE WIN PATTERN TO COMPLETE(FOR OPPOSITE PLAYER)
                    int l_iEnemyPlayersWinningBlockId = -1;
                    if (IXn0ManagerRef.CheckForWinnablePattern(l_arrDesignBinaryValues, !m_CurrentPlayer.BinaryCode, l_arrDesign, out l_iEnemyPlayersWinningBlockId))
                    {
                        if (m_isDebug)
                            Debug.Log("TraditionalXn0Handler:: enemy's winning pattern found:" + l_iEnemyPlayersWinningBlockId);

                        return l_iEnemyPlayersWinningBlockId;
                    }

                    // ADD ALL NULL VALUE(NOT ENTERED) TO THE LIST
                    for (int j = 0; j < l_arrDesignBinaryValues.Length; j++)
                    {
                        if (l_arrDesignBinaryValues[j] == null)
                        {
                            int l_iBlockId = l_arrDesign[j];
                            if (!l_lstBotInputBlockIds.Contains(l_iBlockId))
                            {
                                l_lstBotInputBlockIds.Add(l_iBlockId);
                            }
                        }
                    }
                }
            }

            // CHECK IF ALL PATTERNS ARE DISPOSED 
            int l_iBlockCount = a_arrBlocks.Length;
            for (int i = 0; i < l_iBlockCount; i++)
            {
                if (!a_arrBlocks[i].IsMarked)
                {
                    l_lstBotInputBlockIds.Add(a_arrBlocks[i].Id);
                }
            }

            //CHOOSE ONE RANDOMIZED BLOCK ID
            return IXn0ManagerRef.GetRandomizedNumber(l_lstBotInputBlockIds);
        }

        /// <summary>
        /// If bots plays the beginning turn
        /// </summary>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        int GetBotsFirstInput(UIBlock[] a_arrBlocks)
        {
            int l_iBlockCount = a_arrBlocks.Length;
            int l_iCounter = 0;
            int l_iPlayersInput = -1;
            for (int i = 0; i < l_iBlockCount; i++)
            {
                if (a_arrBlocks[i].IsMarked)
                {
                    l_iPlayersInput = a_arrBlocks[i].Id;
                    l_iCounter++;
                }
            }

            if (l_iCounter == 0)
            {
                return 5;
            }

            if (l_iCounter != 1)
            {
                return -1;
            }

            if (l_iPlayersInput == 1 || l_iPlayersInput == 3 || l_iPlayersInput == 7 || l_iPlayersInput == 9)
            {
                return 5;
            }
            else if (l_iPlayersInput == 5)
            {
                int[] l_arrInputs = { 2, 4, 6, 8 };

                int l_iRandomizedInput = UnityEngine.Random.Range(0, l_arrInputs.Length);
                return l_arrInputs[l_iRandomizedInput];
            }

            return -1;

        }
        #endregion
    }
}