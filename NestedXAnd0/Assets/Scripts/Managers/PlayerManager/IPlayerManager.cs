﻿using System;
using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Manager.PlayerManager
{
    internal interface IPlayerManager : IManagerBase
    {
        /// <summary>
        /// Player 1 Obj
        /// </summary>
        Player Player1 { get; }

        /// <summary>
        /// Player 2 obj
        /// </summary>
        Player Player2 { get; }

        /// <summary>
        /// Current players turn
        /// </summary>
        Player CurrentPlayer { get; }

        /// <summary>
        /// Is player 2 bot?
        /// </summary>
        bool IsPlayer2Bot { get; }

        /// <summary>
        /// Current Bot type
        /// </summary>
        EBotType GetCurrentBotType { get; }

        /// <summary>
        /// Create player Objects
        /// </summary>
        void CreatePlayers();

        /// <summary>
        /// Update player name
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        /// <param name="a_strName"></param>
        void UpdatePlayerName(EPlayer a_EPlayerId, string a_strName);

        /// <summary>
        /// Get requied player via binary code 
        /// </summary>
        /// <param name="a_binaryCode"></param>
        /// <returns></returns>
        Player getPlayerViaBinaryCode(bool a_binaryCode);

        /// <summary>
        /// Swicth current player turn
        /// </summary>
        void SwitchPlayerTurn();

        /// <summary>
        /// Assign player turn via player id
        /// </summary>
        /// <param name="a_EPlayerId"></param>
        void AssignPlayerTurn(EPlayer a_EPlayerId);
        void AssignPlayerTurnWithDelay(EPlayer a_EPlayerId, float a_fltTIme);
        void AssignPlayerTurnWithDelay(EPlayer a_EPlayerId, float a_fltTIme, Action a_callBack);


        /// <summary>
        /// Executes on bots turn (if player 2 is a bot)
        /// </summary>
        event Action InitiateBotsTurn;

        /// <summary>
        /// Executes when the player turn changes
        /// </summary>
        event Action<Player> OnCurrentPlayerChanged;

        /// <summary>
        /// set player 2 status as bot or player
        /// </summary>
        /// <param name="a_isPlayer2Bot"></param>
        void SetPlayer2Status(bool a_isPlayer2Bot);

        /// <summary>
        /// Assign first turn to play
        /// </summary>
        /// <returns></returns>
        EPlayer GetStartPlayer();

        /// <summary>
        /// Resets all  player data
        /// </summary>
        void ResetPlayerData();

        /// <summary>
        /// Set bot type
        /// </summary>
        /// <param name="a_Etype"></param>
        void SetCurrentBotType(EBotType a_Etype);
    }
}