﻿using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal interface IMenuManager : IManagerBase
    {
        /// <summary>
        /// Last menu shown in the screen
        /// </summary>
        /// <value></value>
        MenuType LastShownMenu { get; }

        /// <summary>
        /// Previous menu of the Last menu shown in the screen
        /// </summary>
        /// <value></value>
        MenuType PreviouslyShownMenu { get; }

        /// <summary>
        /// Show Menu Via type
        /// </summary>
        /// <param name="a_Etype"></param>
        void ShowMenu(MenuType a_Etype);

        /// <summary>
        /// Hide Menu via type
        /// </summary>
        /// <param name="a_Etype"></param>
        void HideMenu(MenuType a_Etype);

        /// <summary>
        /// Show UI
        /// </summary>
        void showHowToPlayUI();

        /// <summary>
        /// Hide UI
        /// </summary>
        void hideHowToPlayUI();

        /// <summary>
        /// Show UI
        /// </summary>
        void showUICreditsUI();

        /// <summary>
        /// Hide UI
        /// </summary>
        void hideUICreditsUI();

        /// <summary>
        /// Initiates End Screen menu button animations
        /// </summary>
        void PlayEndScreenBtnAnim();

        /// <summary>
        /// Show UI
        /// </summary>
        void showSettingsUI();

        /// <summary>
        /// Current customized color of x
        /// </summary>
        /// <value></value>
        Color XColor { get; }

        /// <summary>
        /// Current customized color of 0
        /// </summary>
        /// <value></value>
        Color YColor { get; }
    }
    
    /// <summary>
    /// Types of menu
    /// </summary>
    internal enum MenuType : byte
    {
        None, MainMenu, NewGame, SavedGame, InGame, EndGame
    }
}