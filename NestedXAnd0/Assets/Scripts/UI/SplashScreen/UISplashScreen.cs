﻿using System;
using System.Threading.Tasks;
using Shailan.System.Ads;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Shailan.SplashScreen
{
    public class UISplashScreen : MonoBehaviour, IPointerClickHandler
    {
        /// <summary>
        /// Game Manager instance ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }


        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_anim = null;

        /// <summary>
        /// Initiate splash screen animation
        /// </summary>
        public void InitaiteSplashScreen()
        {
            m_anim.Play("SplashScreen");
            IAdmobManagerRef.InitializeAdmob();
        }

        /// <summary>
        /// Play tap to open animation
        /// </summary>
        public void playTapOnStartAnmimation()
        {
            m_anim.Play("TapToStart");
        }

        /// <summary>
        /// On pointer click event
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            IGameManagerRef.OnApplicaionStart();
        }
    }
}