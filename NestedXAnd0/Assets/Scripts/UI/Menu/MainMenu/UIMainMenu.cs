﻿using Firebase.Analytics;
using NestedXn0.UI.Confirmation;
using Shailan.System.Ads;
using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal class UIMainMenu : UIMenuBase
    {
        /// <summary>
        /// Confirmation UI Ref
        /// </summary>
        [SerializeField]
        private UIConfirmation m_UIConfirmationRef = null;

        /// <summary>
        /// Background image obj ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBg = null;

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }


        /// <summary>
        /// Show UI
        /// </summary>
        internal override void ShowUI()
        {
            m_goBg.SetActive(true);
            base.ShowUI();

            //SHOW BANNER AD  AND INTERSTITIAL AD
            // IAdmobManagerRef.ShowIfAdBanner();
            IAdmobManagerRef.ShowBannerAdIfLoaded = true;
            IAdmobManagerRef.LoadIfNotNShowAdBanner();
            IAdmobManagerRef.LoadInterstitialAdIfNotLoaded();
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal override void HideUI()
        {
            base.HideUI();
            m_goBg.SetActive(false);
        }

        /// <summary>
        /// Opens new game menu UI
        /// </summary>
        public void OnClick_NewGameBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(MenuType.NewGame);
        }

        /// <summary>
        /// Opens saved games menu UI
        /// </summary>
        public void OnClick_SavedGamesBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(MenuType.SavedGame);
        }

        /// <summary>
        /// Opens how to play UI
        /// </summary>
        public void OnClick_HowToPlayBtn()
        {
            IAdmobManagerRef.HideAdBanner();
            HideUI();
            IMenuManagerRef.showHowToPlayUI();

            // Analytics
            FirebaseAnalytics.LogEvent("HowToPlay");
        }

        /// <summary>
        /// Opens setting menu UI
        /// </summary>
        public void OnClick_SettingsBtn()
        {
            IAdmobManagerRef.HideAdBanner();
            HideUI();
            IMenuManagerRef.showSettingsUI();

            // Analytics
            FirebaseAnalytics.LogEvent("Settings");
        }

        /// <summary>
        /// OPens credits UI
        /// </summary>
        public void OnClick_CreditsBtn()
        {
            IAdmobManagerRef.HideAdBanner();
            HideUI();
            IMenuManagerRef.showUICreditsUI();

            // Analytics
            FirebaseAnalytics.LogEvent("Credits");
        }

        /// <summary>
        /// shows confirmation box
        /// </summary>
        public void OnClick_ExitBtn()
        {
            m_UIConfirmationRef.ShowUI();
        }
        /// <summary>
        /// Quits the application
        /// </summary>
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}