﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shailan.Utils.SceneHandler
{
    public class SceneHandler
    {
        /// <summary>
        /// Scene Name
        /// </summary>
        /// <value></value>
        public string SceneName { get; private set; }

        /// <summary>
        /// Progress value?
        /// </summary>
        /// <value></value>
        public float Progress { get; private set; }

        /// <summary>
        /// Load mode type
        /// </summary>
        private LoadSceneMode m_Mode = LoadSceneMode.Single;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="a_strSceneName"></param>
        public SceneHandler(string a_strSceneName)
        {
            SceneName = a_strSceneName;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="a_strSceneName"></param>
        /// <param name="a_Mode"></param>
        public SceneHandler(string a_strSceneName, LoadSceneMode a_Mode)
        {
            SceneName = a_strSceneName;
            m_Mode = a_Mode;
        }

        /// <summary>
        /// Load scene in synchornously
        /// </summary>
        /// <param name="a_callback"></param>
        public void LoadScene(Action a_callback = null)
        {
            SceneManager.LoadScene(SceneName, m_Mode);
            a_callback?.Invoke();
        }

        /// <summary>
        /// Load scene Asynchornously
        /// </summary>
        /// <param name="a_callback"></param>
        public void LoadSceneAsync(Action a_callback = null)
        {
            Progress = 0;
            AsyncOperation l_operation = SceneManager.LoadSceneAsync(SceneName, m_Mode);
            InitiateOperation(l_operation, a_callback);
        }
        
        /// <summary>
        /// Unload scene in synchornously
        /// </summary>
        /// <param name="a_callback"></param>
        public void UnLoadScene(Action a_callback = null)
        {
#pragma warning disable 0612, 0618
            if (!SceneManager.UnloadScene(SceneName))
            {
                Debug.LogError($"SceneHandler::{SceneName} scene Unload Unsuccessfull!!");
                return;
            }
#pragma warning restore 0612, 0618
            a_callback?.Invoke();
        }

        /// <summary>
        /// Unload scene Asynchornously
        /// </summary>
        /// <param name="a_callback"></param>
        public void UnLoadSceneAsync(Action a_callback = null)
        {
            AsyncOperation l_operation = SceneManager.UnloadSceneAsync(SceneName);
            InitiateOperation(l_operation, a_callback);
        }

        /// <summary>
        /// Sets scene active
        /// </summary>
        /// <param name="a_callback"></param>
        public void SetSceneActive(Action a_callback = null)
        {
            Scene l_scene = SceneManager.GetSceneByName(SceneName);
            if (!SceneManager.SetActiveScene(l_scene))
            {
                Debug.LogError($"SceneHandler::{SceneName} scene activation Unsuccessfull!!");
                return;
            }
            a_callback?.Invoke();
        }

        /// <summary>
        /// Initiates async load or unload operation with an optional callback
        /// </summary>
        /// <param name="l_operation"></param>
        /// <param name="a_callback"></param>
        /// <returns></returns>
        async void InitiateOperation(AsyncOperation l_operation, Action a_callback)
        {
            Progress = l_operation.progress;
            while (!l_operation.isDone)
            {
                Progress = l_operation.progress;
                // await Task.Delay(1);
                await Task.Yield();
            }
            Progress = 1;
            a_callback?.Invoke();
        }
    }
}