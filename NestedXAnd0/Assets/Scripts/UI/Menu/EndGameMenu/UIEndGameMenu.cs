﻿using System;
using Firebase.Analytics;
using NestedXn0.Manager.MainGameUIManager;
using NestedXn0.Manager.Xn0Manager;
using NestedXn0.UI.Confirmation;
using Shailan.System.Ads;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using Shailan.System.Sound;
using Shailan.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.Menu
{
    internal class UIEndGameMenu : UIMenuBase
    {
        /// <summary>
        /// Game Manager ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Main Game Manager UI Ref
        /// </summary>
        private IMainGameUIManager m_IMainGameUIManagerRef = null;
        private IMainGameUIManager IMainGameUIManagerRef
        {
            get
            {
                if (m_IMainGameUIManagerRef == null)
                {
                    m_IMainGameUIManagerRef = ManagersHandler.GetManager<IMainGameUIManager>();
                }
                return m_IMainGameUIManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        /// <summary>
        /// Confirmation UI Ref
        /// </summary>
        [SerializeField]
        private UIConfirmation m_UIQuitConfirmationRef = null, m_UIMainMenuConfirmationRef = null;

        /// <summary>
        /// Rects of the menu btns
        /// </summary>
        [SerializeField]
        private RectTransform m_rectPlayAgain = null, m_rectMainMenu = null, m_rectExit = null;

        /// <summary>
        /// Menu btn refs
        /// </summary>
        [SerializeField]
        private Button m_btnPlayAgain = null, m_btnMainMenu = null, m_btnExit = null;

        /// <summary>
        /// STart Positions of the button before lerp
        /// </summary>
        [SerializeField]
        private Vector2 m_vec2PlayAgainStartPos = Vector2.zero, m_vec2MenuMenuStartPos = Vector2.zero, m_vec2ExitStartPos = Vector2.zero;

        /// <summary>
        /// End positions of the button before lerp
        /// </summary>
        [SerializeField]
        private Vector2 m_vec2PlayAgainEndPos = Vector2.zero, m_vec2MenuMenuEndPos = Vector2.zero, m_vec2ExitEndPos = Vector2.zero;

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// Resets and Restarts the game
        /// </summary>
        public void OnClick_PlayAgainBtn()
        {
            // show interstitial ad
            // IAdmobManagerRef.ShowAdInterstitial();

            HideUI();
            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                // IMainGameUIManagerRef.ResetTraditinalXn0GameData();
                // IMainGameUIManagerRef.InitiateTraditinalXn0Game();
                IMainGameUIManagerRef.RestartTraditinalXn0Game();
            }
            else if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Nested))
            {
                // IMainGameUIManagerRef.ResetNestedXn0GameData();
                // IMainGameUIManagerRef.InitiateNestedXnoGame();
                IMainGameUIManagerRef.RestartNestedXnoGame();
            }
            else
            {
                Debug.LogError("UIEndGameMenu::EXn0Type not asigned!!!");
            }

            
            // Analytics
            FirebaseAnalytics.LogEvent("PlayAgain");
        }

        /// <summary>
        /// Opens confirmation
        /// </summary>
        public void OnClick_MainMenuBtn()
        {
            // show interstitial ad
            // IAdmobManagerRef.ShowAdInterstitial();

            m_UIMainMenuConfirmationRef.ShowUI();
        }

        /// <summary>
        /// Redirects to Main menu
        /// </summary>
        public void OpenMainMenu()
        {
            HideUI();

            if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Traditional))
            {
                IMainGameUIManagerRef.ResetTraditinalXn0GameData();
            }
            else if (IGameManagerRef.ECurrentXn0Type.Equals(EXn0Type.Nested))
            {
                IMainGameUIManagerRef.ResetNestedXn0GameData();
            }
            else
            {
                Debug.LogError("UIEndGameMenu::EXn0Type not asigned!!!");
            }

            IMenuManagerRef.ShowMenu(MenuType.MainMenu);

            //PLAY AMBIENT MUSIC
            ISoundManagerRef.playAmbientMusic("a_amb_bensound_little_idea");

            //SHOW BANNER AD
            // IAdmobManagerRef.ShowAdBanner();
        }

        /// <summary>
        /// shows confirmation box
        /// </summary>
        public void OnClick_ExitBtn()
        {
            // show interstitial ad
            // IAdmobManagerRef.ShowAdInterstitial();

            m_UIQuitConfirmationRef.ShowUI();
        }

        /// <summary>
        /// Quits the application
        /// </summary>
        public void QuitGame()
        {
            Application.Quit();
        }


        #region TWEENING

        /// <summary>
        /// Lerps and shows UI after celebration UI
        /// </summary>
        internal void ShowEndScreenBtns()
        {
            m_btnPlayAgain.interactable = m_btnExit.interactable = m_btnMainMenu.interactable = false;
            m_rectPlayAgain.anchoredPosition = m_vec2PlayAgainStartPos;
            m_rectMainMenu.anchoredPosition = m_vec2MenuMenuStartPos;
            m_rectExit.anchoredPosition = m_vec2ExitStartPos;

            ShowUI();

            UITweening.MoveLerp(m_rectPlayAgain, m_rectPlayAgain.anchoredPosition, m_vec2PlayAgainEndPos, 1.0f, () => { m_btnPlayAgain.interactable = true; });
            UITweening.MoveLerp(m_rectMainMenu, m_rectMainMenu.anchoredPosition, m_vec2MenuMenuEndPos, 1.0f, () => { m_btnMainMenu.interactable = true; });
            UITweening.MoveLerp(m_rectExit, m_rectExit.anchoredPosition, m_vec2ExitEndPos, 1.0f, () =>
            {
                m_btnExit.interactable = true;

                // // show interstitial ad
                // if (IAdmobManagerRef.IsInterstitialAdLoaded)    // CHECK IF LOADED
                //     IAdmobManagerRef.ShowAdInterstitial();      // SHOW IF LOADED 
                // else
                //     IAdmobManagerRef.LoadInterstitialAd();     // LOAD IF NOT LOADED
            });
        }

        #endregion
    }
}