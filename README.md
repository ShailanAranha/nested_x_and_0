NESTED X AND 0

1. OVERVIEW

Implementing an revamped and innovative version of the classic 3x3 tic-tac-toe game where every square has a nested tic-tac-toe game inside it and players need to follow a brand new set of rules to win the game.
Three levels of Bot implemented (Beginner, Regular and Pro) for single player mode.
Developing for Android platform using Unity3D.

2. LICENCE

This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/3.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.