﻿using Shailan.System.Fps;
using TMPro;
using UnityEngine;

namespace NestedXn0.UI.Fps
{
    internal class UIFps : MonoBehaviour
    {
        /// <summary>
        /// Fps Counter instance ref
        /// </summary>
        [SerializeField]
        private FpsCounter m_FpsCounter = null;
        private IFpsCounter m_IFpsCounterRef = null;

        /// <summary>
        /// Fps text
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtFps = null;

        /// <summary>
        /// Target frame rate required
        /// </summary>
        [SerializeField]
        private int m_iTargetFrameRate = 30;

        private void Awake()
        {
            m_IFpsCounterRef = (IFpsCounter)m_FpsCounter;
        }

        private void Start()
        {
            Application.targetFrameRate = m_iTargetFrameRate;
            QualitySettings.vSyncCount = 0;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        private void Update()
        {
            m_txtFps.text = m_IFpsCounterRef.Fps.ToString("0.00");
        }

    }
}