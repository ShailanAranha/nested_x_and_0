﻿using NestedXn0.Manager.MainGameUIManager;
using NestedXn0.Manager.Xn0Manager;
using Shailan.System.GameManager;
using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal class UINewGameMenu : UIMenuBase
    {
        /// <summary>
        /// Main game UI manager ref
        /// </summary>
        private IMainGameUIManager m_IMainGameUIManagerRef = null;
        private IMainGameUIManager IMainGameUIManagerRef
        {
            get
            {
                if (m_IMainGameUIManagerRef == null)
                {
                    m_IMainGameUIManagerRef = ManagersHandler.GetManager<IMainGameUIManager>();
                }
                return m_IMainGameUIManagerRef;
            }
        }

        /// <summary>
        /// Game manager ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Background image obj ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBg = null;

        /// <summary>
        /// Show UI
        /// </summary>
        internal override void ShowUI()
        {
            m_goBg.SetActive(true);
            base.ShowUI();
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal override void HideUI()
        {
            base.HideUI();
            m_goBg.SetActive(false);
        }

        /// <summary>
        /// Initates the player selection screen for classic type
        /// </summary>
        public void OnClick_ClassicBtn()
        {
            HideUI();
            IGameManagerRef.ECurrentXn0Type = EXn0Type.Traditional;
            IMainGameUIManagerRef.ShowPlayerSelectionUI();

            // IMainGameUIManagerRef.InitiateTraditinalXn0Game();
        }

        /// <summary>
        /// Initates the player selection screen for nested type
        /// </summary>
        public void OnClick_NestedBtn()
        {
            HideUI();
            IGameManagerRef.ECurrentXn0Type = EXn0Type.Nested;
            IMainGameUIManagerRef.ShowPlayerSelectionUI();

            // IMainGameUIManagerRef.InitiateNestedXnoGame();
        }

        /// <summary>
        /// Takes back to main menu
        /// </summary>
        public void onClick_BackBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(MenuType.MainMenu);
        }
    }
}