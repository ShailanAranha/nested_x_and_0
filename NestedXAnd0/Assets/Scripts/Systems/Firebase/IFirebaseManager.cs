﻿using System.Collections;
using System.Collections.Generic;
using Shailan.System.Manager;
using UnityEngine;

namespace Shailan.System.Analytics
{
    public interface IFirebaseManager : IManagerBase
    {
        bool EnableBannerAd { get; }
        bool EnableInterstitialAd { get; }
    }
}