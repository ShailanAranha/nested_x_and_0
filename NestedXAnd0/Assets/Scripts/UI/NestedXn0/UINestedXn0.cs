﻿using System;
using System.Collections;
using System.Threading.Tasks;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Nested;
using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.UI
{
    internal class UINestedXn0 : MonoBehaviour
    {
        /// <summary>
        /// List of nested block references
        /// </summary>
        [SerializeField]
        private UINestedBlock[] m_arrNestedBlock = null;

        /// <summary>
        /// Bg image ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBg = null;

        /// <summary>
        /// Player Manager ref
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// NestedXn0Handler script ref
        /// </summary>
        [SerializeField]
        private NestedXn0Handler m_NestedXn0HandlerRef = null;

        /// <summary>
        /// Raised ehn a winner is achieved
        /// </summary>
        /// bool param is binary code of the player, int  param is winning pattern
        public event Action<bool, int> OnWinnerAchieved = null;

        /// <summary>
        /// Invoked when all block are marked and no winning pattern is achieved
        /// </summary>
        public event Action OnDrawAchieved = null;

        /// <summary>
        /// Holds cache of last players patterns based on the input
        /// </summary>
        private Pattern[] m_arrLastPlayersPatternCache = null;

        /// <summary>
        /// Holds cache of current players patterns based on the previous input
        /// </summary>
        private Pattern[] m_arrCurrentPlayersPreviousInputPatternCache = null;

        /// <summary>
        /// Main Gameobject transform 
        /// </summary>
        [SerializeField]
        private RectTransform m_rectMain = null;

        /// <summary>
        /// Is debug?
        /// </summary>
        [SerializeField]
        private bool m_isDebug = false;

        /// <summary>
        ///  Exectues when one nested block is completed
        /// </summary>
        /// <param name="a_iNestedBlockId"></param>
        /// <param name="a_nestedBlockWinnerBinaryCode"></param>
        /// <returns>IsGame completed?</returns>
        internal bool onNestedBlockComplete(int a_iNestedBlockId, bool a_nestedBlockWinnerBinaryCode)
        {
            // Current player
            // Player m_currentPlayer = IPlayerManagerRef.CurrentPlayer;
            if (m_isDebug)
                Debug.LogError("Block Winner:" + a_nestedBlockWinnerBinaryCode);

            Player m_winnerPlayer = IPlayerManagerRef.getPlayerViaBinaryCode(a_nestedBlockWinnerBinaryCode);

            // Update UI
            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrNestedBlock[i].Id == a_iNestedBlockId)
                {
                    // m_arrNestedBlock[i].SetBinaryCode(m_currentPlayer.BinaryCode);
                    // m_arrNestedBlock[i].SetSprite(m_currentPlayer.BinaryCodeSprite, m_currentPlayer.SpriteColor);
                    m_arrNestedBlock[i].SetBinaryCode(a_nestedBlockWinnerBinaryCode);
                    m_arrNestedBlock[i].SetSprite(m_winnerPlayer.BinaryCodeSprite, m_winnerPlayer.SpriteColor);

                    // GENERATE LAST PATTERN CACHE
                    m_arrCurrentPlayersPreviousInputPatternCache = m_arrLastPlayersPatternCache;
                    m_arrLastPlayersPatternCache = m_arrNestedBlock[i].arrPatterns;

                    // Verify input
                    Pattern m_winningPattern = null;
                    if (m_NestedXn0HandlerRef.VerifyPattern(m_arrNestedBlock, m_arrNestedBlock[i].arrPatterns, a_nestedBlockWinnerBinaryCode, out m_winningPattern))
                    {
                        if (m_isDebug)
                            Debug.LogError("Winner found");

                        // OnWinnerAchieved?.Invoke(m_currentPlayer.BinaryCode, m_winningPattern.PatternId);
                        OnWinnerAchieved?.Invoke(a_nestedBlockWinnerBinaryCode, m_winningPattern.PatternId);
                        return true;
                    }
                }
            }

            if (IsAllBlocksMarked())
            {
                if (m_isDebug)
                {
                    Debug.LogError("Game Over!!!");
                    Debug.LogError("NO WINNER");
                }

                OnDrawAchieved?.Invoke();
                return true;
            }

            // Switch Turns
            // IPlayerManagerRef.SwitchPlayerTurn();

            return false;
        }

        /// <summary>
        /// Assign turn via players binary code
        /// </summary>
        /// <param name="a_playeBInaryCode"></param>
        public void AssignTurn(bool a_playeBInaryCode)
        {
            Player l_player = IPlayerManagerRef.getPlayerViaBinaryCode(a_playeBInaryCode);
            IPlayerManagerRef.AssignPlayerTurn(l_player.EPlayerId);
        }

        /// <summary>
        /// Is all block marked?
        /// </summary>
        /// <returns></returns>
        private bool IsAllBlocksMarked()
        {
            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (!m_arrNestedBlock[i].IsMarked)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Enable all block for the current player to choose a nested block
        /// </summary>
        internal void EnableAllBlocks()
        {
            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                m_arrNestedBlock[i].EnableBlock();
            }
        }

        /// <summary>
        /// Disable all block for interaction
        /// </summary>
        internal void DisableAllBlocks()
        {
            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                m_arrNestedBlock[i].DisableBlock();
            }
        }

        /// <summary>
        /// Get active block id
        /// </summary>
        /// <returns></returns>
        internal int getActiveBlock()
        {
            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrNestedBlock[i].IsBlockActive)
                {
                    return m_arrNestedBlock[i].Id;
                }
            }

            Debug.LogError("UINestedXn0:: No block active");
            return -1;
        }

        /// <summary>
        /// Reset all blocks data
        /// </summary>
        internal void ResetAllBlocks()
        {
            StopCoroutine(PlayBotsTurn());

            int l_iCount = m_arrNestedBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                m_arrNestedBlock[i].ResetBlock();
            }

            m_arrCurrentPlayersPreviousInputPatternCache = m_arrLastPlayersPatternCache = null;
            m_NestedXn0HandlerRef.ResetData();

            if (m_rectMain.localScale.x > 1)
            {
                m_rectMain.anchoredPosition = Vector2.zero;
                m_rectMain.localScale = new Vector3(1, 1, 1);
            }
        }

        /// <summary>
        /// Show UI
        /// </summary>
        internal void ShowUI()
        {
            gameObject.SetActive(true);
            m_goBg.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal void HideUI()
        {
            gameObject.SetActive(false);
            m_goBg.SetActive(false);
        }

        #region BOT

        /// <summary>
        /// Bot will play the turn
        /// </summary>
        internal void BotsTurn()
        {
            // PlayBotsTurn();
            StartCoroutine(PlayBotsTurn());
        }

        /// <summary>
        /// Bot will play the turn with a delay
        /// </summary>
        /// <returns></returns>
        // private async void PlayBotsTurn()
        // {
        //     await Task.Delay(TimeSpan.FromSeconds(2));
        //     int l_botsInput = m_NestedXn0HandlerRef.GetBotsInputBlock(m_arrNestedBlock, m_arrLastPlayersPatternCache, m_arrCurrentPlayersPreviousInputPatternCache);

        //     int l_iCount = m_arrNestedBlock.Length;

        //     if (m_isDebug)
        //         Debug.LogError("l_botsInput:" + l_botsInput);

        //     for (int i = 0; i < l_iCount; i++)
        //     {
        //         if (m_arrNestedBlock[i].Id.Equals(l_botsInput))
        //         {
        //             m_arrNestedBlock[i].ActivateBlock();
        //             m_arrNestedBlock[i].traditionalXn0BotTurn();
        //             break;
        //         }
        //     }
        // }

        private IEnumerator PlayBotsTurn()
        {
            // await Task.Delay(TimeSpan.FromSeconds(2));
            yield return new WaitForSeconds(2.0f);
            int l_botsInput = m_NestedXn0HandlerRef.GetBotsInputBlock(m_arrNestedBlock, m_arrLastPlayersPatternCache, m_arrCurrentPlayersPreviousInputPatternCache);

            int l_iCount = m_arrNestedBlock.Length;

            if (m_isDebug)
                Debug.LogError("l_botsInput:" + l_botsInput);

            for (int i = 0; i < l_iCount; i++)
            {
                if (m_arrNestedBlock[i].Id.Equals(l_botsInput))
                {
                    m_arrNestedBlock[i].ActivateBlock();
                    m_arrNestedBlock[i].traditionalXn0BotTurn();
                    break;
                }
            }
        }

        #endregion

    }
}