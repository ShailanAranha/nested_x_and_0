﻿using System;
using System.Collections.Generic;
using NestedXn0.Manager.MainGameUIManager;
using NestedXn0.UI;
using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Manager.Xn0Manager
{
    internal class Xn0Manager : ManagerBase, IXn0Manager
    {
        /// <summary>
        /// Is debug?
        /// </summary>
        [SerializeField]
        protected bool m_isDebug = false;

        /// <summary>
        /// Main Game UI Manager ref
        /// </summary>
        private IMainGameUIManager m_IMainGameUIManagerRef = null;
        private IMainGameUIManager IMainGameUIManagerRef
        {
            get
            {
                if (m_IMainGameUIManagerRef == null)
                {
                    m_IMainGameUIManagerRef = ManagersHandler.GetManager<IMainGameUIManager>();
                }
                return m_IMainGameUIManagerRef;
            }
        }

        /// <summary>
        /// Converst string pattern to int array
        /// </summary>
        /// <param name="a_value"></param>
        /// <returns></returns>
        public int[] getPatternDesignArray(string a_value)
        {
            char[] l_seperator = new char[] { ',' };
            string[] l_arr = a_value.Split(l_seperator, 3, StringSplitOptions.RemoveEmptyEntries);

            int l_iValue1 = -1;
            int l_iValue2 = -1;
            int l_iValue3 = -1;

            if (!int.TryParse(l_arr[0], out l_iValue1))
            {
                Debug.LogError("Xn0Manager:: try parse failed, please check the value:" + l_arr[0]);
            }

            if (!int.TryParse(l_arr[1], out l_iValue2))
            {
                Debug.LogError("Xn0Manager:: try parse failed, please check the value:" + l_arr[1]);
            }

            if (!int.TryParse(l_arr[2], out l_iValue3))
            {
                Debug.LogError("Xn0Manager:: try parse failed, please check the value:" + l_arr[2]);
            }

            return new int[] { l_iValue1, l_iValue2, l_iValue3 };
        }

        /// <summary>
        /// Get binary code array of the block ids in the pattern for Xn0 type
        /// </summary>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        public bool?[] getBinaryCodeOfPattern(int[] a_arrDesign, UIBlock[] a_arrBlocks)
        {
            bool?[] l_arrBInaryValue = new bool?[3] { null, null, null };

            // a_result = l_arrInputValue;
            for (int j = 0; j < a_arrDesign.Length; j++)
            {
                for (int k = 0; k < a_arrBlocks.Length; k++)
                {
                    if (a_arrBlocks[k].Id.Equals(a_arrDesign[j]))
                    {
                        l_arrBInaryValue[j] = a_arrBlocks[k].BinaryCode;
                        break;
                    }
                }
            }

            return l_arrBInaryValue;
        }

        /// <summary>
        ///  Get binary code array of the block ids in the pattern for Nested Xn0 type
        /// </summary>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        public bool?[] getBinaryCodeOfPattern(int[] a_arrDesign, UINestedBlock[] a_arrBlocks)
        {
            bool?[] l_arrBInaryValue = new bool?[3] { null, null, null };

            // a_result = l_arrInputValue;
            for (int j = 0; j < a_arrDesign.Length; j++)
            {
                for (int k = 0; k < a_arrBlocks.Length; k++)
                {
                    if (a_arrBlocks[k].Id.Equals(a_arrDesign[j]))
                    {
                        l_arrBInaryValue[j] = a_arrBlocks[k].BinaryCode;
                        break;
                    }
                }
            }

            return l_arrBInaryValue;
        }

        /// <summary>
        /// The respective pattern of the player input code, matchs with players binary code? 
        /// </summary>
        /// <param name="a_arrCodes"></param>
        /// <param name="a_playerBinaryCode"></param>
        /// <returns></returns>
        public bool IsInputCodeMatchWithPlayerBInaryCodes(bool?[] a_arrCodes, bool a_playerBinaryCode)
        {
            for (int i = 0; i < a_arrCodes.Length; i++)
            {
                if (a_arrCodes[i] == null)
                {
                    return false;
                }
                else if (!a_arrCodes[i].Equals(a_playerBinaryCode))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if the pattern is disposed
        /// </summary>
        /// <param name="a_arrCodes"></param>
        /// <param name="a_playerBinaryCode"></param>
        /// <returns></returns>
        public bool IsDisposePattern(bool?[] a_arrCodes, bool a_playerBinaryCode)
        {
            for (int i = 0; i < a_arrCodes.Length; i++)
            {
                if (a_arrCodes[i] == null)
                {
                    continue;
                }
                else if (!a_arrCodes[i].Equals(a_playerBinaryCode))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// return random value from the list provided
        /// </summary>
        /// <param name="a_lst"></param>
        /// <returns></returns>
        public int GetRandomizedNumber(List<int> a_lst)
        {
            int l_randdomizedNum = UnityEngine.Random.Range(0, a_lst.Count);
            return a_lst[l_randdomizedNum];
        }

        /// <summary>
        /// checks if any winning pattern is achieved
        /// </summary>
        /// <param name="a_arrDesignBinaryValues"></param>
        /// <param name="a_playerBInaryCode"></param>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_iBlockId"></param>
        /// <returns></returns>
        public bool CheckForWinnablePattern(bool?[] a_arrDesignBinaryValues, bool a_playerBInaryCode, int[] a_arrDesign, out int a_iBlockId)
        {
            a_iBlockId = -1;
            int l_iCounter = 0;

            int l_iCount = a_arrDesignBinaryValues.Length;
            for (int j = 0; j < l_iCount; j++)
            {
                if (a_arrDesignBinaryValues[j].Equals(a_playerBInaryCode))
                {
                    l_iCounter++;
                }
            }

            if (!(l_iCounter == 2))
            {
                return false;
            }

            for (int j = 0; j < l_iCount; j++)
            {
                if (a_arrDesignBinaryValues[j] == null)
                {
                    a_iBlockId = a_arrDesign[j];
                }
            }

            return true;
        }
    }

    /// <summary>
    /// Xn0 Type
    /// </summary>
    internal enum EXn0Type
    {
        None, Traditional, Nested
    }
}