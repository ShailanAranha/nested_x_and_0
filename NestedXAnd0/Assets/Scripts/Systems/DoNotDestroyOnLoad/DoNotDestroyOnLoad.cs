﻿using UnityEngine;

namespace Shailan.System.DoNotDestroyOnLoad
{
    internal class DoNotDestroyOnLoad : MonoBehaviour
    {
        /// <summary>
        /// Initiating do not destory the gameobject on awake(Gameobject won't destroy on scene unload or reload)
        /// </summary>
        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}