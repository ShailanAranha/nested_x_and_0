﻿using NestedXn0.Menu;
using NestedXn0.PlayerBoard.UI;
using NestedXn0.UI;
using Shailan.System.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.Manager.Hud
{
    internal class HudManager : ManagerBase, IHudManager
    {
        /// <summary>
        /// Player Board instance ref
        /// </summary>
        [SerializeField]
        private UIPlayerboard m_UIPlayerBoardRef = null;

        /// <summary>
        /// CamZoomToUI Script ref
        /// </summary>
        [SerializeField]
        private CamZoomToUI m_CamZoomToUIRef = null;

        /// <summary>
        /// Hud Buttons of Xn0 and nested Xn0
        /// </summary>
        [SerializeField]
        private GameObject m_goXn0Btns = null, m_goNestedXn0Btns = null;

        /// <summary>
        /// Zoom Btn ref
        /// </summary>
        [SerializeField]
        private Button m_btnZoom = null;

        /// <summary>
        /// Zoomin and zoom out sprite reference
        /// </summary>
        [SerializeField]
        private Sprite m_sptZoomIn = null, m_sptZoomOut = null;

        /// <summary>
        ///  Zoom Image ref
        /// </summary>
        [SerializeField]
        private Image m_imgZoom = null;

        /// <summary>
        /// Menu Manager instance ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// UI Ref
        /// </summary>
        [SerializeField]
        private UINestedXn0 m_UINestedXn0Ref = null;

        /// <summary>
        ///  IS Activted block Zoomed in?
        /// </summary>
        /// <value></value>
        public bool IsActiveBlockZoomedIn { get; private set; } = false;

        /// <summary>
        /// Caching block Id which is currently active
        /// </summary>
        private int m_iCurrentActiveBlockId = -1;

        /// <summary>
        /// Custom Manager start
        /// </summary>
        protected override void ManagerStart()
        {
            base.ManagerStart();
            m_CamZoomToUIRef.OnZoomInFinished += () => { m_btnZoom.interactable = true; };
            m_CamZoomToUIRef.OnZoomOutFinished += () => { m_btnZoom.interactable = true; };
        }

        /// <summary>
        /// Custom Manager destroy
        /// </summary>
        protected override void ManagerDestroy()
        {
            m_CamZoomToUIRef.OnZoomInFinished -= () => { m_btnZoom.interactable = true; };
            m_CamZoomToUIRef.OnZoomOutFinished -= () => { m_btnZoom.interactable = true; };
            base.ManagerDestroy();
        }

        /// <summary>
        /// Show UI
        /// </summary>
        public void ShowXn0Hud()
        {
            m_UIPlayerBoardRef.ShowUI();
            m_goXn0Btns.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        public void HideXn0Hud()
        {
            m_UIPlayerBoardRef.HideUI();
            m_goXn0Btns.SetActive(false);
        }

        /// <summary>
        /// Show UI
        /// </summary>
        public void ShowNestedXn0Hud()
        {
            m_UIPlayerBoardRef.ShowUI();
            m_goNestedXn0Btns.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        public void HideNestedXn0Hud()
        {
            m_UIPlayerBoardRef.HideUI();
            m_goNestedXn0Btns.SetActive(false);
        }

        /// <summary>
        /// Show all the HUd buttons
        /// </summary>
        public void ShowXn0Btns()
        {
            m_goXn0Btns.SetActive(true);
        }

        /// <summary>
        /// Hide all the HUd buttons
        /// </summary>
        public void HideXn0Btns()
        {
            m_goXn0Btns.SetActive(false);
        }

        /// <summary>
        /// Show all the Nested UI HUd buttons
        /// </summary>
        public void ShowNestedXn0Btns()
        {
            m_goNestedXn0Btns.SetActive(true);
        }

        /// <summary>
        /// Hide all the Nested UI HUd buttons
        /// </summary>
        public void HideNestedXn0Btns()
        {
            m_goNestedXn0Btns.SetActive(false);
        }

        /// <summary>
        /// Back Button click event 
        /// </summary>
        public void onClickBackBtn()
        {
            IMenuManagerRef.ShowMenu(MenuType.InGame);
            // Time.timeScale = 0;
        }

        /// <summary>
        /// Reset player Board Data
        /// </summary>
        public void ResetPlayerBoardData()
        {
            m_UIPlayerBoardRef.ResetData();
            m_iCurrentActiveBlockId = -1;
            IsActiveBlockZoomedIn = false;
            m_imgZoom.sprite = m_sptZoomIn;
        }

        /// <summary>
        /// Zoom Btn click event
        /// </summary>
        public void onClickZoomBtn()
        {
            if (m_iCurrentActiveBlockId == -1)
            {
                return;
            }

            m_btnZoom.interactable = false;
            if (IsActiveBlockZoomedIn)
            {
                m_CamZoomToUIRef.ZoomOutBlock(m_iCurrentActiveBlockId);
                m_imgZoom.sprite = m_sptZoomIn;
                IsActiveBlockZoomedIn = false;
            }
            else
            {
                m_CamZoomToUIRef.ZoomInBlock(m_iCurrentActiveBlockId);
                m_imgZoom.sprite = m_sptZoomOut;
                IsActiveBlockZoomedIn = true;
            }
        }

        /// <summary>
        /// Camera Zoom in to block via Id 
        /// </summary>
        /// <param name="a_iBlockId"></param>
        public void ZoomInToBlock(int a_iBlockId)
        {
            if (IsActiveBlockZoomedIn)
            {
                return;
            }
            m_imgZoom.sprite = m_sptZoomOut;
            m_btnZoom.interactable = false;

            IsActiveBlockZoomedIn = true;
            m_iCurrentActiveBlockId = a_iBlockId;
            m_CamZoomToUIRef.ZoomInBlock(a_iBlockId);
        }

        /// <summary>
        /// Camera Zoom outs to block via Id 
        /// </summary>
        /// <param name="a_iBlockId"></param>
        public void ZoomOutToBlock(int a_iBlockId)
        {
            if (!IsActiveBlockZoomedIn)
            {
                return;
            }
            m_imgZoom.sprite = m_sptZoomIn;
            m_btnZoom.interactable = false;

            IsActiveBlockZoomedIn = false;
            m_iCurrentActiveBlockId = a_iBlockId;
            m_CamZoomToUIRef.ZoomOutBlock(a_iBlockId);
        }

        /// <summary>
        /// Zoom Out Of Current active Block
        /// </summary>
        public void ZoomOutOfCurrentBlock()
        {
            ZoomOutToBlock(m_UINestedXn0Ref.getActiveBlock());
        }
    }
}