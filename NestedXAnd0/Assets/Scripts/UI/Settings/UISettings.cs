﻿using Firebase.Analytics;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Menu;
using Shailan.System.Ads;
using Shailan.System.Manager;
using Shailan.System.Sound;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.UI.Setting
{
    internal class UISettings : MonoBehaviour
    {
        #region SHOW HIDE

        /// <summary>
        /// Menu Manager ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        [Space, Header("SHOW HIDE"), Space, SerializeField]

        /// <summary>
        /// X customization component
        /// </summary>
        private GameObject m_goXCustomization = null;

        /// <summary>
        /// 0 customization component
        /// </summary>
        [SerializeField]
        private GameObject m_go0Customization = null;

        /// <summary>
        /// background image reference
        /// </summary>
        [SerializeField]
        private GameObject m_goBackgroundImg = null;

        /// <summary>
        /// Show UI
        /// </summary>
        internal void ShowUI()
        {
            IAdmobManagerRef.ShowBannerAdIfLoaded = false;
            if (IMenuManagerRef.LastShownMenu.Equals(MenuType.InGame))
            {
                m_goXCustomization.SetActive(false);
                m_go0Customization.SetActive(false);
            }
            else
            {
                m_goXCustomization.SetActive(true);
                m_go0Customization.SetActive(true);
            }

            m_goBackgroundImg.SetActive(true);
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hied UI
        /// </summary>
        internal void HideUI()
        {
            IAdmobManagerRef.ShowBannerAdIfLoaded = true;
            gameObject.SetActive(false);
            m_goBackgroundImg.SetActive(false);
        }

        /// <summary>
        /// Takes back to main menu
        /// </summary>
        public void onClick_backBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(IMenuManagerRef.LastShownMenu);
        }

        #endregion

        #region SAVE DATA
        private const string AMBIENT_SOUND_VOL_LEVEL_KEY = "Ambient Sound Volume Level";
        private const string SFX_SOUND_VOL_LEVEL_KEY = "Sfx Sound Volume Level";
        private const string AMBIENT_SOUND_MUTED_KEY = "Is Ambient Sound Muted";
        private const string SFX_SOUND_MUTED_KEY = "Is SFX Sound Muted";

        private const string X_COLOR_KEY = "X_COLOR";
        private const string O_COLOR_KEY = "0_COLOR";

        /// <summary>
        /// Save sound and customization data
        /// </summary>
        private void SaveData()
        {
            PlayerPrefs.SetFloat(AMBIENT_SOUND_VOL_LEVEL_KEY, AmbientSoundVolumeLevel);
            PlayerPrefs.SetFloat(SFX_SOUND_VOL_LEVEL_KEY, SfxSoundVolumeLevel);

            PlayerPrefs.SetString(AMBIENT_SOUND_MUTED_KEY, IsAmbientSoundMuted.ToString());
            PlayerPrefs.SetString(SFX_SOUND_MUTED_KEY, IsSfxSoundMuted.ToString());

            PlayerPrefs.Save();
        }

        /// <summary>
        /// get saved sound and customization data
        /// </summary>
        internal void getSavedData()
        {
            // Get saved volume level
            if (PlayerPrefs.HasKey(AMBIENT_SOUND_VOL_LEVEL_KEY))
            {
                AmbientSoundVolumeLevel = PlayerPrefs.GetFloat(AMBIENT_SOUND_VOL_LEVEL_KEY);
                m_sldAmbient.value = AmbientSoundVolumeLevel;
                ISoundManagerRef.ChangeAmbientVolume(AmbientSoundVolumeLevel);
            }

            if (PlayerPrefs.HasKey(AMBIENT_SOUND_VOL_LEVEL_KEY))
            {
                SfxSoundVolumeLevel = PlayerPrefs.GetFloat(SFX_SOUND_VOL_LEVEL_KEY);
                m_sldSfx.value = SfxSoundVolumeLevel;
                ISoundManagerRef.ChangeSfxVolume(SfxSoundVolumeLevel);
            }

            // Ambient (get muted value saved)

            if (PlayerPrefs.HasKey(AMBIENT_SOUND_MUTED_KEY))
            {
                bool l_isAmbientSoundMuted = false;
                if (!bool.TryParse(PlayerPrefs.GetString(AMBIENT_SOUND_MUTED_KEY), out l_isAmbientSoundMuted))
                {
                    Debug.LogError("Try parse failed, please check the value!!");
                }

                IsAmbientSoundMuted = l_isAmbientSoundMuted;
                if (IsAmbientSoundMuted)
                {
                    m_imgAmbientSoundIcon.sprite = m_sptMuted;
                }
                else
                {
                    m_imgAmbientSoundIcon.sprite = m_sptUnmuted;
                }

                ISoundManagerRef.muteAmbientMusic(IsAmbientSoundMuted);
            }

            // sfx (get muted value saved)

            if (PlayerPrefs.HasKey(AMBIENT_SOUND_MUTED_KEY))
            {
                bool l_isSfxSoundMuted = false;
                if (!bool.TryParse(PlayerPrefs.GetString(SFX_SOUND_MUTED_KEY), out l_isSfxSoundMuted))
                {
                    Debug.LogError("Try parse failed, please check the value!!");
                }

                IsSfxSoundMuted = l_isSfxSoundMuted;
                if (IsSfxSoundMuted)
                {
                    m_imgSfxSoundIcon.sprite = m_sptMuted;
                }
                else
                {
                    m_imgSfxSoundIcon.sprite = m_sptUnmuted;
                }

                ISoundManagerRef.muteSfx(IsSfxSoundMuted);
            }

            // Get saved color
            if (PlayerPrefs.HasKey(X_COLOR_KEY))
            {
                int l_xValue = PlayerPrefs.GetInt(X_COLOR_KEY);

                switch (l_xValue)
                {
                    case 1:
                        m_imgXCurrentlySelected = m_imgXCol1;

                        m_imgXCol1.sprite = m_sprSeletctedColor;
                        m_imgXRef.color = m_imgXCol1.color;
                        break;

                    case 2:
                        m_imgXCurrentlySelected = m_imgXCol2;

                        m_imgXCol2.sprite = m_sprSeletctedColor;
                        m_imgXRef.color = m_imgXCol2.color;
                        break;

                    case 3:
                        m_imgXCurrentlySelected = m_imgXCol3;

                        m_imgXCol3.sprite = m_sprSeletctedColor;
                        m_imgXRef.color = m_imgXCol3.color;
                        break;

                    case 4:
                        m_imgXCurrentlySelected = m_imgXCol4;

                        m_imgXCol4.sprite = m_sprSeletctedColor;
                        m_imgXRef.color = m_imgXCol4.color;
                        break;

                    default:
                        Debug.LogError("UISerttings::value not found!!");
                        break;

                }
            }
            else
            {
                m_imgXCurrentlySelected = m_imgXCol1;
                m_imgXCol1.sprite = m_sprSeletctedColor;
                m_imgXRef.color = m_imgXCol1.color;
            }

            if (PlayerPrefs.HasKey(O_COLOR_KEY))
            {
                int l_0Value = PlayerPrefs.GetInt(O_COLOR_KEY);

                switch (l_0Value)
                {
                    case 1:
                        m_imgYCurrentlySelected = m_imgYCol1;

                        m_imgYCol1.sprite = m_sprSeletctedColor;
                        m_imgYRef.color = m_imgYCol1.color;
                        break;

                    case 2:
                        m_imgYCurrentlySelected = m_imgYCol2;

                        m_imgYCol2.sprite = m_sprSeletctedColor;
                        m_imgYRef.color = m_imgYCol2.color;
                        break;

                    case 3:
                        m_imgYCurrentlySelected = m_imgYCol3;

                        m_imgYCol3.sprite = m_sprSeletctedColor;
                        m_imgYRef.color = m_imgYCol3.color;
                        break;

                    case 4:
                        m_imgYCurrentlySelected = m_imgYCol4;

                        m_imgYCol4.sprite = m_sprSeletctedColor;
                        m_imgYRef.color = m_imgYCol4.color;
                        break;

                    default:
                        Debug.LogError("UISerttings::value not found!!");
                        break;
                }
            }
            else
            {
                m_imgYCurrentlySelected = m_imgYCol1;
                m_imgYCol1.sprite = m_sprSeletctedColor;
                m_imgYRef.color = m_imgYCol1.color;
            }
        }

        #endregion

        #region MONOBEHAVIOUR CALLS

        private void OnDestroy()
        {
            SaveData();
        }
        #endregion

        #region MUSIC

        /// <summary>
        /// Ambient music volume slider
        /// </summary>
        [Space, Header("MUSIC"), Space, SerializeField]
        private Slider m_sldAmbient = null;

        /// <summary>
        /// Sfx volume slider
        /// </summary>
        [SerializeField]
        private Slider m_sldSfx = null;

        /// <summary>
        /// Mutes and unmuted icons
        /// </summary>
        [SerializeField]
        private Sprite m_sptMuted = null, m_sptUnmuted = null;

        /// <summary>
        /// Sound image icon ref
        /// </summary>
        [SerializeField]
        private Image m_imgAmbientSoundIcon = null, m_imgSfxSoundIcon = null;

        /// <summary>
        /// Volume level of Ambient music
        /// </summary>
        /// <value></value>
        private float AmbientSoundVolumeLevel { get; set; } = 1.0f;

        /// <summary>
        /// Volume level of sfx
        /// </summary>
        /// <value></value>
        private float SfxSoundVolumeLevel { get; set; } = 1.0f;

        /// <summary>
        /// Is Ambient sound muted?
        /// </summary>
        /// <value></value>
        private bool IsAmbientSoundMuted { get; set; } = false;

        /// <summary>
        /// Is sfx muted?
        /// </summary>
        /// <value></value>
        private bool IsSfxSoundMuted { get; set; } = false;

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// On volume level change event
        /// </summary>
        public void OnValueChanged_Ambient()
        {
            ISoundManagerRef.ChangeAmbientVolume(m_sldAmbient.value);
            AmbientSoundVolumeLevel = m_sldAmbient.value;
        }

        /// <summary>
        /// On volume level change event(sfx)
        /// </summary>
        public void OnValueChanged_Sfx()
        {
            ISoundManagerRef.ChangeSfxVolume(m_sldSfx.value);
            SfxSoundVolumeLevel = m_sldSfx.value;
        }

        /// <summary>
        /// Toggles mute and unmute of ambient sound
        /// </summary>
        public void onClickMuteBtn_ambient()
        {
            IsAmbientSoundMuted = !IsAmbientSoundMuted;
            ISoundManagerRef.muteAmbientMusic(IsAmbientSoundMuted);

            if (IsAmbientSoundMuted)
            {
                m_imgAmbientSoundIcon.sprite = m_sptMuted;
            }
            else
            {
                m_imgAmbientSoundIcon.sprite = m_sptUnmuted;
            }
        }

        /// <summary>
        /// Toggles mute and unmute of sfx
        /// </summary>
        public void onClickMuteBtn_sfx()
        {
            IsSfxSoundMuted = !IsSfxSoundMuted;
            ISoundManagerRef.muteSfx(IsSfxSoundMuted);

            if (IsSfxSoundMuted)
            {
                m_imgSfxSoundIcon.sprite = m_sptMuted;
            }
            else
            {
                m_imgSfxSoundIcon.sprite = m_sptUnmuted;
            }
        }

        #endregion

        #region COLOR CUSTOMIZATION

        /// <summary>
        /// X image ref
        /// </summary>
        [Space, Header("COLOR CUSTOMIZATION"), Space, SerializeField]
        private Image m_imgXRef = null;

        /// <summary>
        /// 0 image ref
        /// </summary>
        [SerializeField]
        private Image m_imgYRef = null;

        /// <summary>
        /// Customization colors for X
        /// </summary>
        [SerializeField]
        private Image m_imgXCol1 = null, m_imgXCol2 = null, m_imgXCol3 = null, m_imgXCol4 = null;

        /// <summary>
        /// Customization colors for 0
        /// </summary>
        [SerializeField]
        private Image m_imgYCol1 = null, m_imgYCol2 = null, m_imgYCol3 = null, m_imgYCol4 = null;

        /// <summary>
        /// Selected and unselected icon ref
        /// </summary>
        [SerializeField]
        private Sprite m_sprSeletctedColor = null, m_sprUnSeletctedColor = null;

        /// <summary>
        /// Currently selected customization of x and 0
        /// </summary>
        private Image m_imgXCurrentlySelected = null, m_imgYCurrentlySelected = null;

        /// <summary>
        /// Playewr manager ref
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Cuurently selected color for x
        /// </summary>
        /// <value></value>
        internal Color XColor { get { return m_imgXCurrentlySelected.color; } }

        /// <summary>
        /// Cuurently selected color for 0
        /// </summary>
        /// <value></value>
        internal Color YColor { get { return m_imgYCurrentlySelected.color; } }

        /// <summary>
        /// Assign Customization 1 for X
        /// </summary>
        public void onClick_XCol1()
        {
            m_imgXCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgXCurrentlySelected = m_imgXCol1;

            m_imgXCol1.sprite = m_sprSeletctedColor;
            m_imgXRef.color = m_imgXCol1.color;

            // SAVE
            PlayerPrefs.SetInt(X_COLOR_KEY, 1);

            //Analytics
            sendColorChoiceToFirebase("X", "White");
        }

        /// <summary>
        /// Assign Customization 2 for X
        /// </summary>
        public void onClick_XCol2()
        {
            m_imgXCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgXCurrentlySelected = m_imgXCol2;

            m_imgXCol2.sprite = m_sprSeletctedColor;
            m_imgXRef.color = m_imgXCol2.color;

            // SAVE
            PlayerPrefs.SetInt(X_COLOR_KEY, 2);

            //Analytics
            sendColorChoiceToFirebase("X", "Red");
        }

        /// <summary>
        /// Assign Customization 3 for X
        /// </summary>
        public void onClick_XCol3()
        {
            m_imgXCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgXCurrentlySelected = m_imgXCol3;

            m_imgXCol3.sprite = m_sprSeletctedColor;
            m_imgXRef.color = m_imgXCol3.color;

            // SAVE
            PlayerPrefs.SetInt(X_COLOR_KEY, 3);

            //Analytics
            sendColorChoiceToFirebase("X", "Green");
        }


        /// <summary>
        /// Assign Customization 4 for X
        /// </summary>
        public void onClick_XCol4()
        {
            m_imgXCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgXCurrentlySelected = m_imgXCol4;

            m_imgXCol4.sprite = m_sprSeletctedColor;
            m_imgXRef.color = m_imgXCol4.color;

            // SAVE
            PlayerPrefs.SetInt(X_COLOR_KEY, 4);

            //Analytics
            sendColorChoiceToFirebase("X", "Blue");
        }

        /// <summary>
        /// Assign Customization 1 for 0
        /// </summary>
        public void onClick_YCol1()
        {
            m_imgYCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgYCurrentlySelected = m_imgYCol1;

            m_imgYCol1.sprite = m_sprSeletctedColor;
            m_imgYRef.color = m_imgYCol1.color;

            // SAVE
            PlayerPrefs.SetInt(O_COLOR_KEY, 1);

            //Analytics
            sendColorChoiceToFirebase("0", "White");
        }

        /// <summary>
        /// Assign Customization 2 for 0
        /// </summary>
        public void onClick_YCol2()
        {
            m_imgYCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgYCurrentlySelected = m_imgYCol2;

            m_imgYCol2.sprite = m_sprSeletctedColor;
            m_imgYRef.color = m_imgYCol2.color;

            // SAVE
            PlayerPrefs.SetInt(O_COLOR_KEY, 2);

            //Analytics
            sendColorChoiceToFirebase("0", "Red");
        }

        /// <summary>
        /// Assign Customization 3 for 0
        /// </summary>
        public void onClick_YCol3()
        {
            m_imgYCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgYCurrentlySelected = m_imgYCol3;

            m_imgYCol3.sprite = m_sprSeletctedColor;
            m_imgYRef.color = m_imgYCol3.color;

            // SAVE
            PlayerPrefs.SetInt(O_COLOR_KEY, 3);

            //Analytics
            sendColorChoiceToFirebase("0", "Green");
        }

        /// <summary>
        /// Assign Customization 4 for 0
        /// </summary>
        public void onClick_YCol4()
        {
            m_imgYCurrentlySelected.sprite = m_sprUnSeletctedColor;
            m_imgYCurrentlySelected = m_imgYCol4;

            m_imgYCol4.sprite = m_sprSeletctedColor;
            m_imgYRef.color = m_imgYCol4.color;

            // SAVE
            PlayerPrefs.SetInt(O_COLOR_KEY, 4);

            //Analytics
            sendColorChoiceToFirebase("0", "Blue");
        }
        #endregion

        #region ANALYTICS

        private void sendColorChoiceToFirebase(string a_strBinaryCode, string a_col)
        {
            // Analytics
            FirebaseAnalytics.LogEvent("ColorChoice",
            new Parameter("binary_code", a_strBinaryCode),
            new Parameter("Color", a_col));
        }

        #endregion

    }
}
