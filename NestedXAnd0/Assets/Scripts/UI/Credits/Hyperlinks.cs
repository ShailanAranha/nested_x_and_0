﻿using Shailan.System.GameManager;
using Shailan.System.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NestedXn0.Hyperlinks
{
    internal class Hyperlinks : MonoBehaviour, IPointerClickHandler
    {
        /// <summary>
        /// Text component ref
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtRef = null;

        /// <summary>
        /// Game Manager instance ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Open external link
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(m_txtRef, Input.mousePosition, null);
            if (linkIndex != -1)
            {
                TMP_LinkInfo linkInfo = m_txtRef.textInfo.linkInfo[linkIndex];

                Application.OpenURL(linkInfo.GetLinkID());
            }
        }

#if UNITY_EDITOR
        /// <summary>
        /// helper tool
        /// </summary>
        private void Reset()
        {
            m_txtRef = gameObject.GetComponent<TMP_Text>();
        }
#endif
    }
}