﻿using Shailan.System.Manager;

namespace Shailan.System.Sound
{
    public interface ISoundManager : IManagerBase
    {
        #region AMBIENT MUSIC

        /// <summary>
        /// Plays Ambient music via id
        /// </summary>
        /// <param name="a_strAudioId">Audio Id</param>
        void playAmbientMusic(string a_strAudioId);

        /// <summary>
        /// Pauses ambient music audio source
        /// </summary>
        void pauseAmbientMusic();

        /// <summary>
        ///  Mutes ambient music audio source
        /// </summary>
        /// <param name="a_isMute">Is mute?</param>
        void muteAmbientMusic(bool a_isMute);

        /// <summary>
        /// Changes volume level
        /// </summary>
        /// <param name="a_fltValue">Volume level value</param>
        void ChangeAmbientVolume(float a_fltValue);

        /// <summary>
        /// Stops Ambient music audio source and unloads asset
        /// </summary>
        void stopAmbientMusic();
        #endregion

        #region SFX

        /// <summary>
        /// Plays sfx via id
        /// </summary>
        /// <param name="a_strAudioId">Audio Id</param>
        void playSfx(string a_strAudioId);

        /// <summary>
        /// Pauses sfx audio source
        /// </summary>
        void pauseSfx();

        /// <summary>
        ///  Mutes sfx audio source
        /// </summary>
        /// <param name="a_isMute">Is mute?</param>
        void muteSfx(bool a_isMute);

        /// <summary>
        /// Changes volume level
        /// </summary>
        /// <param name="a_fltValue">Volume level value</param>
        void ChangeSfxVolume(float a_fltValue);

        /// <summary>
        /// Stops sfx audio source and unloads asset
        /// </summary>
        void stopSfx();
        #endregion
    }
}