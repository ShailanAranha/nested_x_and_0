﻿using NestedXn0.Manager.PlayerManager;
using NestedXn0.UI.Welcome;
using Shailan.System.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.PlayerBoard.UI
{
    internal class UIPlayerboard : MonoBehaviour
    {
        /// <summary>
        /// Player name text component reference
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtPlayer1 = null, m_txtPlayer2 = null;

        /// <summary>
        /// Color to indicate player's turn
        /// </summary>
        [SerializeField]
        private Color m_colYourTurnSignal = Color.green;

        /// <summary>
        /// To Reset the indicator
        /// </summary>
        [SerializeField]
        private Color m_colNotYourTurnSignal = Color.black;

        /// <summary>
        /// Player Signal image reference
        /// </summary>
        [SerializeField]
        private Image m_imgPlayer1Signal = null, m_imgPlayer2Signal = null;

        /// <summary>
        /// Player Manager Instance reference
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Welcome UI Ref
        /// </summary>
        [SerializeField]
        private UIWelcome m_UIWelcomeRef = null;

        /// <summary>
        /// Monobehaviour start method
        /// </summary>
        private void Start()
        {
            IPlayerManagerRef.OnCurrentPlayerChanged -= PlayerTurn;
            IPlayerManagerRef.OnCurrentPlayerChanged += PlayerTurn;

            m_UIWelcomeRef.OnWelcomeScreenCompleted -= AssignStartSignal;
            m_UIWelcomeRef.OnWelcomeScreenCompleted += AssignStartSignal;
        }

        /// <summary>
        /// Monobehaviour destroy method
        /// </summary>
        private void OnDestroy()
        {
            IPlayerManagerRef.OnCurrentPlayerChanged -= PlayerTurn;
        }

        /// <summary>
        /// Update the signal as per the players turn
        /// </summary>
        /// <param name="m_currentPlayer"></param>
        private void PlayerTurn(Player m_currentPlayer)
        {
            if (m_currentPlayer.EPlayerId.Equals(EPlayer.Player1))
            {
                m_imgPlayer1Signal.color = m_colYourTurnSignal;
                m_imgPlayer2Signal.color = m_colNotYourTurnSignal;
            }
            else
            {
                m_imgPlayer1Signal.color = m_colNotYourTurnSignal;
                m_imgPlayer2Signal.color = m_colYourTurnSignal;
            }
        }

        /// <summary>
        /// Assign player name and start player signal
        /// </summary>
        private void AssignInitialData()
        {
            m_imgPlayer1Signal.color = m_colNotYourTurnSignal;
            m_imgPlayer2Signal.color = m_colNotYourTurnSignal;

            m_txtPlayer1.text = IPlayerManagerRef.Player1.PlayerName;
            m_txtPlayer2.text = IPlayerManagerRef.Player2.PlayerName;
        }

        /// <summary>
        /// Assign start player signal
        /// </summary>
        private void AssignStartSignal()
        {
            if (IPlayerManagerRef.CurrentPlayer.EPlayerId.Equals(EPlayer.Player1))
            {
                m_imgPlayer1Signal.color = m_colYourTurnSignal;
            }
            else
            {
                m_imgPlayer2Signal.color = m_colYourTurnSignal;
            }
        }

        /// <summary>
        /// Show UI on screen
        /// </summary>
        internal void ShowUI()
        {
            AssignInitialData();
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide UI on screen
        /// </summary>
        internal void HideUI()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Reset data
        /// </summary>
        internal void ResetData()
        {
            m_imgPlayer1Signal.color = m_colNotYourTurnSignal;
            m_imgPlayer2Signal.color = m_colNotYourTurnSignal;
        }
    }
}