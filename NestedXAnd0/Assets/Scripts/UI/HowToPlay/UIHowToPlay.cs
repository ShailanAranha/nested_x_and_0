﻿using NestedXn0.Menu;
using Shailan.System.Ads;
using Shailan.System.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace NestedXn0.UI.HowToPlay
{
    internal class UIHowToPlay : MonoBehaviour
    {
        /// <summary>
        /// Menu Manager ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Admob manager instance ref
        /// </summary>
        private IAdmobManager m_IAdmobManagerRef = null;
        private IAdmobManager IAdmobManagerRef
        {
            get
            {
                if (m_IAdmobManagerRef == null)
                {
                    m_IAdmobManagerRef = ManagersHandler.GetManager<IAdmobManager>();
                }
                return m_IAdmobManagerRef;
            }
        }

        /// <summary>
        /// Scroll down message game object
        /// </summary>
        [SerializeField]
        private GameObject m_goScrollMsg = null;

        /// <summary>
        /// background image reference
        /// </summary>
        [SerializeField]
        private GameObject m_goBackgroundImg = null;

        /// <summary>
        /// Scroll ref
        /// </summary>
        [SerializeField]
        private ScrollRect m_srRef = null;

        /// <summary>
        /// Show UI
        /// </summary>
        internal void ShowUI()
        {
            IAdmobManagerRef.ShowBannerAdIfLoaded = false;
            m_goBackgroundImg.SetActive(true);
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal void HideUI()
        {
            IAdmobManagerRef.ShowBannerAdIfLoaded = true;
            gameObject.SetActive(false);
            m_goBackgroundImg.SetActive(false);
            m_srRef.verticalNormalizedPosition = 1;
        }

        /// <summary>
        /// On Click event for black btn 
        /// </summary>
        public void onClick_BackBtn()
        {
            HideUI();
            IMenuManagerRef.ShowMenu(IMenuManagerRef.LastShownMenu);
            m_goScrollMsg.SetActive(true);
        }

        /// <summary>
        /// Deactivates scrolling message gameobject
        /// </summary>
        public void DisableScrollMsg()
        {
            m_goScrollMsg.SetActive(false);
        }
    }

}