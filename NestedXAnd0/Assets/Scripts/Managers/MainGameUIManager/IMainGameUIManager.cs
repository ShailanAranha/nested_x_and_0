﻿using Shailan.System.Manager;

namespace NestedXn0.Manager.MainGameUIManager
{
    internal interface IMainGameUIManager : IManagerBase
    {
        /// <summary>
        /// Shows UI
        /// </summary>
        void ShowPlayerSelectionUI();

        /// <summary>
        /// Initiates required function to start Traditinal Xn0 Game
        /// </summary>
        void InitiateTraditinalXn0Game();

        /// <summary>
        /// Initiates required function to start Nested Xn0 Game
        /// </summary>
        void InitiateNestedXnoGame();

        /// <summary>
        /// Exectues Restart
        /// </summary>
        void RestartTraditinalXn0Game();

        /// <summary>
        /// Exectues Restart
        /// </summary>
        void RestartNestedXnoGame();

        /// <summary>
        /// Reset all data and UI 
        /// </summary>
        void ResetTraditinalXn0GameData();

        /// <summary>
        /// Reset all data and UI 
        /// </summary>
        void ResetNestedXn0GameData();
    }
}