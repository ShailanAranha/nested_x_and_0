﻿using Shailan.System.Manager;

namespace Shailan.System.Ads
{
    public interface IAdmobManager : IManagerBase
    {
        void InitializeAdmob();
        bool IsPlayerUnderAged { get; set; }

        #region BANNER AD
        bool IsBannerAdLoaded { get; }
        bool ShowBannerAdIfLoaded { get; set; }
        void LoadBannerAd();
        void ShowIfAdBanner();
        void HideAdBanner();
        void LoadIfNotNShowAdBanner();
        #endregion

        #region INTERSTITIAL AD
        bool IsInterstitialAdLoaded { get; }
        void LoadInterstitialAd();
        void ShowAdInterstitial();
        void ShowAdRewaredVideo();
        void LoadInterstitialAdIfNotLoaded();
        #endregion
    }
}
