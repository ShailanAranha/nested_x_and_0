﻿using System;
using System.Threading.Tasks;
using NestedXn0.Manager.PlayerManager;
using Shailan.System.Manager;
using NestedXn0.Traditional;
using UnityEngine;
using UnityEngine.Analytics;

namespace NestedXn0.UI
{
    internal class UIXn0 : MonoBehaviour
    {
        /// <summary>
        /// List of block references
        /// </summary>
        [SerializeField]
        private UIBlock[] m_lstBlock = null;

        /// <summary>
        /// Bg image ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBg = null;

        /// <summary>
        /// Player Manager ref
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// TraditionalXn0Handler script ref
        /// </summary>
        [SerializeField]
        private TraditionalXn0Handler m_TraditionalXn0Ref = null;

        /// <summary>
        /// Invoked when a pattern is sucessfully completed by a player and returns the winning pattern
        /// </summary>
        /// bool param is binary code of the player, int  param is winning pattern
        public event Action<bool, int> OnWinnerAchieved = null;

        /// <summary>
        /// Invoked when all block are marked and no winning pattern is achieved
        /// </summary>
        public event Action OnDrawAchieved = null;

        /// <summary>
        /// Holds cache of last players patterns based on the input
        /// </summary>
        private Pattern[] m_arrLastPlayersPatternCache = null;

        /// <summary>
        /// Holds cache of current players patterns based on the previous input
        /// </summary>
        private Pattern[] m_arrCurrentPlayersPreviousInputPatternCache = null;

        /// <summary>
        /// Is debug?
        /// </summary>
        [SerializeField]
        private bool m_isDebug = false;

        /// <summary>
        /// Executes input conditions of the block via id
        /// </summary>
        /// <param name="a_iBlockId"></param>
        /// <returns>Is continue game?</returns>
        public bool onClickBlock(int a_iBlockId)
        {
            // Debug.LogError(a_iBlockId + ":onClickBlock called");

            // Current player
            Player m_currentPlayer = IPlayerManagerRef.CurrentPlayer;

            // Update UI
            int l_iCount = m_lstBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (m_lstBlock[i].Id == a_iBlockId)
                {
                    m_lstBlock[i].SetBinaryCode(m_currentPlayer.BinaryCode);
                    m_lstBlock[i].SetSprite(m_currentPlayer.BinaryCodeSprite, m_currentPlayer.SpriteColor);

                    // GENERATE LAST PATTERN CACHE
                    m_arrCurrentPlayersPreviousInputPatternCache = m_arrLastPlayersPatternCache;
                    m_arrLastPlayersPatternCache = m_lstBlock[i].arrPatterns;

                    // Verify input
                    Pattern m_winningPattern = null;
                    if (m_TraditionalXn0Ref.VerifyPattern(m_lstBlock, m_lstBlock[i].arrPatterns, out m_winningPattern))
                    {
                        if (m_isDebug)
                            Debug.LogError("Winner found:" + m_winningPattern.PatternId);

                        SetAllBlocksInteractibility(false);  //DISABLE INTERACTION
                        IPlayerManagerRef.InitiateBotsTurn -= BotsTurn;
                        OnWinnerAchieved?.Invoke(m_currentPlayer.BinaryCode, m_winningPattern.PatternId);
                        return false;
                    }
                }
            }

            if (IsAllBlocksMarked())
            {
                if (m_isDebug)
                {
                    Debug.LogError("Game Over!!!");
                    Debug.LogError("NO WINNER");
                }
                SetAllBlocksInteractibility(false);  //DISABLE INTERACTION
                IPlayerManagerRef.InitiateBotsTurn -= BotsTurn;
                OnDrawAchieved?.Invoke();
                return false;
            }

            // Switch Turns
            IPlayerManagerRef.SwitchPlayerTurn();

            // CONTINUE GAME
            return true;
        }

        /// <summary>
        /// Is all block marked?
        /// </summary>
        /// <returns></returns>
        private bool IsAllBlocksMarked()
        {
            int l_iCount = m_lstBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (!m_lstBlock[i].IsMarked)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Reset all blocks data
        /// </summary>
        internal void ResetAllBlocks()
        {
            int l_iCount = m_lstBlock.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                m_lstBlock[i].ResetBlock();
            }

            m_TraditionalXn0Ref.ResetData();
            m_arrLastPlayersPatternCache = null;
            m_arrCurrentPlayersPreviousInputPatternCache = null;

            SetAllBlocksInteractibility(true);  //ENABLE INTERACTION

            // IPlayerManagerRef.InitiateBotsTurn += BotsTurn;
        }

        /// <summary>
        /// Show UI
        /// </summary>
        internal void ShowUI()
        {
            gameObject.SetActive(true);
            m_goBg.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal void HideUI()
        {
            gameObject.SetActive(false);
            m_goBg.SetActive(false);
        }

        #region BOT

        /// <summary>
        /// Register Bot event if player 2 is a bot
        /// </summary>
        internal void RegisterBotEvent()
        {
            if (IPlayerManagerRef.IsPlayer2Bot)
            {
                IPlayerManagerRef.InitiateBotsTurn -= BotsTurn;
                IPlayerManagerRef.InitiateBotsTurn += BotsTurn;
            }
        }

        /// <summary>
        /// Set interactibility of all the blocks
        /// </summary>
        /// <param name="a_isInteractable"></param>
        private void SetAllBlocksInteractibility(bool a_isInteractable)
        {
            for (int i = 0; i < m_lstBlock.Length; i++)
            {
                m_lstBlock[i].IsBlockInteractible = a_isInteractable;
            }
        }

        /// <summary>
        /// Bot will play the turn
        /// </summary>
        internal void BotsTurn()
        {
            SetAllBlocksInteractibility(false);
            PlayBotsTurn();
        }

        /// <summary>
        /// Bot will play the turn with a delay
        /// </summary>
        /// <returns></returns>
        private async void PlayBotsTurn()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            int l_botsInput = m_TraditionalXn0Ref.GetBotsInputBlock(m_lstBlock, m_arrLastPlayersPatternCache, m_arrCurrentPlayersPreviousInputPatternCache);

            int l_iCount = m_lstBlock.Length;

            if (m_isDebug)
                Debug.LogError("l_botsInput:" + l_botsInput);

            bool l_isContinueGame = false;

            for (int i = 0; i < l_iCount; i++)
            {
                if (m_lstBlock[i].Id.Equals(l_botsInput))
                {
                    l_isContinueGame = m_lstBlock[i].OnClick_Block();
                    break;
                }
            }

            if (l_isContinueGame)
            {
                SetAllBlocksInteractibility(true);
            }
        }

        #endregion

    }

    [Serializable]
    internal class Pattern
    {
        /// <summary>
        /// Unique pattern id
        /// </summary>
        [SerializeField]
        private int m_id = -1;
        internal int PatternId { get { return m_id; } }

        /// <summary>
        /// Pattern design
        /// </summary>
        [SerializeField]
        private string m_strDesign = null;
        internal string Design { get { return m_strDesign; } }
    }
}