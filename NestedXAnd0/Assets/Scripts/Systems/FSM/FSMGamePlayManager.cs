﻿using System.Collections.Generic;
using Shailan.Utils.SceneHandler;
using Shailan.System.Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shailan.System.FSM
{
    internal class FSMGamePlayManager : ManagerBase, IFSMGamePlayManager
    {
        /// <summary>
        /// Start up state name key
        /// </summary>
        internal const string STATE_START_UP = "StartUp";

        /// <summary>
        /// Main Menu state name key
        /// </summary>
        internal const string STATE_MAIN_MENU = "Menu";

        /// <summary>
        ///  Main Game state name key
        /// </summary>
        internal const string STATE_MAIN_GAME = "MainGame";

        /// <summary>
        /// Order of state execution
        /// </summary>
        [SerializeField]
        private List<string> m_lstFSMFlow = null;

        /// <summary>
        /// Current running state
        /// </summary>
        /// <value></value>
        public string CurrentState { get; private set; }

        /// <summary>
        /// Index of the current state
        /// </summary>
        /// <value></value>
        public int StateIndex { get; private set; }

        /// <summary>
        /// Is Debug?
        /// </summary>
        [SerializeField]
        private bool m_isDebug = false;

        /// <summary>
        /// Initializing finate state machine
        /// </summary>
        public void InitiateFSM()
        {
            OnEnterState(STATE_START_UP);
            StateIndex = 0;
        }

        /// <summary>
        /// Transitions to next state from the flow list
        /// </summary>
        public void TransitionToNextState()
        {
            OnExitState(m_lstFSMFlow[StateIndex]);

            if (StateIndex < m_lstFSMFlow.Count - 1)
            {
                ++StateIndex;
                OnEnterState(m_lstFSMFlow[StateIndex]);
            }
            else
            {
                Debug.LogError("FSMGamePlayManager:: Next state doesn't exists!!");
            }
        }

        /// <summary>
        /// Transition to state via state name
        /// </summary>
        /// <param name="a_strState">State Name</param>
        public void TransitionToState(string a_strState)
        {
            if (CurrentState.Equals(a_strState))
            {
                Debug.LogError($"FSMGamePlayManager:: already in the state {a_strState}");
                return;
            }

            for (int i = 0; i < m_lstFSMFlow.Count; i++)
            {
                if (m_lstFSMFlow[i].Equals(a_strState))
                {
                    OnExitState(CurrentState);
                    StateIndex = i;
                    OnEnterState(a_strState);
                    break;
                }
            }
        }

        /// <summary>
        /// Exectes on enterring the state
        /// </summary>
        /// <param name="a_state"></param>
        private void OnEnterState(string a_state)
        {
            switch (a_state)
            {
                case STATE_START_UP:

                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state entered!!");

                    CurrentState = STATE_START_UP;
                    break;

                case STATE_MAIN_GAME:
                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state entered!!");

                    CurrentState = STATE_MAIN_GAME;
                    SceneHandler l_MainGame = new SceneHandler(STATE_MAIN_GAME, LoadSceneMode.Single);
                    // l_MainGame.LoadSceneAsync(() => { l_MainGame.SetSceneActive(); });
                    l_MainGame.LoadSceneAsync();
                    break;

                case STATE_MAIN_MENU:
                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state entered!!");

                    CurrentState = STATE_MAIN_MENU;

                    SceneHandler l_MainMenu = new SceneHandler(STATE_MAIN_MENU, LoadSceneMode.Additive);
                    // l_MainMenu.LoadSceneAsync(() => { l_MainMenu.SetSceneActive(); });
                    l_MainMenu.LoadSceneAsync();

                    break;

                default:
                    Debug.LogError($"FSMGamePlayManager::{a_state} state not found!!");
                    break;
            }
        }

        /// <summary>
        /// Executes on existing the state
        /// </summary>
        /// <param name="a_state"></param>
        private void OnExitState(string a_state)
        {
            switch (a_state)
            {
                case STATE_START_UP:
                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state exited!!");

                    break;

                case STATE_MAIN_GAME:
                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state exited!!");

                    break;

                case STATE_MAIN_MENU:
                    if (m_isDebug)
                        Debug.Log($"FSMGamePlayManager:: {a_state} state exited!!");

                    break;

                default:
                    Debug.LogError($"FSMGamePlayManager::{a_state} state not found!!");
                    break;
            }
        }

        // public abstract void OnEnterState(string a_state) { }
        // public abstract void OnExitState(string a_state) { }
    }
}