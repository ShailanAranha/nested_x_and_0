﻿using Firebase.Analytics;
using Shailan.AgeNeutral;
using Shailan.SplashScreen;
using UnityEngine;
using UnityEngine.UI;

namespace Shailan.PrivacyPolicy
{
    public class UIPrivacyPolicy : MonoBehaviour
    {
        [Space, Header("Main Privacy Policy"), SerializeField, Space]
        private Canvas m_cavRef = null;

        [SerializeField]
        private GraphicRaycaster m_grRef = null;

        [SerializeField]
        private UISplashScreen m_UISplashScreenRef = null;

        [SerializeField]
        private UIPrivacyPolicyConfirmation m_UIPrivacyPolicyConfirmationRef = null;

        [SerializeField]
        private GameObject m_goBgRef = null;


        [Space, Header("Privacy Policy Update"), SerializeField, Space]
        private Canvas m_cavUpdateUIRef = null;

        [SerializeField]
        private GraphicRaycaster m_grUpdateUIRef = null;

        [SerializeField]
        private Animation m_animRef = null;

        private bool IsPrivacyPolicyAccepted { get; set; } = false;

        private const string PRIVACY_POLICY_KEY = "is privacy policy accepted?";
        private const string PRIVACY_POLICY_CURRENTLY_VERSION_KEY = "privacy policy current version";

        [SerializeField]
        private int m_PrivacyPolicyUpdateVersion = 1;
        private int PrivacyPolicyCurrentVersion { get; set; } = 0;

        [Space, Header("Age Neutral Screen"), SerializeField, Space]
        private AgeNeutralScreen m_AgeNeutralScreenRef = null;

        private void Start()
        {
            if (PlayerPrefs.HasKey(PRIVACY_POLICY_KEY))
            {
                bool m_isPrivacyPolicyAccepted = false;
                if (!bool.TryParse(PlayerPrefs.GetString(PRIVACY_POLICY_KEY), out m_isPrivacyPolicyAccepted))
                {
                    Debug.LogError("UIPrivacyPolicy:: try parse failed:" + PlayerPrefs.GetString(PRIVACY_POLICY_KEY));
                }
                IsPrivacyPolicyAccepted = m_isPrivacyPolicyAccepted;

            }

            if (!IsPrivacyPolicyAccepted) // NOT ACCEPTED YET
            {

                m_AgeNeutralScreenRef.OnAgeConfirmed += () => { showPrivacyPolicy(); };

                m_goBgRef.SetActive(true);
                m_AgeNeutralScreenRef.ShowUI();

                // showPrivacyPolicy();
                PrivacyPolicyCurrentVersion = m_PrivacyPolicyUpdateVersion;
            }
            else // ACCEPTED
            {

                if (PlayerPrefs.HasKey(PRIVACY_POLICY_CURRENTLY_VERSION_KEY))
                {
                    PrivacyPolicyCurrentVersion = PlayerPrefs.GetInt(PRIVACY_POLICY_CURRENTLY_VERSION_KEY);
                }

                if (m_PrivacyPolicyUpdateVersion > PrivacyPolicyCurrentVersion) // UPDATE VERSION CHECK
                {
                    //SHOW UPDATED PRIVACY POLICY
                    m_AgeNeutralScreenRef.OnAgeConfirmed += showUpdatedPrivacyPolicy;

                    m_goBgRef.SetActive(true);
                    m_AgeNeutralScreenRef.ShowUI();

                    // showUpdatedPrivacyPolicy();
                }
                else
                {
                    m_UISplashScreenRef.InitaiteSplashScreen();
                }

                // m_UISplashScreenRef.InitaiteSplashScreen();
            }
        }

        private void showPrivacyPolicy()
        {
            m_cavRef.enabled = m_grRef.enabled = true;
            m_goBgRef.SetActive(true);
        }

        private void hidePrivacyPolicy()
        {
            m_cavRef.enabled = m_grRef.enabled = false;
            m_goBgRef.SetActive(false);
        }

        private void showUpdatedPrivacyPolicy()
        {
            m_cavUpdateUIRef.enabled = m_grUpdateUIRef.enabled = true;
            m_goBgRef.SetActive(true);
            m_animRef.Play("PrivacyPolicyUpdate");
        }

        private void hideUpdatedPrivacyPolicy()
        {
            m_cavUpdateUIRef.enabled = m_grUpdateUIRef.enabled = false;
            m_goBgRef.SetActive(false);
            m_animRef.Stop();
        }

        public void onClick_AcceptBtn()
        {
            IsPrivacyPolicyAccepted = true;
            PlayerPrefs.SetString(PRIVACY_POLICY_KEY, IsPrivacyPolicyAccepted.ToString());
            hidePrivacyPolicy();

            PrivacyPolicyCurrentVersion = m_PrivacyPolicyUpdateVersion;
            PlayerPrefs.SetInt(PRIVACY_POLICY_CURRENTLY_VERSION_KEY, PrivacyPolicyCurrentVersion);
            hideUpdatedPrivacyPolicy();

            PlayerPrefs.Save();
            m_UISplashScreenRef.InitaiteSplashScreen();
        }

        public void onClick_RejectBtn()
        {
            m_UIPrivacyPolicyConfirmationRef.ShowUI();
        }

        public void onClick_ClikToReadBtn()
        {
            Application.OpenURL("https://shailangamedev.blogspot.com/2021/01/nested-tic-tac-toe.html");

            // Analytics
            FirebaseAnalytics.LogEvent("OpenPrivacyPolicy");
        }

    }
}