﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Shailan.System.Sound;
using Shailan.System.Manager;

namespace NestedXn0.UI
{
    internal class UIBlock : MonoBehaviour, IPointerClickHandler
    {

        #region PROPERTIES

        /// <summary>
        /// Block Id
        /// </summary>
        [SerializeField]
        private int m_id = -1;
        internal int Id { get { return m_id; } }

        /// <summary>
        /// X or 0?
        /// </summary>
        [SerializeField]
        private bool? m_binaryCode = null;
        internal bool? BinaryCode { get { return m_binaryCode; } }

        /// <summary>
        /// FOR EDITOR TESTING
        /// </summary>
        [SerializeField]
        private bool m_isMarked = false;

        /// <summary>
        /// Is Block Marked?
        /// </summary>
        /// <value></value>
        internal bool IsMarked
        {
            get
            {
                if (m_binaryCode == null)
                {
                    // m_isMarked = false;    // FOR EDITOR TESTING
                    return false;
                }
                // m_isMarked = true;      // FOR EDITOR TESTING
                return true;
            }
        }

        /// <summary>
        /// IS block interactable?
        /// </summary>
        /// <value></value>
        internal bool IsBlockInteractible { get; set; } = true;

        /// <summary>
        /// winning patterns possible
        /// </summary>
        [SerializeField]
        private Pattern[] m_arrPatterns = null;
        internal Pattern[] arrPatterns { get { return m_arrPatterns; } }

        #endregion

        /// <summary>
        /// Main UI xn0 ref
        /// </summary>
        [SerializeField]
        private UIXn0 m_UIXn0Ref = null;

        /// <summary>
        /// Icon holding image ref
        /// </summary>
        [SerializeField]
        private Image m_img = null;

        /// <summary>
        /// Sound manager instance ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        /// <summary>
        /// Set X or 0 sprite
        /// </summary>
        /// <param name="a_spr"></param>
        /// <param name="a_col"></param>
        internal void SetSprite(Sprite a_spr, Color a_col)
        {
            m_img.sprite = a_spr;
            m_img.color = a_col;

            //CHANGE ALPHA
            Color l_col = m_img.color;
            l_col.a = 1;
            m_img.color = l_col;
        }

        /// <summary>
        /// Set binary code of the block
        /// </summary>
        /// <param name="a_value"></param>
        internal void SetBinaryCode(bool a_value)
        {
            m_binaryCode = a_value;
        }

        /// <summary>
        /// On block click event
        /// </summary>
        internal bool OnClick_Block()
        {
            if (IsMarked)
            {
                // return;
                return false;
            }
            m_isMarked = true;  // for editor testing
            bool l_isContinueGame = m_UIXn0Ref.onClickBlock(m_id);

            // PLAY SFX
            if (m_binaryCode == null)
                return false;
            else if ((bool)m_binaryCode)
                ISoundManagerRef.playSfx("a_xcode_sfx");
            else
                ISoundManagerRef.playSfx("a_0code_sfx");

            return l_isContinueGame;
        }

        /// <summary>
        /// On pointer click event
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (IsBlockInteractible)
                OnClick_Block();
        }

        /// <summary>
        /// Resets block data
        /// </summary>
        internal void ResetBlock()
        {
            m_img.sprite = null;
            m_binaryCode = null;

            Color l_col = m_img.color;
            l_col.a = 0.0f;
            m_img.color = l_col;

            m_isMarked = false;
        }
    }
}