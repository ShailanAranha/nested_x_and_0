﻿using NestedXn0.Manager.PlayerManager;
using NestedXn0.Manager.Xn0Manager;
using NestedXn0.Menu;
using Shailan.System.FSM;
using Shailan.System.Manager;
using UnityEngine;

namespace Shailan.System.GameManager
{
    internal class GameManager : ManagerBase, IGameManager
    {

        /// <summary>
        /// FSM Game Play Manager instance reference
        /// </summary>
        private IFSMGamePlayManager m_IFSMGamePlayManagerRef = null;
        private IFSMGamePlayManager IFSMGamePlayManagerRef
        {
            get
            {
                if (m_IFSMGamePlayManagerRef == null)
                {
                    m_IFSMGamePlayManagerRef = ManagersHandler.GetManager<IFSMGamePlayManager>();
                }
                return m_IFSMGamePlayManagerRef;
            }
        }

        /// <summary>
        /// Player Manager instance reference
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Menu Manager instance reference
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        private IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Currently running or last ran Xn0 type game 
        /// </summary>
        /// <value></value>
        public EXn0Type ECurrentXn0Type { get; set; }

        /// <summary>
        /// Main camera ref
        /// </summary>
        [SerializeField]
        private Camera m_camMainRef = null;
        public Camera MainCamRef { get { return m_camMainRef; } }

        /// <summary>
        /// Custom Manager start
        /// </summary>
        // protected override void ManagerStart()
        // {
        //     base.ManagerStart();
        //     OnApplicaionStart();
        // }

        /// <summary>
        /// Executes very first thing of the game (On application load)
        /// </summary>
        public void OnApplicaionStart()
        {
            IFSMGamePlayManagerRef.InitiateFSM();

            IFSMGamePlayManagerRef.TransitionToState(FSMGamePlayManager.STATE_MAIN_GAME);
            IFSMGamePlayManagerRef.TransitionToState(FSMGamePlayManager.STATE_MAIN_MENU);

        }
    }
}