﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shailan.PrivacyPolicy
{
    public class UIPrivacyPolicyConfirmation : MonoBehaviour
    {
        /// <summary>
        /// Bg component ref
        /// </summary>
        [SerializeField]
        private GameObject m_goBgRef = null;

        /// <summary>
        /// Show UI
        /// </summary>
        public void ShowUI()
        {
            m_goBgRef.SetActive(true);
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        public void HideUI()
        {
            m_goBgRef.SetActive(false);
            gameObject.SetActive(false);
        }

        public void onClick_BackBtn()
        {
            HideUI();
        }

        public void onClick_ExitBtn()
        {
            Application.Quit();
        }
    }
}