﻿using System;
using Shailan.System.Manager;
using UnityEngine;

namespace Shailan.System.Sound
{
    public class SoundManager : ManagerBase, ISoundManager
    {
        /// <summary>
        /// Array of ambient sound data
        /// </summary>
        [SerializeField]
        private SoundData[] m_arrAmbientMusic = null;

        /// <summary>
        /// Array of sfx sound data
        /// </summary>
        [SerializeField]
        private SoundData[] m_arrSfx = null;

        /// <summary>
        /// Ambient music audio source
        /// </summary>
        [SerializeField]
        private AudioSource m_audsAmbient = null;

        /// <summary>
        /// Sfx Audio source
        /// </summary>
        [SerializeField]
        private AudioSource m_audsSfx = null;

        #region AMBIENT MUSIC

        /// <summary>
        /// Plays Ambient music via id
        /// </summary>
        /// <param name="a_strAudioId">Audio Id</param>
        public void playAmbientMusic(string a_strAudioId)
        {
            SoundData l_soundData = getRequiredSoundData(a_strAudioId, m_arrAmbientMusic);

            m_audsAmbient.clip = l_soundData.AudioClip;
            m_audsAmbient.loop = l_soundData.IsLoop;
            m_audsAmbient.Play();
        }

        /// <summary>
        /// Pauses ambient music audio source
        /// </summary>
        public void pauseAmbientMusic()
        {
            m_audsAmbient.Pause();
        }

        /// <summary>
        ///  Mutes ambient music audio source
        /// </summary>
        /// <param name="a_isMute">Is mute?</param>
        public void muteAmbientMusic(bool a_isMute)
        {
            m_audsAmbient.mute = a_isMute;
        }

        /// <summary>
        /// Changes volume level
        /// </summary>
        /// <param name="a_fltValue">Volume level value</param>
        public void ChangeAmbientVolume(float a_fltValue)
        {
            m_audsAmbient.volume = a_fltValue;
        }

        /// <summary>
        /// Stops Ambient music audio source and unloads asset
        /// </summary>
        public void stopAmbientMusic()
        {
            m_audsAmbient.Stop();
            // Resources.UnloadAsset(m_audsAmbient.clip);
        }

        #endregion

        #region SFX

        /// <summary>
        /// Plays sfx via id
        /// </summary>
        /// <param name="a_strAudioId">Audio Id</param>
        public void playSfx(string a_strAudioId)
        {
            SoundData l_soundData = getRequiredSoundData(a_strAudioId, m_arrSfx);

            m_audsSfx.clip = l_soundData.AudioClip;
            m_audsSfx.Play();
        }

        /// <summary>
        /// Pauses sfx audio source
        /// </summary>
        public void pauseSfx()
        {
            m_audsSfx.Pause();
        }

        /// <summary>
        ///  Mutes sfx audio source
        /// </summary>
        /// <param name="a_isMute">Is mute?</param>
        public void muteSfx(bool a_isMute)
        {
            m_audsSfx.mute = a_isMute;
        }

        /// <summary>
        /// Changes volume level
        /// </summary>
        /// <param name="a_fltValue">Volume level value</param>
        public void ChangeSfxVolume(float a_fltValue)
        {
            m_audsSfx.volume = a_fltValue;
        }

        /// <summary>
        /// Stops sfx audio source and unloads asset
        /// </summary>
        public void stopSfx()
        {
            m_audsSfx.Stop();
            Resources.UnloadAsset(m_audsSfx.clip);
        }

        #endregion

        #region HELPER FUNCTIONS

        /// <summary>
        /// Fetches and Returns required sound data via audio id from provided array 
        /// </summary>
        /// <param name="a_strAudioId"></param>
        /// <param name="a_arrSoundData"></param>
        /// <returns></returns>
        private SoundData getRequiredSoundData(string a_strAudioId, SoundData[] a_arrSoundData)
        {
            int l_iCount = a_arrSoundData.Length;
            for (int i = 0; i < l_iCount; i++)
            {
                if (a_arrSoundData[i].AudioId.Equals(a_strAudioId))
                {
                    return a_arrSoundData[i];
                }
            }

            Debug.LogError("SoundManager:: Sound data not found, please check the audio id:" + a_strAudioId);
            return null;
        }

        #endregion
    }

    [Serializable]
    public class SoundData
    {
        /// <summary>
        /// Unique Audio Id
        /// </summary>
        [SerializeField]
        private string m_strAudioId = null;
        public string AudioId { get { return m_strAudioId; } }

        /// <summary>
        /// Audio clip instance ref
        /// </summary>
        [SerializeField]
        private AudioClip m_strAudioClip = null;
        public AudioClip AudioClip { get { return m_strAudioClip; } }

        /// <summary>
        /// Is Loop?
        /// </summary>
        [SerializeField]
        private bool m_isLoop = false;
        public bool IsLoop { get { return m_isLoop; } }

    }

}
