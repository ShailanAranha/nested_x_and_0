﻿using Shailan.System.Manager;
using UnityEngine;

namespace NestedXn0.Menu
{
    internal abstract class UIMenuBase : MonoBehaviour
    {
        /// <summary>
        /// Type of menu
        /// </summary>
        [SerializeField]
        private MenuType m_EType = MenuType.None;
        internal MenuType EType { get { return m_EType; } }

        /// <summary>
        /// Menu Manager ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        protected IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Show UI
        /// </summary>
        internal virtual void ShowUI()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Hide UI
        /// </summary>
        internal virtual void HideUI()
        {
            gameObject.SetActive(false);
        }
    }
}