﻿using System.Collections.Generic;
using NestedXn0.UI;
using Shailan.System.Manager;

namespace NestedXn0.Manager.Xn0Manager
{
    internal interface IXn0Manager : IManagerBase
    {
        /// <summary>
        /// Converst string pattern to int array
        /// </summary>
        /// <param name="a_value"></param>
        /// <returns></returns>
        int[] getPatternDesignArray(string a_value);

        /// <summary>
        /// Get binary code array of the block ids in the pattern for Xn0 type
        /// </summary>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        bool?[] getBinaryCodeOfPattern(int[] a_arrDesign, UIBlock[] a_arrBlocks);

        /// <summary>
        ///  Get binary code array of the block ids in the pattern for Nested Xn0 type
        /// </summary>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_arrBlocks"></param>
        /// <returns></returns>
        bool?[] getBinaryCodeOfPattern(int[] a_arrDesign, UINestedBlock[] a_arrBlocks);

        /// <summary>
        /// The respective pattern of the player input code, matchs with players binary code? 
        /// </summary>
        /// <param name="a_arrCodes"></param>
        /// <param name="a_playerBinaryCode"></param>
        /// <returns></returns>
        bool IsInputCodeMatchWithPlayerBInaryCodes(bool?[] a_arrCodes, bool a_playerBinaryCode);

        /// <summary>
        /// Checks if the pattern is disposed
        /// </summary>
        /// <param name="a_arrCodes"></param>
        /// <param name="a_playerBinaryCode"></param>
        /// <returns></returns>
        bool IsDisposePattern(bool?[] a_arrCodes, bool a_playerBinaryCode);

        /// <summary>
        /// checks if any winning pattern is achieved
        /// </summary>
        /// <param name="a_arrDesignBinaryValues"></param>
        /// <param name="a_playerBInaryCode"></param>
        /// <param name="a_arrDesign"></param>
        /// <param name="a_iBlockId"></param>
        /// <returns></returns>
        bool CheckForWinnablePattern(bool?[] a_arrDesignBinaryValues, bool a_playerBInaryCode, int[] a_arrDesign, out int a_iBlockId);

        /// <summary>
        /// return random value from the list provided
        /// </summary>
        /// <param name="a_lst"></param>
        /// <returns></returns>
        int GetRandomizedNumber(List<int> a_lst);
    }

}