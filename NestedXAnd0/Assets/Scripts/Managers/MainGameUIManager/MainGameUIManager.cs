﻿using NestedXn0.Manager.Hud;
using NestedXn0.Manager.PlayerManager;
using NestedXn0.Menu;
using Shailan.System.GameManager;
using NestedXn0.UI;
using NestedXn0.UI.PlayerSelection;
using UnityEngine;
using NestedXn0.WinMarkUI;
using System.Threading.Tasks;
using System;
using NestedXn0.UI.Celebration;
using Shailan.System.Manager;
using NestedXn0.UI.Welcome;
using Shailan.System.Sound;
using Shailan.Utils.DelayExecutor;
using UnityEngine.Analytics;
using System.Collections.Generic;
using Firebase.Analytics;

namespace NestedXn0.Manager.MainGameUIManager
{
    internal class MainGameUIManager : ManagerBase, IMainGameUIManager
    {
        /// <summary>
        /// Xn0 UI reference
        /// </summary>
        [SerializeField]
        private UIXn0 m_UIXn0Ref = null;

        /// <summary>
        /// Nested Xn0 UI reference
        /// </summary>
        [SerializeField]
        private UINestedXn0 m_UINestedRef = null;

        /// <summary>
        /// UI Player selection reference
        /// </summary>
        [SerializeField]
        private UIPlayerSelection m_UIPlayerSelectionRef = null;

        /// <summary>
        /// Win mark script reference of both types
        /// </summary>
        [SerializeField]
        private WinMark m_winMarkTraditionalRef = null, m_winMarkNestedRef = null;

        /// <summary>
        /// Welcome UI Ref
        /// </summary>
        [SerializeField]
        private UIWelcome m_UIWelcomeRef = null;

        /// <summary>
        /// UI Celebartion ref
        /// </summary>
        [SerializeField]
        private UICelebration m_UICelebrationRef = null;

        /// <summary>
        /// Menu Manager instance ref
        /// </summary>
        private IMenuManager m_IMenuManagerRef = null;
        private IMenuManager IMenuManagerRef
        {
            get
            {
                if (m_IMenuManagerRef == null)
                {
                    m_IMenuManagerRef = ManagersHandler.GetManager<IMenuManager>();
                }
                return m_IMenuManagerRef;
            }
        }

        /// <summary>
        /// Game Manager instance ref
        /// </summary>
        private IGameManager m_IGameManagerRef = null;
        private IGameManager IGameManagerRef
        {
            get
            {
                if (m_IGameManagerRef == null)
                {
                    m_IGameManagerRef = ManagersHandler.GetManager<IGameManager>();
                }
                return m_IGameManagerRef;
            }
        }

        /// <summary>
        /// Player Manager instance ref 
        /// </summary>
        private IPlayerManager m_IPlayerManagerRef = null;
        private IPlayerManager IPlayerManagerRef
        {
            get
            {
                if (m_IPlayerManagerRef == null)
                {
                    m_IPlayerManagerRef = ManagersHandler.GetManager<IPlayerManager>();
                }
                return m_IPlayerManagerRef;
            }
        }

        /// <summary>
        /// Hud Manager instance ref
        /// </summary>
        private IHudManager m_IHudManagerRef = null;
        private IHudManager IHudManagerRef
        {
            get
            {
                if (m_IHudManagerRef == null)
                {
                    m_IHudManagerRef = ManagersHandler.GetManager<IHudManager>();
                }
                return m_IHudManagerRef;
            }
        }

        /// <summary>
        /// Sound manager ref
        /// </summary>
        private ISoundManager m_ISoundManagerRef = null;
        private ISoundManager ISoundManagerRef
        {
            get
            {
                if (m_ISoundManagerRef == null)
                {
                    m_ISoundManagerRef = ManagersHandler.GetManager<ISoundManager>();
                }
                return m_ISoundManagerRef;
            }
        }

        public const string TRADITIONAL_GAME_TITLE = "Classic Tic-Tac-Toe";
        public const string NESTED_GAME_TITLE = "Nested Tic-Tac-Toe";
        public const string WINNER_ID = "Winner";
        public const string DRAW_ID = "Draw";

        /// <summary>
        /// Shows UI
        /// </summary>
        public void ShowPlayerSelectionUI()
        {
            m_UIPlayerSelectionRef.ShowUI();
        }

        /// <summary>
        /// Initiates required function to start Traditinal Xn0 Game
        /// </summary>
        public void InitiateTraditinalXn0Game()
        {
            //CREATE PLAYERS
            IPlayerManagerRef.CreatePlayers();

            // REGISTER BOT EVENT
            m_UIXn0Ref.RegisterBotEvent();

            //ASSIGN FIRST PLAYER
            EPlayer l_EPlayer = IPlayerManagerRef.GetStartPlayer();
            IPlayerManagerRef.AssignPlayerTurn(l_EPlayer);

            // STOP AMBIENT
            ISoundManagerRef.stopAmbientMusic();

            // SHOW UI
            m_UIXn0Ref.ShowUI();
            IHudManagerRef.ShowXn0Hud();

            //SHOW WELCOME UI
            m_UIWelcomeRef.ShowWelcomeScreen();

            m_UIXn0Ref.OnWinnerAchieved -= OnXn0WinnerAcieved;
            m_UIXn0Ref.OnWinnerAchieved += OnXn0WinnerAcieved;
            m_UIXn0Ref.OnDrawAchieved -= OnXn0DrawAcieved;
            m_UIXn0Ref.OnDrawAchieved += OnXn0DrawAcieved;

            // Analytics Events
            m_UIXn0Ref.OnWinnerAchieved -= sendClassicXn0WinnerData;
            m_UIXn0Ref.OnWinnerAchieved += sendClassicXn0WinnerData;
            m_UIXn0Ref.OnDrawAchieved -= sendClassicXn0DrawData;
            m_UIXn0Ref.OnDrawAchieved += sendClassicXn0DrawData;

            //Firebase    
            sendLevelStartEvent(TRADITIONAL_GAME_TITLE, getPlayerType());
        }


        /// <summary>
        /// Initiates required function to start Nested Xn0 Game
        /// </summary>
        public void InitiateNestedXnoGame()
        {
            //CREATE PLAYERS
            IPlayerManagerRef.CreatePlayers();

            //ASSIGN FIRST PLAYER
            EPlayer l_EPlayer = IPlayerManagerRef.GetStartPlayer();
            IPlayerManagerRef.AssignPlayerTurn(l_EPlayer);

            //ENABLE ALL BLOCKS
            m_UINestedRef.EnableAllBlocks();

            // STOP AMBIENT
            ISoundManagerRef.stopAmbientMusic();

            // SHOW UI
            m_UINestedRef.ShowUI();
            IHudManagerRef.ShowNestedXn0Hud();

            //SHOW WELCOME UI
            m_UIWelcomeRef.ShowWelcomeScreen();

            m_UINestedRef.OnWinnerAchieved -= OnNestedXn0WinnerAcieved;
            m_UINestedRef.OnWinnerAchieved += OnNestedXn0WinnerAcieved;
            m_UINestedRef.OnDrawAchieved -= OnNestedXn0DrawAcieved;
            m_UINestedRef.OnDrawAchieved += OnNestedXn0DrawAcieved;

            // Analytics Events
            m_UINestedRef.OnWinnerAchieved -= sendNestedXn0WinnerData;
            m_UINestedRef.OnWinnerAchieved += sendNestedXn0WinnerData;
            m_UINestedRef.OnDrawAchieved -= sendNestedXn0DrawData;
            m_UINestedRef.OnDrawAchieved += sendNestedXn0DrawData;


            //Firebase    
            sendLevelStartEvent(NESTED_GAME_TITLE, getPlayerType());

        }

        /// <summary>
        /// Exectues Restart
        /// </summary>
        public void RestartTraditinalXn0Game()
        {
            //RESET AND HIDE CELEBRATION UI
            m_UICelebrationRef.HideUI();

            //RESET PLAYR BOARD DATA
            m_IHudManagerRef.ResetPlayerBoardData();

            // RESET BLOCKS
            m_UIXn0Ref.ResetAllBlocks();

            // REGISTER BOT EVENT
            m_UIXn0Ref.RegisterBotEvent();

            //ASSIGN FIRST PLAYER
            EPlayer l_EPlayer = IPlayerManagerRef.GetStartPlayer();
            IPlayerManagerRef.AssignPlayerTurnWithDelay(l_EPlayer, 2.5f);

            //SHOW WELCOME UI
            m_UIWelcomeRef.ShowWelcomeScreen();

            // ASSIGN EVENT 
            m_UIXn0Ref.OnWinnerAchieved -= OnXn0WinnerAcieved;
            m_UIXn0Ref.OnWinnerAchieved += OnXn0WinnerAcieved;
            m_UIXn0Ref.OnDrawAchieved -= OnXn0DrawAcieved;
            m_UIXn0Ref.OnDrawAchieved += OnXn0DrawAcieved;

            // Analytics Events
            m_UIXn0Ref.OnWinnerAchieved -= sendClassicXn0WinnerData;
            m_UIXn0Ref.OnWinnerAchieved += sendClassicXn0WinnerData;
            m_UIXn0Ref.OnDrawAchieved -= sendClassicXn0DrawData;
            m_UIXn0Ref.OnDrawAchieved += sendClassicXn0DrawData;

            //Firebase    
            sendLevelStartEvent(TRADITIONAL_GAME_TITLE, getPlayerType());
        }

        /// <summary>
        /// Exectues Restart
        /// </summary>
        public void RestartNestedXnoGame()
        {
            //RESET AND HIDE CELEBRATION UI
            m_UICelebrationRef.HideUI();

            //RESET PLAYR BOARD DATA
            m_IHudManagerRef.ResetPlayerBoardData();

            // RESET BLOCKS
            m_UINestedRef.ResetAllBlocks();

            //ASSIGN FIRST PLAYER
            EPlayer l_EPlayer = IPlayerManagerRef.GetStartPlayer();
            // IPlayerManagerRef.AssignPlayerTurn(l_EPlayer);
            IPlayerManagerRef.AssignPlayerTurnWithDelay(l_EPlayer, 2f, () => { PlayInitialBotsTurn(l_EPlayer); });

            //ENABLE ALL BLOCKS
            // m_UINestedRef.EnableAllBlocks();

            //SHOW WELCOME UI
            m_UIWelcomeRef.ShowWelcomeScreen();

            //IF BOT, PLAY ITS TURN
            // if (l_EPlayer.Equals(EPlayer.Player2) && IPlayerManagerRef.IsPlayer2Bot)
            // {
            //     m_UINestedRef.BotsTurn();
            // }

            // ASSIGN EVENT
            m_UINestedRef.OnWinnerAchieved -= OnNestedXn0WinnerAcieved;
            m_UINestedRef.OnWinnerAchieved += OnNestedXn0WinnerAcieved;
            m_UINestedRef.OnDrawAchieved -= OnNestedXn0DrawAcieved;
            m_UINestedRef.OnDrawAchieved += OnNestedXn0DrawAcieved;

            // Analytics Events
            m_UINestedRef.OnWinnerAchieved -= sendNestedXn0WinnerData;
            m_UINestedRef.OnWinnerAchieved += sendNestedXn0WinnerData;
            m_UINestedRef.OnDrawAchieved -= sendNestedXn0DrawData;
            m_UINestedRef.OnDrawAchieved += sendNestedXn0DrawData;

            //Firebase    
            sendLevelStartEvent(NESTED_GAME_TITLE, getPlayerType());
        }

        /// <summary>
        /// Reset all data and UI 
        /// </summary>
        public void ResetTraditinalXn0GameData()
        {
            m_UICelebrationRef.HideUI();
            m_UIXn0Ref.HideUI();
            // m_UIPlayerBoardRef.HideUI();
            IHudManagerRef.HideXn0Hud();
            m_UIXn0Ref.ResetAllBlocks();
            IPlayerManagerRef.ResetPlayerData();
            m_winMarkTraditionalRef.ResetMark();   //RESET WIN MARK 
        }

        /// <summary>
        /// Reset all data and UI 
        /// </summary>
        public void ResetNestedXn0GameData()
        {
            m_UICelebrationRef.HideUI();
            m_UINestedRef.HideUI();
            // m_UIPlayerBoardRef.HideUI();
            IHudManagerRef.HideNestedXn0Hud();
            m_UINestedRef.ResetAllBlocks();
            IPlayerManagerRef.ResetPlayerData();
            m_winMarkNestedRef.ResetMark();   //RESET WIN MARK 
        }

        /// <summary>
        /// Called when a winner is achieved in traditional xn0 game
        /// </summary>
        /// <param name="a_playerBinaryCode"></param>
        private void OnXn0WinnerAcieved(bool a_playerBinaryCode, int a_iwinningPattern)
        {
            // m_winMarkTraditionalRef.OnWinMarkCompleted -= () => { IMenuManagerRef.ShowMenu(MenuType.EndGame); };
            // m_winMarkTraditionalRef.OnWinMarkCompleted += () => { IMenuManagerRef.ShowMenu(MenuType.EndGame); };

            m_winMarkTraditionalRef.OnWinMarkCompleted -= () => { m_UICelebrationRef.startCelebration(a_playerBinaryCode); };
            m_winMarkTraditionalRef.OnWinMarkCompleted += () => { m_UICelebrationRef.startCelebration(a_playerBinaryCode); };

            m_winMarkTraditionalRef.WinMarkThePattern(a_iwinningPattern, true);

            DelayExecutor l_obj = new DelayExecutor(0.5f, () => { ISoundManagerRef.playSfx("a_sfx_victory"); });

        }

        /// <summary>
        /// Called when there is a tie in traditional xn0 game
        /// </summary>
        private void OnXn0DrawAcieved()
        {
            // IMenuManagerRef.ShowMenu(MenuType.EndGame);
            m_UICelebrationRef.startCelebration(null);
        }

        /// <summary>
        /// Called when a winner is achieved in NESTED xn0 game
        /// </summary>
        /// <param name="a_playerBinaryCode"></param>
        private void OnNestedXn0WinnerAcieved(bool a_playerBinaryCode, int a_iwinningPattern)
        {
            // m_winMarkNestedRef.OnWinMarkCompleted -= () => { IMenuManagerRef.ShowMenu(MenuType.EndGame); };
            // m_winMarkNestedRef.OnWinMarkCompleted += () => { IMenuManagerRef.ShowMenu(MenuType.EndGame); };

            m_winMarkNestedRef.OnWinMarkCompleted -= () => { m_UICelebrationRef.startCelebration(a_playerBinaryCode); };
            m_winMarkNestedRef.OnWinMarkCompleted += () => { m_UICelebrationRef.startCelebration(a_playerBinaryCode); };

            if (IHudManagerRef.IsActiveBlockZoomedIn)
            {
                WaitBeforeMarkingPAttern(a_iwinningPattern);
            }
            else
            {
                m_winMarkNestedRef.WinMArkNestedPattern(a_iwinningPattern);
                DelayExecutor l_obj = new DelayExecutor(0.5f, () => { ISoundManagerRef.playSfx("a_sfx_victory"); });
            }
        }

        /// <summary>
        /// Delay before marking the winning pattern
        /// </summary>
        /// <param name="a_iwinningPattern"></param>
        /// <returns></returns>
        private async void WaitBeforeMarkingPAttern(int a_iwinningPattern)
        {
            await Task.Delay(TimeSpan.FromSeconds(2));
            m_winMarkNestedRef.WinMArkNestedPattern(a_iwinningPattern);
            await Task.Delay(TimeSpan.FromSeconds(0.5f));
            ISoundManagerRef.playSfx("a_sfx_victory");
        }

        /// <summary>
        /// Called when there is a tie in traditional xn0 game
        /// </summary>
        private void OnNestedXn0DrawAcieved()
        {
            // m_UICelebrationRef.startCelebration(null);

            if (IHudManagerRef.IsActiveBlockZoomedIn)
            {
                DelayExecutor l_obj = new DelayExecutor(1.0f, () => { m_UICelebrationRef.startCelebration(null); });
            }
            else
            {
                m_UICelebrationRef.startCelebration(null);
            }
        }

        /// <summary>
        /// Play Bots Turn if bot is the start player
        /// </summary>
        /// <param name="l_EPlayer"></param>
        private void PlayInitialBotsTurn(EPlayer l_EPlayer)
        {
            if (l_EPlayer.Equals(EPlayer.Player2) && IPlayerManagerRef.IsPlayer2Bot)
            {
                m_UINestedRef.BotsTurn();
            }
            else
            {
                m_UINestedRef.EnableAllBlocks();
            }
        }


        #region ANALYTICS

        public void sendClassicXn0WinnerData(bool a_binaryData, int a_iPatternId)
        {
            // if (IPlayerManagerRef.IsPlayer2Bot)
            // {
            //     AnalyticsResult l_result = Analytics.CustomEvent("ClassicXn0::Winner:", new Dictionary<string, object>() {
            //         {"Bot type", IPlayerManagerRef.GetCurrentBotType}
            //         });
            //     Debug.Log("ClassicXn0::Winner:Result:" + l_result);
            // }

            sendLevelEndEvent(TRADITIONAL_GAME_TITLE, getPlayerType(), WINNER_ID);

            if (IPlayerManagerRef.IsPlayer2Bot)
            {
                Player l_player = IPlayerManagerRef.getPlayerViaBinaryCode(a_binaryData);
                string l_strWinningPLayer = null;
                if (EPlayer.Player1.Equals(l_player.EPlayerId))
                {
                    l_strWinningPLayer = "Player";
                }
                else
                {
                    l_strWinningPLayer = "Bot";
                }


                FirebaseAnalytics.LogEvent("BotType",
                new Parameter("level_type", TRADITIONAL_GAME_TITLE),
                new Parameter("difficulty", IPlayerManagerRef.GetCurrentBotType.ToString()),
                new Parameter("result", WINNER_ID + ", " + l_strWinningPLayer));
            }
        }

        public void sendClassicXn0DrawData()
        {
            // if (IPlayerManagerRef.IsPlayer2Bot)
            // {
            //     AnalyticsResult l_result = Analytics.CustomEvent("ClassicXn0::Draw:", new Dictionary<string, object>() {
            //         {"Bot type", IPlayerManagerRef.GetCurrentBotType}
            //         });
            //     Debug.Log("ClassicXn0::Draw:Result:" + l_result);
            // }

            sendLevelEndEvent(TRADITIONAL_GAME_TITLE, getPlayerType(), DRAW_ID);

            if (IPlayerManagerRef.IsPlayer2Bot)
            {
                FirebaseAnalytics.LogEvent("BotType",
                 new Parameter("level_type", TRADITIONAL_GAME_TITLE),
                new Parameter("difficulty", IPlayerManagerRef.GetCurrentBotType.ToString()),
                new Parameter("result", DRAW_ID));
            }
        }

        public void sendNestedXn0WinnerData(bool a_binaryData, int a_iPatternId)
        {
            // if (IPlayerManagerRef.IsPlayer2Bot)
            // {
            //     AnalyticsResult l_result = Analytics.CustomEvent("NestedXn0::Winner:", new Dictionary<string, object>() {
            //         {"Bot type", IPlayerManagerRef.GetCurrentBotType}
            //         });
            //     Debug.Log("NestedXn0::Winner:Result:" + l_result);
            // }

            sendLevelEndEvent(NESTED_GAME_TITLE, getPlayerType(), WINNER_ID);

            if (IPlayerManagerRef.IsPlayer2Bot)
            {
                Player l_player = IPlayerManagerRef.getPlayerViaBinaryCode(a_binaryData);
                string l_strWinningPLayer = null;
                if (EPlayer.Player1.Equals(l_player.EPlayerId))
                {
                    l_strWinningPLayer = "Player";
                }
                else
                {
                    l_strWinningPLayer = "Bot";
                }

                FirebaseAnalytics.LogEvent("BotType",
                 new Parameter("level_type", NESTED_GAME_TITLE),
                new Parameter("difficulty", IPlayerManagerRef.GetCurrentBotType.ToString()),
                new Parameter("result", WINNER_ID + ", " + l_strWinningPLayer));
            }
        }

        public void sendNestedXn0DrawData()
        {
            // if (IPlayerManagerRef.IsPlayer2Bot)
            // {
            //     AnalyticsResult l_result = Analytics.CustomEvent("NestedXn0::Draw:", new Dictionary<string, object>() {
            //         {"Bot type", IPlayerManagerRef.GetCurrentBotType}
            //         });
            //     Debug.Log("NestedXn0::Draw:Result:" + l_result);
            // }

            sendLevelEndEvent(NESTED_GAME_TITLE, getPlayerType(), DRAW_ID);

            if (IPlayerManagerRef.IsPlayer2Bot)
            {


                FirebaseAnalytics.LogEvent("BotType",
                 new Parameter("level_type", NESTED_GAME_TITLE),
                new Parameter("difficulty", IPlayerManagerRef.GetCurrentBotType.ToString()),
                new Parameter("result", DRAW_ID));
            }
        }

        public void sendLevelStartEvent(string a_strLevelType, string a_strPlayerType)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart,
            new Parameter("level_type", a_strLevelType),
            new Parameter("player_type", a_strPlayerType));
        }

        public void sendLevelEndEvent(string a_strLevelType, string a_strPlayerType, string a_strResult)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd,
            new Parameter("level_type", a_strLevelType),
            new Parameter("player_type", a_strPlayerType),
            new Parameter("result", a_strResult));
        }

        #endregion

        #region ANALYTIC HELPER

        private string getPlayerType()
        {
            string l_strPlayerType = null;

            if (IPlayerManagerRef.IsPlayer2Bot)
            {
                l_strPlayerType = "Bot";
            }
            else
            {
                l_strPlayerType = "Player";
            }

            return l_strPlayerType;
        }

        #endregion
    }
}