﻿using Shailan.System.Manager;

namespace NestedXn0.Manager.Hud
{
    internal interface IHudManager : IManagerBase
    {
        /// <summary>
        ///  IS Activted block Zoomed in?
        /// </summary>
        /// <value></value>
        bool IsActiveBlockZoomedIn { get; }

        /// <summary>
        /// Show UI
        /// </summary>
        void ShowXn0Hud();

        /// <summary>
        /// Hide UI
        /// </summary>
        void HideXn0Hud();

        /// <summary>
        /// Show UI
        /// </summary>
        void ShowNestedXn0Hud();

        /// <summary>
        /// Hide UI
        /// </summary>
        void HideNestedXn0Hud();

        /// <summary>
        /// Camera Zoom in to block via Id 
        /// </summary>
        /// <param name="a_iBlockId"></param>
        void ZoomInToBlock(int a_iBlockId);

        /// <summary>
        /// Camera Zoom outs to block via Id 
        /// </summary>
        /// <param name="a_iBlockId"></param>
        void ZoomOutToBlock(int a_iBlockId);

        /// <summary>
        /// Show all the HUd buttons
        /// </summary>
        void ShowXn0Btns();

        /// <summary>
        /// Hide all the HUd buttons
        /// </summary>
        void HideXn0Btns();

        /// <summary>
        /// Show all the Nested UI HUd buttons
        /// </summary>
        void ShowNestedXn0Btns();

        /// <summary>
        /// Hide all the Nested UI HUd buttons
        /// </summary>
        void HideNestedXn0Btns();

        /// <summary>
        /// Reset player Board Data
        /// </summary>
        void ResetPlayerBoardData();

        /// <summary>
        /// Zoom Out Of Current active Block
        /// </summary>
        void ZoomOutOfCurrentBlock();
    }
}