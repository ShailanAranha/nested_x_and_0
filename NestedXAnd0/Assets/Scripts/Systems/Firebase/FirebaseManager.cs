﻿using UnityEngine;
using Firebase;
using Firebase.Analytics;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Extensions;
using Firebase.RemoteConfig;
using System;
using Shailan.System.Manager;

namespace Shailan.System.Analytics
{
    public class FirebaseManager : ManagerBase, IFirebaseManager
    {
        // Start is called before the first frame update
        protected override void ManagerStart()
        {
            base.ManagerStart();

            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                // FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                FirebaseApp l_app = FirebaseApp.DefaultInstance;
                // FireBaseRemoteConfigInit();

                if (m_isDisableRemoteConfigNEnableAds)
                {
                    EnableInterstitialAd = EnableBannerAd = true;
                }
                else
                {
                    FireBaseRemoteConfigInit();
                }
            }
            );
        }


        #region REMOTE CONFIG

        public bool EnableBannerAd { get; private set; }
        public bool EnableInterstitialAd { get; private set; }

        private Dictionary<string, object> m_dicDefaultValues = new Dictionary<string, object>();

        private FirebaseRemoteConfig m_instance = null;
        private FirebaseRemoteConfig RemoteConfigDefaultInstance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = FirebaseRemoteConfig.DefaultInstance;
                }
                return m_instance;
            }
        }

        [SerializeField]
        private bool m_isDebug = false;

        [SerializeField]
        private bool m_isDisableRemoteConfigNEnableAds = false;

        private void FireBaseRemoteConfigInit()
        {
            // These are the values that are used if we haven't fetched data from the
            // server
            // yet, or if we ask for values that the server doesn't have:
            m_dicDefaultValues.Add("EnableBannerAd", false);
            m_dicDefaultValues.Add("EnableInterstitialAd", false);
            m_dicDefaultValues.Add("Test", "Firebase remote config default text");

            RemoteConfigDefaultInstance.SetDefaultsAsync(m_dicDefaultValues)
              .ContinueWithOnMainThread(task =>
              {
                  Debug.Log("Remote config is ready");
                  FetchDataAsync();
              });
        }


        // Start a fetch request.
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number. For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        public Task FetchDataAsync()
        {
            Debug.Log("Fetching data...");
            // Task fetchTask = RemoteConfigDefaultInstance.FetchAsync(TimeSpan.Zero); // For Testing
            Task fetchTask = RemoteConfigDefaultInstance.FetchAsync();
            return fetchTask.ContinueWithOnMainThread(FetchComplete);
        }

        private void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("Fetch completed successfully!");
            }

            ConfigInfo info = RemoteConfigDefaultInstance.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    RemoteConfigDefaultInstance.ActivateAsync()
                    .ContinueWithOnMainThread(task =>
                    {
                        // if (m_isDebug)
                        Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).", info.FetchTime));

                        EnableBannerAd = RemoteConfigDefaultInstance.GetValue("EnableBannerAd").BooleanValue;
                        EnableInterstitialAd = RemoteConfigDefaultInstance.GetValue("EnableInterstitialAd").BooleanValue;

                        if (m_isDebug)
                        {
                            Debug.LogError(RemoteConfigDefaultInstance.GetValue("Test").StringValue);
                            Debug.LogError("EnableBannerAd:" + EnableBannerAd);
                            Debug.LogError("EnableInterstitialAd:" + EnableInterstitialAd);
                        }
                    });

                    break;
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
        }

        #endregion
    }
}